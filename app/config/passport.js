global.passport = require('passport');
var passportLocal = require('passport-local');
var FacebookStrategy = require('passport-facebook').Strategy;
var User = require('../models/users');
var Scored = require('../models/scoreds');
var App = require('../models/apps');
var AppRepeat = require('../models/apprepeat');
var InviteFriend = require('../models/invitefriend');
var configAuth = require('../config/auth');
var facebook = require('../config/facebook');
var helper = require('../config/helpers');
var moment = require('moment-timezone');

passport.use(new passportLocal.Strategy({
    	// by default, local strategy uses username and password, we will override with email
	    usernameField : 'email',
	    passwordField : 'password',
	    passReqToCallback : true // allows us to pass back the entire request to the callback
	}, function(req, email, password, done) {
		User.findOne({ 'email' :  email, fb_id: null }, function(err, user) {
            // if there are any errors, return the error before anything else
            if (err)
                return done(err);
            
        	// User not found or user found but the password is wrong
            if (!user || !user.validPassword(password))
                return done(null, false, req.flash('loginMessage', 'The username or password you have entered is invalid.')); // create the loginMessage and save it to session as flashdata
            
            // all is well, return successful user
            user.lastlogin = moment()._d.toISOString();
            
			if (req.params['appurl'] != 'superadmin') {
				App.findOne({ 'url' : req.params['appurl'] }, function(err, app) {
					Scored.findOne({ 'app_id' : app._id, 'user_id' : user._id }, function(err, scored) {
    	        		if (scored) {
    	        			scored.user_created = user.created;
    	        			scored.user_lastlogin = user.lastlogin;
    	        			scored.save();
    	        		} else {
    	        			var newScored = new Scored();
    	                    newScored.app_id = app._id;
    	                    newScored.user_id = user._id;
    	                    newScored.user_created = user.created;
    	                    newScored.user_lastlogin = user.lastlogin;
    	                    newScored.save();
    	        		}
    	        	});
				});
    		}
			
			// Update last login to scored table
			user.save(function() {
            	return done(null, user);
            });
        });
	}));


passport.use('local-signup', new passportLocal.Strategy({
    // by default, local strategy uses username and password, we will override with email
    usernameField : 'email',
    passwordField : 'password',
    passReqToCallback : true // allows us to pass back the entire request to the callback
},
function(req, email, password, done) {
    // asynchronous
    // User.findOne wont fire unless data is sent back
    process.nextTick(function() {
        // Valid password at least 6 characters
        if (password.length < 6) {
            return done(null, false, req.flash('signupMessage', 'The password must at least 6 characters.'));
        }
	    // find a user whose email is the same as the forms email
	    // we are checking to see if the user trying to login already exists
	    User.findOne({ 'email' :  email }, function(err, user) {
	        // if there are any errors, return the error
	        if (err)
	            return done(err);
	        App.findOne({ 'url' : req.params['appurl'] }, function(err, app) {
	        	 // check to see if theres already a user with that email
		        if (user) {
		            return done(null, false, req.flash('signupMessage', 'That email is already taken.'));
		        } else {
		            // if there is no user with that email
		            // create the user
		            var newUser = new User();
		
		            // set the user's local credentials
		            newUser.email    = email;
		            newUser.password = newUser.generateHash(password);
		            newUser.name     = helper.formatUserName(req.body.name.trim());
		            newUser.role	 = 3; // Role 3 is normal user 
		
		            // save the user
		            newUser.save(function(err) {
		            	var newScored = new Scored();
	                    newScored.app_id = app._id;
	                    newScored.user_id = newUser._id;
	                    newScored.user_created = newUser.created;
	                    newScored.user_lastlogin = newUser.lastlogin;
	                    newScored.save();
	                    
	                    // Increase point for user invite new one
	                    InviteFriend.findOne({
	                    	email_invite: newUser.email,
	                    	user_invite_success_id: null
	                    }, function(err, inviteResult) {
	                    	if (inviteResult) {
	                    		Scored.findOne({
	                    			app_id: app._id,
	                    			user_id: inviteResult.user_invite_friend_id
	                    		}, function(error, score) {
	                    			if (! error && score) {
	                    				score.point += app.invite_point;
	                    				score.save(function(saveErr) {
	                    					if (! saveErr) {
	                    						inviteResult.user_invite_success_id = newUser._id;
	                    						inviteResult.save();
	                    					}
	                    				});
	                    			}
	                    		})
	                    	}
	                    })
	                    
		                return done(null, newUser);
		            });
		        }
	        });
	        
	       
	    });
    });

}));

passport.use(new FacebookStrategy({

    // pull in our app id and secret from our auth.js file
    clientID        : configAuth.facebookAuth.clientID,
    clientSecret    : configAuth.facebookAuth.clientSecret,
    callbackURL     : configAuth.facebookAuth.callback,
    passReqToCallback: true,
    profileFields   : ['id', 'displayName', 'photos', 'email', 'gender']
},

// facebook will send back the token and profile
function(req, token, refreshToken, profile, done) {
    // asynchronous
    process.nextTick(function() {
    	console.log('--------profile-----------');
    	console.log(profile);
    	console.log('--------token-----------');
    	console.log(token);
    	var fb_email = '';
    	if( typeof(profile.emails) != "undefined") {
    		fb_email = profile.emails.length ? profile.emails[0].value : '';	// facebook can return multiple emails so we'll take the first
    	}
    	var condition = {'fb_id' : profile.id}; // find the user in the database based on their facebook id or user register as admin of app which login only FB
    	if (fb_email) {
    		condition = {
    				$or: [
    				       { fb_id: profile.id },
    				       { $and: [{email: fb_email}, {role: 2}] }
    				]
    		}
    	}
        
        User.findOne(condition, function(err, user) {
        	
            // if there is an error, stop everything and return that
            // ie an error connecting to the database
            if (err)
                return done(err);
            
            App.findOne({ 'url' : req.params['id'] }, function(err, app) {
            	 // if the user is found, then log them in
                if (user) {
                	// Update facebook infomation
                	//user.avatar = profile.photos[0].value;
                	//user.name = helper.formatUserName(profile.displayName);
                	//user.email = fb_email;
                	//user.fb_gender = profile.gender;
                	user.fb_token = token;
                	user.lastlogin = moment()._d.toISOString();
                	// Get fb friends
                	facebook.getFbData(token, '/me/friends', function(data){
                        user.fb_friends = data.summary['total_count'];
                        user.save();
                    });
                	
                	// Upsert scored with gender
                	Scored.findOne({ 'app_id' : app._id, 'user_id' : user._id }, function(err, scored) {
                		if (scored) {
                			scored.fb_gender = profile.gender;
                			scored.user_created = user.created;
                			scored.user_lastlogin = user.lastlogin;
                			scored.save();
                		} else {
                			var newScored = new Scored();
                            newScored.fb_gender = profile.gender;
                            newScored.app_id = app._id;
                            newScored.user_id = user._id;
                            newScored.user_created = user.created;
                            newScored.user_lastlogin = user.lastlogin;
                            newScored.save();
                		}
                	});
                	
                	user.save(function() {
                		return done(null, user); // user found, return that user
                	});
                } else {
                    // if there is no user found with that facebook id, create them
                    var newUser            = new User();

                    // set all of the facebook information in our user model
                    newUser.avatar   = profile.photos[0].value;
                    newUser.fb_id    = profile.id; // set the users facebook id                   
                    newUser.fb_token = token; // we will save the token that facebook provides to the user                    
                    newUser.name  = helper.formatUserName(profile.displayName);
                    newUser.email = fb_email;
                    newUser.role  = 3; // Role 3 is normal user 
                    newUser.fb_gender = profile.gender;
                    // Get fb friends
                	facebook.getFbData(token, '/me/friends', function(data){
                        newUser.fb_friends = data.summary['total_count'];
                        newUser.save();
                    });
                    
                    // save our user to the database
                    newUser.save(function(err, user) {
                        if (err)
                            throw err;
                    	
                    	var newScored = new Scored();
                        newScored.fb_gender = profile.gender;
                        newScored.app_id = app._id;
                        newScored.user_id = user._id;
                        newScored.user_created = user.created;
                        newScored.user_lastlogin = user.lastlogin;
                        newScored.save();
                        
                        // Increase point for user invite new one
                        InviteFriend.findOne({
                        	email_invite: newUser.email,
                        	user_invite_success_id: null
                        }, function(err, inviteResult) {
                        	if (inviteResult) {
                        		Scored.findOne({
                        			app_id: app._id,
                        			user_id: inviteResult.user_invite_friend_id
                        		}, function(error, score) {
                        			if (! error && score) {
                        				score.point += app.invite_point;
                        				score.save(function(saveErr) {
                        					if (! saveErr) {
                        						inviteResult.user_invite_success_id = newUser._id;
                        						inviteResult.save();
                        					}
                        				});
                        			}
                        		})
                        	}
                        })
    					
                        // if successful, return the new user
                        return done(null, newUser);
                    });
                }
            });
           

        });
    });

}));


passport.serializeUser(function(user, done) {	
	done(null, {id : user._id, name: user.name, email: user.email, role: user.role, fb_gender: user.fb_gender ? user.fb_gender : '', avatar: user.avatar ? user.avatar : '/images/no-avatar.jpg'});
});

passport.deserializeUser(function(user, done) {
	done(null, user);
});