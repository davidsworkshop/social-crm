var multer  = require('multer');

// expose our config directly to our application using module.exports
module.exports = {

    'multer' : {
        'storageQuestion' : storage('question'),
        'storageApp' : storage('app'),
        'imageFilter' : imageFilter()
    },
    'smtpconfig' : {
    	'host': 'secure166.inmotionhosting.com',
    	'port': 465,
    	'user': 'phuc.do@quodisys.com',
    	'pass': 'phuc1234'
    },
    'adminemail' : 'phuc.do@quodisys.com',
    'domain': 'quizzu.rocks'

};

function storage(type) {
	var prefixFile = '';
	
	switch(type) {
		case 'question':
			prefixFile = 'qs-';
	        break;
	    case 'app':
	    	prefixFile = 'app-';
	        break;
	}
	
	var storage = multer.diskStorage({
	  destination: './public/uploads',
	  filename: function (req, file, cb) {
		var re = /(?:\.([^.]+))?$/;
		var ext = re.exec(file.originalname)[1];
		var randomNumber = Math.floor(Math.random() * 1000);	// Add random number to fix upload 1 file many time in same time
		if(req.params['appurl'] != 'superadmin')
			cb(null, prefixFile +'image-' + Date.now() +'-'+ randomNumber +'.'+ ext);
		else
			cb(null, prefixFile +'image-' + Date.now() +'-'+ randomNumber +'.'+ ext);
	  }
	})
	
	return storage;
}

function imageFilter() {
	var imageFilter = function(req, file, cb) {
		var acceptMimeType = ['image/jpeg', 'image/pjpeg', 'image/png', 'image/gif'];
		if (acceptMimeType.indexOf(file.mimetype) == -1) {
			return cb(new Error('Image file type is not accept.'));
		}
		
		cb(null, true);
	}
	
	return imageFilter;
}