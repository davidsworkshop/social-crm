$(document).ready(function() {
	$(document)
	.on('click', '.check-wrapper span', function() {
		var answer = $(this).attr('data-answer'),
			correctAnswer = $('.qs-answer').text();
		
		if (answer != correctAnswer) {
			$('#helpModal').modal('show');
			$('.preview-question .question-title').hide();
			$('.preview-question .question-footer').hide();
		}
	})
	
	.on('click', '.modal-body .icon-Icon-Check', function() {
		$('#helpModal').modal('hide');
		$('.question-title').show();
		$('.question-footer').show();
	})
	
	// Fit question image background
	// $('.question-page .question-content').css('background-position', 'center '+ $('.my-container.percent-competion-wrapper').outerHeight() + 'px');
	// $( window ).resize(function() {
	// 	$('.question-page .question-content').css('background-position', 'center '+ $('.my-container.percent-competion-wrapper').outerHeight() + 'px');
	// });
	
	// Fit modal width
	var questionWrapperWidth = $('.question-content').width()
	$('.modal-dialog').css('width', questionWrapperWidth + 'px');
	
	// Percent complete
	window.circleProgress = new ProgressBar.Circle('#cirle-progress', {
		  strokeWidth: 3,
		  trailWidth: 1,
		  trailColor: '#FFBDBD',
		  easing: 'easeInOut',
		  duration: 700,
		  text: {
		  	style: {
		            // Default: color same as stroke color (options.color)
		            color: '#f22549',
		            fontSize: '16px',
		            fontWeight: '600',
		            position: 'absolute',
		            left: '50%',
		            top: '50%',
		            padding: 0,
		            margin: 0,
		            // You can specify styles which will be browser prefixed
		            transform: {
		                prefix: true,
		                value: 'translate(-50%, -50%)'
		            }
		        },
		  },
		  from: { color: '#f22549', width: 1 },
		  to: { color: '#f22549', width: 4 },
		  // Set default step function for all animate calls
		  step: function(state, circle) {
			// $('.question-page .question-content').css('background-position', 'center '+ $('.my-container.percent-competion-wrapper').outerHeight() + 'px');
			circle.path.setAttribute('stroke', state.color);

			var value = Math.round(circle.value() * 100);
			if (value === 0) {
			  circle.setText('0%');
			} else {
			  circle.setText(value +'<span class="small-percent">%</span>');
			}
		 }
	});
	circleProgress.animate(0);
	
	// Facebook share
	$('.facebook-share').click(function() {
		var link = $(this).attr('data-link');
		
	    FB.ui({
	        method: 'share',
	        mobile_iframe: true,
	        href: link,
	        }, function(response){
	        	console.log(response)
	        });
	});
});

/* Set the width of the side navigation */
function openNav() {
	if (! $('#mySidenav').length) {	return false; }
	
	document.getElementsByClassName("icon-icon-menu")[0].style.display = "none";	// Hide menu icon
	
	// For device width small then ipad we define slide nav width fit with device
	if ($(window).width() < 768 ) {
		document.getElementById("mySidenav").style.width = "220px";
		return;
	}
	
    document.getElementById("mySidenav").style.width = "310px";
}