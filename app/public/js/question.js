var countClick = 0,
	todayScore = 0,
	bonusSkipAvailable = true;

$(document).ready(function() {
	$(document)
	.on('click', '.bonus-bottom .proccess-skip .icon-Icon-no', skipBonus)
	.on('click', '.check-wrapper.answer-action span', answerQuestion)
	.on('click', '.modal-body .icon-Icon-Check', function() {
		$('#helpModal').modal('hide');
		$('.question-title').show();
		$('.question-footer').show();
		PageTransitions.nextPage(1);
	})
	.on('click', '.facebook-share', shareFacebook)
	.on('click', '.facebook-share-owner', postToUserFB)

	function constructor() {
		// Close sidenav when click outside
		$(document).click(function (e) {
		    if (! $(e.target).closest('#mySidenav').length && e.target.id != "menu-icon") {
		    	closeNav();
		    }
		});

		// Fit question image background
		// $('.question-page .question-content').css('background-position', 'center '+ $('.my-container.percent-competion-wrapper').outerHeight() + 'px');
		// $( window ).resize(function() {
		// 	$('.question-page .question-content').css('background-position', 'center '+ $('.my-container.percent-competion-wrapper').outerHeight() + 'px');
		// });


		var questionWrapperWidth = $('.question-content').width();
		$('.modal-dialog').css('width', questionWrapperWidth + 'px');
		
		// Show top menu if not have walk through page
		if(! $('.pt-page.walkthrough-page').length) {
			$('.menu-wrapper').removeClass('hidden');
		}
		
		$('.today-score').text(numberWithCommas(score));
		if (! parseInt(totalQuestionAndRound)) {
			$('.menu-top .menu-icon-wrapper').removeClass('hidden');
		}
	}

	function bonusProccess(bonusId, proccess) {
		$.ajax({
            type: 'POST',
            url: window.location.origin +'/'+ appUrl +'/bonus-proccess',
            data: {
                bonus_id: bonusId,
                proccess: proccess
            },
            dataType: 'json',
            success: function(res) {
            	if (typeof res.error == 'undefined') {
            		score += res.point;
        			todayScore += res.point;
        			$('.today-score').text(numberWithCommas(score));
        			$('.total-score').text(numberWithCommas(score));

        			PageTransitions.nextPage(1);
        			countClick++;
        			setCircleProgress();
        			checkLastGame();
            	} else{
            		alert(res.error);
            	}

            	setTimeout(function() {
            		bonusSkipAvailable = true;
            	}, 500);
            }
        });
	}

	function skipBonus() {
		if (bonusSkipAvailable) {
			bonusSkipAvailable = false;
			var bonusId = $('.pt-page-current input[name=bn_id]').val();
			bonusProccess(bonusId, 0);
		}
	}

	function answerQuestion() {
		var answer = $(this).attr('data-answer');
		if (typeof(answer) == 'undefined') { return false;}

        countClick++;
        setCircleProgress();

		var	questionId = $(this).closest('.question-content').find('input[name=question_id]').val(),
			correctAnswer = $(this).closest('.question-content').find('input[name=correct_answer]').val(),
			explain = $(this).closest('.question-content').find('input[name=explain]').val();

		if (typeof(answer) == 'undefined') { return false;}
		if (answer == correctAnswer) {
			score += point_adwarded;
			todayScore += point_adwarded;
			$('.today-score').text(numberWithCommas(score));
			$('.total-score').text(numberWithCommas(score));
			PageTransitions.nextPage(1);
		} else {
			score -= point_deducted;
			todayScore -= point_deducted;
			$('.today-score').text(numberWithCommas(score));
			$('.total-score').text(numberWithCommas(score));
			$('.modal-body').find('.help-content').text(explain);
			$('#helpModal').modal('show');
			$('.question-title').hide();
			$('.question-footer').hide();
		}

		$.ajax({
            type: 'POST',
            url: window.location.origin +'/'+ appUrl +'/answer-question',
            data: {
            	question_id: questionId,
            	answer: answer,
            },
            dataType: 'json',
            success: function(data) {
                checkLastGame();
            }
        });
	}

	function shareFacebook() {
		var link = $(this).attr('data-link');

	    FB.ui({
	        method: 'feed',
	        link: link,
	        display: 'popup',
	        }, function(response){
	        	if (response && response.post_id && bonusSkipAvailable) {
	        		bonusSkipAvailable = false;
	        		var bonusId = $('.pt-page-current input[name=bn_id]').val();
	        		bonusProccess(bonusId, 1);
			    }
	        });
	}
	
	function shareFacebookOwner() {
		FB.ui({
	        method: 'feed',
	        link: 'https://www.facebook.com/HP/',
	        display: 'popup',
	        }, function(response){
	        	console.log(response)
	        });
	}

	// Like FB action
	setTimeout(function(){
		FB.Event.subscribe('edge.create', function(response) {
			if (bonusSkipAvailable) {
				bonusSkipAvailable = false;
				var bonusId = $('.pt-page-current input[name=bn_id]').val();
				bonusProccess(bonusId, 1);
			}

		});

		FB.Event.subscribe('edge.remove', function(response) {
		    // Action unlike
		});
	},5000);

	function checkLastGame() {
		if (countClick == totalQuestionAndRound) {
            $.ajax({
                type: 'POST',
                url: window.location.origin +'/'+ appUrl +'/answer-total',
                data: {},
                dataType: 'json',
            });
        }
	}
	
	// Post to FB wall
	function postToUserFB() {
		var desc = 'I just completed the '+ appName,
			link = location.origin +'/'+ appUrl,
			picture = location.origin + appProfileImg;
		if ($('.today-score').text() != 0) {
			desc += ' and scored '+ $('.today-score').text() +' points!';
		}
		FB.ui({
			method: 'feed',
			name: 'Quizzu',
			link: link,
			picture: picture,
			//caption: 'this is caption',
			description: desc,
		},
		function(response) {
			
		});
	}

	// Percent complete
	window.circleProgress = new ProgressBar.Circle('#cirle-progress', {
		  strokeWidth: 3,
		  trailWidth: 1,
		  trailColor: '#FFBDBD',
		  easing: 'easeInOut',
		  duration: 700,
		  text: {
		  	style: {
		            // Default: color same as stroke color (options.color)
		            color: '#f22549',
		            fontSize: '16px',
		            fontWeight: '600',
		            position: 'absolute',
		            left: '50%',
		            top: '50%',
		            padding: 0,
		            margin: 0,
		            // You can specify styles which will be browser prefixed
		            transform: {
		                prefix: true,
		                value: 'translate(-50%, -50%)'
		            }
		        },
		  },
		  from: { color: '#f22549', width: 1 },
		  to: { color: '#f22549', width: 4 },
		  // Set default step function for all animate calls
		  step: function(state, circle) {
			circle.path.setAttribute('stroke', state.color);

			var value = Math.round(circle.value() * 100);
			if (value === 0) {
			  circle.setText('0%');
			} else {
			  circle.setText(value +'<span class="small-percent">%</span>');
			}
		 }
	});

	function setCircleProgress() {
		var totalQsAndRou = parseInt(totalQuestionAndRound);
		if (countClick && totalQsAndRou) {

			circleProgress.animate((countClick / totalQsAndRou));  // Number from 0.0 to 1.0
		} else {
			circleProgress.animate(0);  // Number from 0.0 to 1.0
		}
	}

	setCircleProgress();
	constructor();
});

function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}
