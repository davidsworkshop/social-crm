$(document).ready(function() {
	var increaseUnique = 0,
		toDay = moment().format('YYYY-MM-DD');
	
	// When not exist storage landingPage => create storage and increase unique visit
	if (! localStorage.getItem('landingPage')) {
		increaseUnique = 1;
		var landingData = {};
		landingData[appId] = {
			increaseUnique: increaseUnique,
			firstTime : 1,
			date: toDay
		};
		
		// Increase unique visit this date
		$.ajax({
	        type: 'POST',
	        url: window.location.origin +'/'+ appUrl +'/apiLandingPage/',
	        dataType: 'json',
	        data : landingData[appId],
	        success: function(res) {
	            if (res.success) {
	            	localStorage.setItem('landingPage', JSON.stringify(landingData));
	            } else {
	            	console.log(res.message);
	            }
	        }
	    });
	} else {
		var landingData = JSON.parse(localStorage.getItem('landingPage'));
		
		// If exist storage landingPage but not exist property for current app
		if (typeof(landingData[appId]) == "undefined") {
			increaseUnique = 1;
			
			landingData[appId] = {
				increaseUnique: increaseUnique,
				firstTime : 1,
				date: toDay
			};
		} else {
			landingData[appId]['increaseUnique'] = 0;
		}
		
		$.ajax({
	        type: 'POST',
	        url: window.location.origin +'/'+ appUrl +'/apiLandingPage/',
	        dataType: 'json',
	        data : landingData[appId],
	        success: function(res) {
	            if (res.success) {
	            	// If not the first time visit this app => change first time to 0
	            	if (! increaseUnique) {
	            		landingData[appId]['firstTime'] = 0;
	            		landingData[appId]['date'] = toDay;
	            	}
	            	localStorage.setItem('landingPage', JSON.stringify(landingData));
	            } else {
	            	console.log(res.message);
	            }
	        }
	    });
	}
});