(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
//supper admin
ga('create', window.CONFIG.GA_TRACKING_ID_SUPERADMIN, 'auto');
ga('require', 'linkid', 'linkid.js');
ga('require', 'displayfeatures');
//app
ga('create', ga_app_tracking_id, 'auto', {'name': 'newTracker'});  // New tracker.
ga('newTracker.send', 'pageview');

ga('send', 'pageview');
