var pageviews, nsups, appvisitors;

$(document).ready(function() {
	$(document)
		.on('click', '#page-view-custom-btn', getCustomPageView)
		.on('click', '#repeat-chart-custom-btn', getCustomRepeatChart)
		.on('click', '#pageviews-select li:not(.active)', pageViewsChange)	// Change pageviews chart when change type
		.on('click', '#appvisitors-select li:not(.active)', repeatVisitorChange)
		.on('click', 'body', function(e) {
			if (! $(e.target).hasClass("btn-drp")) {
				$('ul.dropdown-lst').fadeOut();
			}
		})
		.on('click', '#newsgnups li:not(.active)', newSignupChartChange)

	$('#datetimepicker1, #datetimepicker2, #datetimepickerRepeat1, #datetimepickerRepeat2').datetimepicker({
		useCurrent: false,
		format: 'dddd DD MMMM YYYY',
		maxDate: moment(),
		widgetPositioning: {vertical: 'bottom'}
	});

	$('#datetimepicker1').on("dp.change", function(e) {
		$('#datetimepicker2, #page-view-custom-btn').removeClass('hidden');
		$('#datetimepicker2').data("DateTimePicker").minDate($('#datetimepicker1').data("DateTimePicker").date());
	});

	$('#datetimepickerRepeat1').on("dp.change", function(e) {
		$('#datetimepickerRepeat2, #repeat-chart-custom-btn').removeClass('hidden');
		$('#datetimepickerRepeat2').data("DateTimePicker").minDate($('#datetimepickerRepeat1').data("DateTimePicker").date());
	});
	var pwusertb = $('#pwusertb').removeAttr('width').DataTable( {
	    scrollY:        "340px",
	    scrollCollapse: true,
	    paging:         false,
	    columnDefs: [
 	        {
 	        	"width": "35%",
 	        	"targets": 0,
 	            "orderable": false,
 	        },
 	    ],
 	    order: [[ 1, 'desc' ]],
	    fixedColumns: true
	} );
	var linksin = $('#linksin').removeAttr('width').DataTable( {
	    scrollY:        "340px",
	    scrollCollapse: true,
	    paging:         false,
	    columnDefs: [
	         {
	        	 "width": "60%",
	         	"targets": 0,
	             "orderable": false
	         },
	         {
	        	 "width": "15%",
	         	"targets": 1,
	             "orderable": false
	         }
	    ],
	    fixedColumns: true
	} );
	$("#pwusertb_wrapper .dataTables_length, #pwusertb_info, #pwusertb_filter ,#pwusertb_paginate").hide();
	$("#linksin_wrapper .dataTables_length, #linksin_info, #linksin_filter ,#linksin_paginate").hide();

	// Set time out 1 minute to update numbet unique visit today
	setInterval(function(){
		$.ajax({
	        type: 'POST',
	        url: window.location.origin +'/'+ appUrl +'/apiGetUniqueToday',
	        dataType: 'json',
	        success: function(res) {
	            $('#unique-today-numb').text(res.uniqueToday.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
	        }
	    });
	}, 60000);
});



function pageViewsChange() {
	var type = $(this).attr('data-value');
	if (type != 'custom') {
		$('#datetimepicker1, #datetimepicker2, #page-view-custom-btn').addClass('hidden');
		$.ajax({
	        type: 'POST',
	        url: window.location.origin +'/'+ appUrl +'/apiGetPageView/' +type,
	        dataType: 'json',
	        success: function(res) {
	            var dataProvider = res.data;
	            initPageViewsChart(dataProvider, res.datenum, res.toDate);
	        }
	    });
	} else {
		if ($('#datetimepicker1').data("DateTimePicker").date()) {
			$('#datetimepicker2, #page-view-custom-btn').removeClass('hidden');
		}
		$('#datetimepicker1').removeClass('hidden');
	}
}

function repeatVisitorChange() {
	var type = $(this).attr('data-value');
	if (type != 'custom') {
		$('#datetimepickerRepeat1, #datetimepickerRepeat2, #repeat-chart-custom-btn').addClass('hidden');
		$.ajax({
	        type: 'POST',
	        url: window.location.origin +'/'+ appUrl +'/apiGetAppRepeat/' +type,
	        dataType: 'json',
	        success: function(res) {
	            var dataProvider = res.data;
	            initRepeatVisitorChart(dataProvider, res.datenum, res.toDate);
	        }
	    });
	} else {
		if ($('#datetimepickerRepeat1').data("DateTimePicker").date()) {
			$('#datetimepickerRepeat2, #repeat-chart-custom-btn').removeClass('hidden');
		}
		$('#datetimepickerRepeat1').removeClass('hidden');
	}
}

function newSignupChartChange() {
	var time = $(this).attr('data-value');
	$.ajax({
        type: 'POST',
        url: window.location.origin +'/'+ appUrl +'/apiGetSignupByMonth/' +time,
        dataType: 'json',
        success: function(res) {
        	initNewSignupChart(res.data, res.datenum);
        }
    });
}

function getCustomPageView() {
	var from = $('#datetimepicker1').data("DateTimePicker").date(),
		to = $('#datetimepicker2').data("DateTimePicker").date()

	if (from && to) {
		if (from > to) {
			$('.alert-modal .modal-body').html('<p class="text-danger">From Date larger than To Date.</p>');
			$('.alert-modal').modal('show');
		} else {
			$.ajax({
		        type: 'POST',
		        url: window.location.origin +'/'+ appUrl +'/apiGetPageView/custom',
		        data: {
		        	from: from.subtract(1,'d').format('YYYY-MM-DD'),
		        	to: to.format('YYYY-MM-DD')
		        },
		        dataType: 'json',
		        success: function(res) {
		            var dataProvider = res.data;
		            initPageViewsChart(dataProvider, res.datenum, res.toDate)
		        }
		    });
		}

	} else {
		$('.alert-modal .modal-body').html('<p class="text-danger">Please Choose From Date and To Date.</p>');
		$('.alert-modal').modal('show');
	}
}

function getCustomRepeatChart() {
	var from = $('#datetimepickerRepeat1').data("DateTimePicker").date(),
		to = $('#datetimepickerRepeat2').data("DateTimePicker").date()

	if (from && to) {
		if (from > to) {
			$('.alert-modal .modal-body').html('<p class="text-danger">From Date larger than To Date.</p>');
			$('.alert-modal').modal('show');
		} else {
			$.ajax({
		        type: 'POST',
		        url: window.location.origin +'/'+ appUrl +'/apiGetAppRepeat/custom',
		        data: {
		        	from: from.subtract(1,'d').format('YYYY-MM-DD'),
		        	to: to.format('YYYY-MM-DD')
		        },
		        dataType: 'json',
		        success: function(res) {
		            initRepeatVisitorChart(res.data, res.datenum, res.toDate);
		        }
		    });
		}

	} else {
		$('.alert-modal .modal-body').html('<p class="text-danger">Please Choose From Date and To Date.</p>');
		$('.alert-modal').modal('show');
	}
}

var gender = AmCharts.makeChart( "gender", {
  "type": "pie",
  "balloonText": "[[title]]: [[percent]]% ([[value]])",
  "theme": "light",
  "fontFamily": "'Open Sans' !important",
  "pullOutRadius": 0,
  "percentPrecision": 0,
  "pullOutDuration": 0,
  "startDuration": 0,
  "dataProvider": [
  {
    "title": "Males",
    "value": numMale,
    "color": "#355A71",
    "percent": percentMale
  },
  {
    "title": "Females",
    "value": numFemale,
    "color": "#BE6274",
    "percent": percentFemale
  },],
  "balloon": {
		"adjustBorderColor": false,
		"borderColor": "#515974",
		"borderThickness": 0,
		"color": "#EAEEF5",
		"cornerRadius": 8,
		"disableMouseEvents": false,
		"fadeOutDuration": 0,
		"fillAlpha": 1,
		"fillColor": "#515974",
		"fontSize": 15,
		"horizontalPadding": 14,
		"pointerWidth": 0,
		"shadowAlpha": 0.04,
		"verticalPadding": 10,
		"fixedPosition": false,
		"pointerOrientation": "up",
	},
  "titleField": "title",
  "valueField": "value",
  "labelRadius": 5,
  "colorField": "color",
  "radius": "42%",
  "innerRadius": "65%",
  "labelText": "",
  "export": {
    "enabled": true
  }
} );

/* initPageViewsChart
 * @param basedata : array (Data from model appViews)
 * @param DateNum : int (Number date item should show in chart)
 * @param toDate : date string
 * */
function initPageViewsChart(basedata, dateNum, toDate) {
	var dataProvider = createPageViewDatas(basedata, dateNum, toDate);
	pageviews = AmCharts.makeChart("pageviews",
			{
		"type": "serial",
		"fontFamily": "'Open Sans' !important",
		"addClassNames": true,
		"categoryField": "dateformat",
		"columnSpacing": 9,
		"columnWidth": 0,
		"synchronizeGrid": true,
		"zoomOutOnDataUpdate": false,
		"autoMarginOffset": 0,
		"marginBottom": 70,
		"startDuration": 0,
		"marginLeft": 40,
		"marginRight": 40,
		"marginTop": 0,
		"maxZoomFactor": 0,
		"zoomOutButtonImageSize": 8,
		"zoomOutButtonPadding": 5,
		"gridAboveGraphs": false,
		"synchronizeGrid": true,
		"sequencedAnimation": false,
		"startEffect": "elastic",
		"backgroundColor": "#ECF0F5",
		"borderColor": "#ECF0F5",
		"color": "#000000",
		"handDrawn": false,
		"handDrawScatter": 0,
		"handDrawThickness": 0,
		"hideBalloonTime": 1,
		"panEventsEnabled": false,
		"processCount": 0,
		"theme": "chalk",
		"export": {
			"enabled": true
		},
		"categoryAxis": {
			"startOnAxis": "true",
			"tickLength": 0,
			"fontSize": 13,
			"dashLength": 5,
			"tickLength": 4,
			"axisColor": "#0F0C0C",
			"gridAlpha": 0,
			"gridColor": "#F2F6FC",

		},
		"chartCursor": {
			"enabled": true,
			"oneBalloonOnly": true,
			"categoryBalloonEnabled": false,
			"cursorColor": "#E1E6ED",
			"cursorAlpha": 0.4,
			"avoidBalloonOverlapping": false,
			"leaveAfterTouch": true,
			"tabIndex": 30
		},
		"trendLines": [],
		"graphs": [
			{
				"balloonColor": "#515974",
				"balloonText": "" +
						"<strong><div class='max-weight' style='text-align:center !important;'>[[ballon_date]]</div></strong>" +
						"<div style='height:10px;'></div>" +
						"<span class='text-left'>Page Views</span><br><span class='text-left'><strong class='max-weight'>[[view]]</strong></span>" +
						"<div style='height:5px;'></div>" +
						"<span class='text-left'>Change</span><br><strong class='max-weight'>[[change]]</strong>" +
						"<div style='height:5px;'></div>" +
						"<span class='text-left'>Percentage</span><br><strong class='text-left max-weight'>[[pct]]</strong>",
				//"bullet": "round",
				"bullet": "none",
				"bulletBorderAlpha": 1,
				"bulletBorderColor": "#515974",
				"bulletColor": "#515974",
				"bulletSize": 10,
				"color": "#333",
				"columnWidth": 1,
				"cursorBulletAlpha": 0,
				"fillAlphas": 1,
				"fillColors": ["#FBAD2E", "#E434BF"],
				"fixedColumnWidth": 0,
				"id": "pageviews-amchart",
				"labelAnchor": "middle",
				"labelPosition": "top",
				//"labelText": "[[view]]",
				"labelText": "[[]]",
				"legendColor": "",
				"lineAlpha": 1,
				"maxBulletSize": 48,
				"minBulletSize": 7,
				"negativeFillAlphas": 0.56,
				"periodSpan": 0,
				"precision": 0,
				"showAllValueLabels": true,
				"showBulletsAt": "high",
				"tabIndex": 5,
				"title": "days",
				"type": "smoothedLine",
				"useLineColorForBulletBorder": true,
				"valueAxis": "valueaxis-pageview",
				"valueField": "view",
				//"stepDirection": "center",
			}
		],
		"guides": [],
		"valueAxes": [
			{
				"axisFrequency": 0,
				"id": "valueaxis-pageview",
				"minimum": -1,
				"pointPosition": "middle",
				"position": "left",
				"zeroGridAlpha": 0,
				"autoGridCount": false,
				"axisColor": "",
				"axisThickness": 0,
				"color": "#000000",
				"fillColor": "#000000",
				"firstDayOfWeek": 0,
				"ignoreAxisWidth": true,
				"tickLength": 0,
				"title": "",
				"titleBold": false,
				"titleFontSize": 0,
				"titleRotation": 0,
				"gridCount": 5,
				"gridThickness": 1,
				"minHorizontalGap": 0,
				"minVerticalGap": 0,
				"gridColor": "#d7d7d7",
				"gridAlpha": 0.5,
				"fontSize": 13
			}
		],
		"allLabels": [],
		"balloon": {
			"adjustBorderColor": false,
			"borderColor": "#515974",
			"borderThickness": 0,
			"color": "#EAEEF5",
			"cornerRadius": 8,
			"disableMouseEvents": false,
			"fadeOutDuration": 0.26,
			"fillAlpha": 1,
			"fillColor": "#515974",
			"fontSize": 14,
			"horizontalPadding": 20,
			"pointerWidth": 0,
			"shadowAlpha": 0.04,
			"showBullet": true,
			"verticalPadding": 12,
			"fixedPosition": false,
			"pointerOrientation": "up",
		},
		"titles": [
			{
				"id": "pageviews-title",
				"size": 12,
				"text": ""
			}
		],
		"dataProvider": dataProvider
	});
}
/* initNewSignupChart
 * @param basedata : array (Data from model Scoreds)
 * @param DateNum : int (Number date item should show in chart)
 * */
function initNewSignupChart(basedata, dateNum) {
	var dataProvider = createNewSignupChartDatas(basedata, dateNum);

	nsups = AmCharts.makeChart("nsups",
		{
			"type": "serial",
			"categoryField": "day",
			"columnWidth": 0.6,
			"columnSpacing": 10,
			"autoMargins": false,
			"marginLeft": 00,
			"fontFamily": "Open Sans",
			"marginRight": 00,
			"marginTop": 0,
			"plotAreaBorderColor": "",
			"gridAboveGraphs": true,
			"startDuration": 0,
			"theme": "light",
			"categoryAxis": {
				"startOnAxis": false,
				"axisColor": "",
				"axisThickness": 0,
				"centerLabels": true,
				"dashLength": 1,
				"gridAlpha": 0,
				"gridColor": "",
				"gridCount": 0,
				"gridThickness": 0,
				"ignoreAxisWidth": true,
				"minHorizontalGap": 0,
				"minVerticalGap": 0,
				"tickLength": 0
			},
			"trendLines": [],
			"graphs": [
				{
					"balloonColor": "",
					"cornerRadiusTop": 4,
					"fillAlphas": 1,
					"id": "value",
					"lineColor": "",
					"title": "value",
					"type": "column",
					"valueField": "value",
					"balloonFunction": function( graphDataItem, amGraph) {
						if (graphDataItem.dataContext.value) {
							return graphDataItem.dataContext.value.toFixed();
						} else {
							return 0;
						}
					}
				},
				{
					"accessibleLabel": "",
					"balloonText": "",
					"behindColumns": true,
					"bulletHitAreaSize": -2,
					"bulletSize": 6,
					"fillAlphas": 1,
					"fillColors": "#E1E7F0",
					"id": "remainingValue",
					"labelText": "[[]]",
					"lineColor": "#E1E7F0",
					"showBalloon": false,
					"showOnAxis": true,
					"title": "",
					"type": "column",
					"valueField": "remainingValue"
				}
			],
			"guides": [],
			"valueAxes": [
				{
					"id": "nsups-valueaxes",
					"stackType": "100%",
					"dashLength": 5,
					"gridAlpha": 0.5,
					"gridColor": "#FFFFFF",
					"axisThickness": 0,
					"labelsEnabled": false,
					"minHorizontalGap": 0,
					"minVerticalGap": 26,
					"tickLength": 0,
					"title": ""
				}
			],
			"allLabels": [],
			"balloon": {
				"borderColor": "#FFF",
				"color": "#FFFFFF",
				"cornerRadius": 19,
				"fillAlpha": 1,
				"fontSize": 15,
				"fillColor": "#515974",
				"horizontalPadding": 20,
				"shadowColor": "",
				"verticalPadding": 10,
		 		"borderThickness": 1,
			},
			"titles": [
				{
					"id": "Title-1",
					"size": 15,
					"text": ""
				}
			],
			"dataProvider": dataProvider
		}
	);
	newSignUpStyleHover();
}

/* initRepeatVisitorChart
 * @param basedata : array (Data from model apprepeat)
 * @param DateNum : int (Number date item should show in chart)
 * @param toDate : date string
 * */
function initRepeatVisitorChart(basedata, dateNum, toDate) {
	var dataProvider = createRepeatVisitorDatas(basedata, dateNum, toDate);
	appvisitors = AmCharts.makeChart("appvisitors",
	{
		"type": "serial",
		"fontFamily": "'Open Sans' !important",
		"addClassNames": true,
		"categoryField": "dateformat",
		"columnSpacing": 0,
		"plotAreaBorderColor": "",
		"plotAreaFillColors": "",
		"zoomOutButtonImageSize": 12,
		"startEffect": "easeOutSine",
		"accessible": true,
		"gridAboveGraphs": false,
		"addClassNames": true,
		"backgroundColor": "",
		"borderColor": "",
		"autoMargins": false,
		"marginBottom": 70,
		"marginLeft": 40,
		"marginRight": 40,
		"marginTop": 10,
		"fontSize": 14,
		"processTimeout": -2,
		"theme": "light",
		"categoryAxis": {
			"startOnAxis": "true",
			"tickLength": 0,
			"fontSize": 13,
			"dashLength": 5,
			"tickLength": 4,
			"axisColor": "#0F0C0C",
			"gridAlpha": 0,
			"gridColor": "#F2F6FC",
		},
		"chartCursor": {
			"enabled": true,
			"oneBalloonOnly": true,
			"categoryBalloonEnabled": false,
			"cursorColor": "#E1E6ED",
			"cursorAlpha": 0.4,
			"avoidBalloonOverlapping": false,
			"tabIndex": 30,
			"leaveAfterTouch": true,
		},
		"trendLines": [],
		"graphs": [
			{
				"balloonColor": "",
				"balloonText": "" + "<strong><div class='max-weight' style='text-align:center !important;'>[[ballon_date]]</div></strong>" +
						"<i class='circle-in-bloon unique fa fa-circle-o' aria-hidden='true'></i>" +
						"<div style='height:10px;'></div>" +
						"Unq. Visitors<br><strong>[[uniqueNum]]</strong>" +
						"<br><div style='height:5px;'></div>" +
						"Change<br><strong>[[uniqueChange]]</strong>" +
						"<br><div style='height:5px;'></div>" +
						"Pct Change<br><strong>[[uniquePct]]</strong>" +
						"<br><div style='height:5px;'></div>" +
						"Repeat Ratio<br><strong>[[repeatRatio]]</strong>",
				"showBalloon":false,
				"bullet": "none", // load first time
				"bulletBorderColor": "#373434",
				"bulletColor": "#373434",
				"bulletHitAreaSize": -2,
				"bulletSize": 7,
				"fillAlphas": 1,
				"fillColors": ["#49AECB","#D6ECE2" ],
				"id": "repeat-visitors",
				"labelText": "[[none]]", //load first time
				"lineColor": "#4AAFCB",
				"lineAlpha": 0,
				"tabIndex": -1,
				"title": "graph 2",
				"type": "smoothedLine",
				"valueField": "uniqueNum",
				"showAllValueLabels": true,
			},
			{
				"balloonColor": "",
				"balloonText": "" + "<strong><div class='max-weight' style='text-align:center !important;'>[[ballon_date]]</div></strong>" +
						"<i class='circle-in-bloon repeat fa fa-circle-o' aria-hidden='true'></i>" +
						"<div style='height:10px;'></div>" +
						"Rpt. Visitors<br><strong>[[repeatNum]]</strong>" +
						"<br><div style='height:5px;'></div>" +
						"Change<br><strong>[[repeatChange]]</strong>" +
						"<br><div style='height:5px;'></div>" +
						"Pct Change<br><strong>[[repeatPct]]</strong>" +
						"<br><div style='height:5px;'></div>" +
						"Repeat Ratio<br><strong>[[repeatRatio]]</strong>",
				"showBalloon":true,
				"bullet": "none",
				//"bullet": "round",hide
				"bulletColor": "#373434",
				"bulletBorderColor": "#373434",
				"color": "#000000",
				"fillAlphas": 1,
				"fillColors": ["#7f567a", "#f7a0ed"],
				"id": "unique-visitors",
				"labelText": "[[none]]",
				//"labelText": "[[repeatNum]]",
				"lineThickness": 0,
				"negativeLineColor": "",
				"lineAlpha": 0,
				"tabIndex": -2,
				"title": "graph 1",
				"type": "smoothedLine",
				"valueField": "repeatNum",
				"showAllValueLabels": true,
			}
		],
		"guides": [],
		"valueAxes": [
			{
				"axisFrequency": 0,
				"id": "valueaxis-pageview",
				"minimum": 0,
				"pointPosition": "middle",
				"position": "left",
				"zeroGridAlpha": 0,
				"autoGridCount": false,
				"axisColor": "",
				"axisThickness": 0,
				"color": "#000000",
				"fillColor": "#000000",
				"firstDayOfWeek": 0,
				"ignoreAxisWidth": true,
				"tickLength": 0,
				"title": "",
				"titleBold": false,
				"titleFontSize": 0,
				"titleRotation": 0,
				"gridCount": 5,
				"gridThickness": 1,
				"minHorizontalGap": 0,
				"minVerticalGap": 0,
				"gridColor": "#d7d7d7",
				"gridAlpha": 0.5,
				"fontSize": 13
			}
		],
		"allLabels": [],
		"balloon": {
			"borderColor": "#515974",
			"color": "#FFFFFF",
			"cornerRadius": 8,
			"disableMouseEvents": false,
			"fillAlpha": 1,
			"fillColor": "#515974",
			"fixedPosition": false,
			"horizontalPadding": 15,
			"offsetY": 50,
			"pointerOrientation": "up",
			"shadowColor": "",
			"pointerWidth": 0,
			"verticalPadding": 15
		},
		"titles": [
			{
				"id": "Title-1",
				"text": ""
			}
		],
		"dataProvider": dataProvider
	});

	$('#appvisitors-click #repeat').removeClass('hasUnique').text("Repeat Visitors");
}

/* createPageViewDatas
 * @param baseData : array (Data from model appViews)
 * @param dateNum : int (Number date item should show in chart)
 * return array (dataProvider for chart)
 * */
function createPageViewDatas(baseData, dateNum, toDate) {
	var result = [],
		fromDate = moment(toDate).subtract(dateNum,'d'),
		hideCategoryNum = dateNum - 1 <= 18 || dateNum - 1 >= 70 ? 1 : dateNum - 1 <= 36 ? 2 : Math.floor((dateNum - 1) / 15);  // Hide some category title when number item too large

	var availableDate = getAvailableDate(baseData);
	for (var i = 0; i < dateNum; i++) {
		var itemDate = fromDate.add(1, 'day').format('YYYY-MM-DD'),
			itemData = '',
			view = 0,
			change = 0,
			pct = '0.0%';

		if (availableDate.indexOf(itemDate) != -1) {
			itemData = baseData[availableDate.indexOf(itemDate)];
			view = itemData['count_view'];
			if (i != 0) {
				change = itemData['count_view'] - result[i - 1]['view'];
				pct = result[i - 1]['view'] == 0 && view == 0 ? '0.0%' : result[i - 1]['view'] == 0 ? '100.0%' : (change / result[i - 1]['view'] * 100).toFixed(1).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + '%';
			}
		} else {
			if (i != 0) {
				change = 0 - result[i - 1]['view'];
				pct = result[i - 1]['view'] == 0 && view == 0 ? '0.0%' : result[i - 1]['view'] == 0 ? '100.0%' : (change / result[i - 1]['view'] * 100).toFixed(1).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + '%';
			}
		}

		result.push({
			view: view,
			dateformat: fromDate.format('ddd<br>DD MMM'),
			change: change > 0 ? '+'+ change : change,
			pct: pct != '0.0%' && pct.indexOf('-') == -1 ? '+'+ pct : pct,
			ballon_date: fromDate.format('DD MMM')
		});
	}
	result.shift();
	return result;
}

function getAvailableDate(datas) {
	var result = [];
	if (datas.length) {
		for (var i in datas) {
			var item = datas[i];
			result[i] = moment(item['date']).format('YYYY-MM-DD');
		}
	}

	return result;
}

function initCharts() {
	initPageViewsChart(pageViewDatas, 8, appViewToDate);
	initNewSignupChart(signupThisMonth, numDayFromBeginThisMonth);
	initRepeatVisitorChart(appRepeatDatas, 8, appRepeatToDate);
}

/* createNewSignupChartDatas
 * @param baseData : array (Data from model scoreds)
 * @param dateNum : int (Number date item should show in chart)
 * return array (dataProvider for chart)
 * */
function createNewSignupChartDatas(baseData, dateNum) {
	var result = []

	var listDayWithNumberSignup = [];

	for (var x in baseData) {
		var item = baseData[x],
			itemDay = moment(item['user_created']).format('D');
		if (typeof(listDayWithNumberSignup[itemDay]) == 'undefined') {
			listDayWithNumberSignup[itemDay] = 1;
		} else {
			listDayWithNumberSignup[itemDay]++;
		}
	}

	var maxValue = Object.keys(listDayWithNumberSignup).reduce(function(m, k){ return listDayWithNumberSignup[k] > m ? listDayWithNumberSignup[k] : m }, -Infinity),
		valueRemain = maxValue ? maxValue <= 10 ? 10 : (parseInt(((maxValue / 10).toFixed(0))) + 1) * 10 : 0;

	for (var i = 1; i <= dateNum; i++) {
		if (typeof(listDayWithNumberSignup[i]) != 'undefined') {
			result.push({day: i, value: listDayWithNumberSignup[i] - 0.1, remainingValue: valueRemain - listDayWithNumberSignup[i]});
		} else {
			result.push({day: i, value: 0, remainingValue: valueRemain});
		}
	}

	return result;
}

/* createRepeatVisitorDatas
 * @param baseData : array (Data from model appRepeat)
 * @param dateNum : int (Number date item should show in chart)
 * return array (dataProvider for chart)
 * */
function createRepeatVisitorDatas(baseData, dateNum, toDate) {
	var result = [],
		fromDate = moment(toDate).subtract(dateNum,'d'),
		hideCategoryNum = dateNum - 1 <= 18 || dateNum - 1 >= 70 ? 1 : dateNum - 1 <= 36 ? 2 : Math.floor((dateNum - 1) / 15);  // Hide some category title when number item too large

	var availableDate = getAvailableDate(baseData);
	for (var i = 0; i < dateNum; i++) {
		var itemDate = fromDate.add(1, 'day').format('YYYY-MM-DD'),
			itemData = '',
			repeatNum = 0,
			uniqueNum = 0,
			repeatChange = 0,
			uniqueChange = 0,
			repeatPct = '0.0%',
			uniquePct = '0.0%',
			repeatRatio = 0;

		if (availableDate.indexOf(itemDate) != -1) {
			itemData = baseData[availableDate.indexOf(itemDate)];
			repeatNum = itemData['user_login_count'];
			uniqueNum = itemData['user_signup_count'];
			if (i != 0) {
				repeatChange = itemData['user_login_count'] - result[i - 1]['repeatNum'];
				uniqueChange = itemData['user_signup_count'] - result[i - 1]['uniqueNum'];
				repeatPct = result[i - 1]['repeatNum'] == 0 && repeatNum == 0 ? '0.0%' : result[i - 1]['repeatNum'] == 0 ? '100.0%' : (repeatChange / result[i - 1]['repeatNum'] * 100).toFixed(1).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + '%';
				uniquePct = result[i - 1]['uniqueNum'] == 0 && uniqueNum == 0 ? '0.0%' : result[i - 1]['uniqueNum'] == 0 ? '100.0%' : (uniqueChange / result[i - 1]['uniqueNum'] * 100).toFixed(1).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + '%';
			}
		} else {
			if (i != 0) {
				repeatChange = 0 - result[i - 1]['repeatNum'];
				uniqueChange = 0 - result[i - 1]['uniqueNum'];
				repeatPct = result[i - 1]['repeatNum'] == 0 && repeatNum == 0 ? '0.0%' : result[i - 1]['repeatNum'] == 0 ? '100.0%' : (repeatChange / result[i - 1]['repeatNum'] * 100).toFixed(1).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + '%';
				uniquePct = result[i - 1]['uniqueNum'] == 0 && uniqueNum == 0 ? '0.0%' : result[i - 1]['uniqueNum'] == 0 ? '100.0%' : (uniqueChange / result[i - 1]['uniqueNum'] * 100).toFixed(1).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + '%';
			}
		}

		result.push({
			repeatNum: repeatNum,
			uniqueNum: uniqueNum,
			repeatChange: repeatChange > 0 ? '+'+ repeatChange : repeatChange,
			uniqueChange: uniqueChange > 0 ? '+'+ uniqueChange : uniqueChange,
			dateformat: fromDate.format('ddd<br>DD MMM'),
			repeatChange: repeatChange,
			uniqueChange: uniqueChange,
			repeatPct: repeatPct != '0.0%' && repeatPct.indexOf('-') == -1 ? '+'+ repeatPct : repeatPct,
			uniquePct: uniquePct != '0.0%' && uniquePct.indexOf('-') == -1 ? '+'+ uniquePct : uniquePct,
			repeatRatio: uniqueNum == 0 || repeatNum == 0 ? 0 : (repeatNum / uniqueNum).toFixed(2),
			ballon_date: fromDate.format('DD MMM')
		});
	}

	result.shift();
	return result;
}

// Init all chart in dashboard here
initCharts();

$('#appvisitors-click #repeat').on('click',function() {
    if ($(this).hasClass('hasUnique')){
        $(this).removeClass('hasUnique');
		$(this).text("Repeat Visitors");
		//appvisitors.graphs[0].bullet = "none";
		//appvisitors.graphs[0].labelText = "[[]]";
		appvisitors.graphs[0].showBalloon = false;
		//appvisitors.graphs[0].fillAlphas = 0.9;
		//appvisitors.graphs[1].bullet = "round";
		//appvisitors.graphs[1].labelText = "[[repeatNum]]";
		appvisitors.graphs[1].showBalloon = true;
		//appvisitors.graphs[1].fillAlphas = 1;

    } else {
        $(this).addClass('hasUnique');
		$(this).text("Unique Visitors");
		//appvisitors.graphs[0].bullet = "round";
		//appvisitors.graphs[0].labelText = "[[uniqueNum]]";
		appvisitors.graphs[0].showBalloon = true;
		//appvisitors.graphs[0].fillAlphas = 1;
		//appvisitors.graphs[1].bullet = "none";
		//appvisitors.graphs[1].labelText = "[[]]";
		appvisitors.graphs[1].showBalloon = false;
		//appvisitors.graphs[1].fillAlphas = 0.9;

    }
	appvisitors.validateData();
});

function newSignUpStyleHover() {
	nsups.addListener( "rollOverGraphItem", function( event ) {
		event.item.columnGraphics.node.setAttribute("class", "nsups-hover");
	} );
}

(function($) {
   $.fn.fixMe = function() {
      return this.each(function() {
         var $this = $(this),
            $t_fixed;
         function init() {
            $this.wrap('<div class="container" />');
            $t_fixed = $this.clone();
            $t_fixed.find("tbody").remove().end().addClass("fixed").insertBefore($this);
            resizeFixed();
         }
         function resizeFixed() {
            $t_fixed.find("th").each(function(index) {
               $(this).css("width",$this.find("th").eq(index).outerWidth()+"px");
            });
         }
         function scrollFixed() {
            var offset = $(this).scrollTop(),
            tableOffsetTop = $this.offset().top,
            tableOffsetBottom = tableOffsetTop + $this.height() - $this.find("thead").height();
            if(offset < tableOffsetTop || offset > tableOffsetBottom)
               $t_fixed.hide();
            else if(offset >= tableOffsetTop && offset <= tableOffsetBottom && $t_fixed.is(":hidden"))
               $t_fixed.show();
         }
         $(window).resize(resizeFixed);
         $(window).scroll(scrollFixed);
         init();
      });
   };
})(jQuery);
