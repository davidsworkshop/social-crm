tinyMCE.init({
	selector: "textarea.editwitheditor",
    menubar: false,
    plugins: ['textcolor', 'code'],
    toolbar: "undo redo | styleselect | bold italic | alignleft, aligncenter, alignright, alignjustify, bullist, numlist | forecolor backcolor | code",
    init_instance_callback : function(editor) {
        var ed = tinyMCE.activeEditor;
	    var ifr = tinyMCE.DOM.get(ed.id + '_ifr');
	    ed.dom.setAttrib(ifr, 'title', '');
	    $('[title]').tooltip();
     }
});