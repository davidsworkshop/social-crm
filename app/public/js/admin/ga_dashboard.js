//analytics  
	// Replace with your client ID from the developer console.
var CLIENT_ID = window.CONFIG.GA_CLIENT_ID_API_SUPERADMIN;
var VIEW_ID = ga_view_id;

  // Set authorized scope.
var SCOPES = ['https://www.googleapis.com/auth/analytics.readonly'];


function authorize(event) {
    // Handles the authorization flow.
    // `immediate` should be false when invoked from the button click.
    var useImmdiate = event ? false : true;
    var authData = {
      client_id: CLIENT_ID,
      scope: SCOPES,
      immediate: useImmdiate
    };    
    gapi.auth.authorize(authData, function(response) {
      var authButton = document.getElementById('auth-button');
      if (response.error) {
        authButton.hidden = false;
      }
      else {
        authButton.hidden = true;
        queryAccounts();
      }
    });
}

function queryAccounts() {
  // Load the Google Analytics client library.
  gapi.client.load('analytics', 'v3').then(function() {

    // Get a list of all Google Analytics accounts for this user
    gapi.client.analytics.management.accounts.list().then(handleAccounts);
  });
}

function handleAccounts(response) {
  // Handles the response from the accounts list method.
  if (response.result.items && response.result.items.length) {
    // Get the first Google Analytics account.
    var firstAccountId = response.result.items[0].id;

    // Query for properties.
    queryProperties(firstAccountId);
  } else {
    console.log('No accounts found for this user.');
  }
}

function queryProperties(accountId) {
  // Get a list of all the properties for the account.
  gapi.client.analytics.management.webproperties.list(
      {'accountId': accountId})
    .then(handleProperties)
    .then(null, function(err) {
      // Log any errors.
      console.log(err);
  });
}

function handleProperties(response) {
  // Handles the response from the webproperties list method.
  if (response.result.items && response.result.items.length) {

    // Get the first Google Analytics account
    var firstAccountId = response.result.items[0].accountId;

    // Get the first property ID
    var firstPropertyId = response.result.items[0].id;

    // Query for Views (Profiles).
    queryProfiles(firstAccountId, firstPropertyId);
  } else {
    console.log('No properties found for this user.');
  }
}

function queryProfiles(accountId, propertyId) {
  // Get a list of all Views (Profiles) for the first property
  // of the first Account.
  gapi.client.analytics.management.profiles.list({
      'accountId': accountId,
      'webPropertyId': propertyId
  })
  .then(handleProfiles)
  .then(null, function(err) {
      // Log any errors.
      console.log(err);
  });
}

function handleProfiles(response) {
  // Handles the response from the profiles list method.
  if (response.result.items && response.result.items.length) {
    // Get the first View (Profile) ID.
    var firstProfileId = response.result.items[0].id;

    // Query the Core Reporting API.
    queryCoreReportingApi(firstProfileId);
  } else {
    console.log('No views (profiles) found for this user.');
  }
}

function queryCoreReportingApi(profileId) {

	gapi.client.request({
	      path: '/v4/reports:batchGet',
	      root: 'https://analyticsreporting.googleapis.com/',
	      method: 'POST',
	      body: {
	        reportRequests: [
	          {
	            viewId: VIEW_ID,
	            dateRanges: [
                 	{
                 		startDate: '2016-01-01',
                 		endDate: 'today'
             		}
	            ],           
	            dimensions:[
                	{
                		name:"ga:userAgeBracket"
            		}
              	]              	
	          }
	        ]
	      }
	    }).then(function(res) {
	    	$('#auth-button').hide();
		    var total = res.result.reports[0].data.totals[0].values[0];
		    var dataRows = res.result.reports[0].data.rows;
		    if(dataRows != null)
	    	{
			    var data18_24 = 0;
			    var data25_34 = 0;
			    var data35_44 = 0;
			    var data45_54 = 0;
			    var data55_64 = 0;
			    var data65 = 0;
			    for(i = 0; i<dataRows.length; i ++) {
			    	if(dataRows[i]['dimensions'][0] == "18-24") {
			    		data18_24 = dataRows[i]['metrics'][0].values[0];
			    	}
			    	
			    	if(dataRows[i]['dimensions'][0] == "25-34") {
			    		data25_34 = dataRows[i]['metrics'][0].values[0];
			    	}
			    	
			    	if(dataRows[i]['dimensions'][0] == "35-44") {
			    		data35_44 = dataRows[i]['metrics'][0].values[0];
			    	}
	
			    	if(dataRows[i]['dimensions'][0] == "45-54") {
			    		data45_54 = dataRows[i]['metrics'][0].values[0];
			    	}
			    	
			    	if(dataRows[i]['dimensions'][0] == "55-64") {
			    		data55_64 = dataRows[i]['metrics'][0].values[0];
			    	}
			    	
			    	if(dataRows[i]['dimensions'][0] == "65+") {
			    		data65 = dataRows[i]['metrics'][0].values[0];
			    	}
			    	 
			    }		    		   
			    		     
			    //fill chart		    		   		    		  
			    $('#data1824').text( Math.round(dataAnalyticsHtml(data18_24,total)) + "%" );
			    $('#data2534').text( Math.round(dataAnalyticsHtml(data25_34,total)) + "%" );
			    $('#data3544').text( Math.round(dataAnalyticsHtml(data35_44,total)) + "%" );
			    $('#data4554').text( Math.round(dataAnalyticsHtml(data45_54,total)) + "%" );
			    $('#data5564').text( Math.round(dataAnalyticsHtml(data55_64,total)) + "%" );
			    $('#data65').text( dataAnalyticsHtml(data65,total) + "%" );
			    
			    var agebreak = AmCharts.makeChart( "agebreak", {
			    	  "type": "pie",
			    	  "theme": "light",
			    	  "fontFamily": "'NotoSans' !important",
			    	  "dataProvider": [
			    	  {
			    	    "title": "18 - 24",
			    	    "value": data18_24,
			    	    "color": "#FAC8BF"
			    	  },
			    	  {
			    	    "title": "25 - 34",
			    	    "value": data25_34,
			    	    "color": "#6C9C7F"
			    	  },
			    	  {
			    	    "title": "35 - 44",
			    	    "value": data35_44,
			    	    "color": "#6A8D9D"
			    	  },
			    	  {
			    	    "title": "45 - 54",
			    	    "value": data45_54,
			    	    "color": "#6C9C7F"
			    	  },
			    	  {
			    	    "title": "55 - 63",
			    	    "value": data55_64,
			    	    "color": "#FAE3BF"
			    	  },
			    	  {
			    	    "title": "65 and Above",
			    	    "value": data65,
			    	    "color": "#FCB02A"
			    	  },],
			    	  "titleField": "title",
			    	  "valueField": "value",
			    	  "labelRadius": 5,
			    	  "balloon": {
			    			"adjustBorderColor": false,
			    			"borderColor": "#515974",
			    			"borderThickness": 0,
			    			"color": "#EAEEF5",
			    			"cornerRadius": 8,
			    			"disableMouseEvents": false,
			    			"fadeOutDuration": 0,
			    			"fillAlpha": 1,
			    			"fillColor": "#515974",
			    			"fontSize": 15,
			    			"horizontalPadding": 14,
			    			"pointerWidth": 0,
			    			"shadowAlpha": 0.04,
			    			"verticalPadding": 10,
			    			"fixedPosition": false,
			    			"pointerOrientation": "up",
			    		},
			    	  "radius": "42%",
			    	  "innerRadius": "65%",
			    	  "labelText": "",
			    	  "export": {
			    	    "enabled": true
			    	  }
			    	} );
	    	}
		    
		    //get BounceRate
		    gapi.client.request({
		        path: '/v4/reports:batchGet',
		        root: 'https://analyticsreporting.googleapis.com/',
		        method: 'POST',
		        body: {
		          reportRequests: [
		            {
		              viewId: VIEW_ID,
		              dateRanges: [
		                {
		                  startDate: '2016-01-01',
		                  endDate: 'today'
		                }
		              ],                       
		  			metrics:[
		                 {
		                   expression:"ga:bounceRate"            	             	  
		                 }
		  			]          	                    
		            }
		          ]
		        }
		      }).then(function (res) {		    	  
		    	  var dataBounceRate = Math.round(res.result.reports[0].data.totals[0].values[0]);
		    	  $('#dataBounceRate').text(dataBounceRate + '%');
		    	  
		    	  var chart = AmCharts.makeChart( "bouncerate", {
		    		  "type": "pie",
		    		  "theme": "light",
		    		  "fontFamily": "'NotoSans' !important",
		    		  "balloonText": "<span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
		    		  "dataProvider": [
		    				{
		    					"category": "op-1",
		    					"column-1": dataBounceRate,
		    					"color": "#9DDDEE"
		    				},
		    				{
		    					"category": "op-2",
		    					"column-1": (100 - dataBounceRate),
		    					"color": "#00BBEC"
		    				}
		    			],
		    			"balloon": {
		    				"adjustBorderColor": false,
		    				"borderColor": "#515974",
		    				"borderThickness": 0,
		    				"color": "#EAEEF5",
		    				"cornerRadius": 8,
		    				"disableMouseEvents": false,
		    				"fadeOutDuration": 0,
		    				"fillAlpha": 1,
		    				"fillColor": "#515974",
		    				"fontSize": 15,
		    				"horizontalPadding": 14,
		    				"pointerWidth": 0,
		    				"shadowAlpha": 0.04,
		    				"verticalPadding": 10,
		    				"fixedPosition": false,
		    				"pointerOrientation": "up",
		    			},
		    		  "titleField": "title",
		    		  "valueField": "column-1",
		    		  "labelRadius": 5,
		    		  "colorField": "color",
		    		  "radius": "42%",
		    		  "innerRadius": "65%",
		    		  "labelText": "",
		    		  "export": {
		    		    "enabled": true
		    		  }
		    		});
		      });
		    
		    
		    //get Affinity
		    
		    gapi.client.request({
		        path: '/v4/reports:batchGet',
		        root: 'https://analyticsreporting.googleapis.com/',
		        method: 'POST',
		        body: {
		          reportRequests: [
		            {
		              viewId: VIEW_ID,
		              dateRanges: [
		                {
		                  startDate: '2016-01-01',
		                  endDate: 'today'
		                }
		              ],                       
		              dimensions:[
		                 {
		                	 name: 'ga:interestAffinityCategory'            	             	  
		                 }
		  			]          	                    
		            }
		          ]
		        }
		      }).then(function (response) {		    	  
		    	  	var result = [];
	    	  		var data = response.result.reports[0].data.rows;
		    	    var totals = response.result.reports[0].data.totals[0].values;		    	    
		    	    for(key in data) {       
		    	        var items = {name: data[key].dimensions[0], value: data[key].metrics[0].values[0] }
		    	        result.push(items);
		    	    }
		    	    result.sort(function(a, b) {
		    	        return parseFloat(a.value) - parseFloat(b.value);
		    	    });
		    	    
		    	    
		    	   
		    	    var dataAffinity = []; 
		    	    var countAffinity  = 0;
		    	    for(i = result.length - 1 ; i >= 0 ; i--) {
		    	    	if( countAffinity < window.CONFIG.DASHBOARD_ADMIN_MAX_AFFINITY ) {
		    	    		var objectAffinity = {
		    	    				percents: dataAnalyticsHtml(result[i].value, totals),
		    	    				info: dataAnalyticsHtml(result[i].value, totals)+'%',
		    	    				color: window.CONFIG.DASHBOARD_ADMIN_COLOR_AFFINITY[countAffinity],
		    	    				decs: result[i].name
		    	    				};
		    	    		dataAffinity.push(objectAffinity);
		    	    	}else {
		    	    		break;
		    	    	}
		    	    	countAffinity++;		    	    	
		    	    }
		    	    
		    	    var affreach = AmCharts.makeChart("affreach",
		    	    		{
		    	    			"type": "serial",
		    	    			"categoryField": "info",
		    	    			"rotate": true,
		    	    			"autoMarginOffset": 15,
		    	    			"autoMargins": false,
		    	    			"marginLeft": 60,
		    	    			"marginRight": 300,
		    	    			"plotAreaBorderColor": "",
		    	    			"startEffect": "bounce",
		    	    			"addClassNames": true,
		    	    			"processCount": 999,
		    	    			"processTimeout": -3,
		    	    			"theme": "light",
		    	    			"categoryAxis": {
		    	    				"position": "left",
		    	    				"axisThickness": 0,
		    	    				"gridAlpha": 0,
		    	    				"gridColor": "#FFFFFF",
		    	    				"gridCount": 0,
		    	    				"gridThickness": 0,
		    	    				"minorGridAlpha": 0,
		    	    				"minorTickLength": 1,
		    	    				"offset": 10,
		    	    				"tickLength": -4,
		    	    				"titleRotation": 0
		    	    			},
		    	    			"trendLines": [],
		    	    			"graphs": [
		    	    				{
		    	    					"accessibleLabel": "[[title]] [[percents]] [[value]]",
		    	    					"balloonText": "[[info]]",
		    	    					"colorField": "color",
		    	    					"columnWidth": 0.46,
		    	    					"cornerRadiusTop": 5,
		    	    					"dashLength": 3,
		    	    					"fillAlphas": 1,
		    	    					"fillColors": "#7E5D7E",
		    	    					"id": "affinity-reach",
		    	    					"labelPosition": "right",
		    	    					"labelText": "[[decs]]",
		    	    					"lineAlpha": 0,
		    	    					"lineColor": "#7E5D7E",
		    	    					"lineThickness": 0,
		    	    					"minDistance": 0,
		    	    					"periodSpan": 2,
		    	    					"pointPosition": "",
		    	    					"showOnAxis": true,
		    	    					"title": "",
		    	    					"topRadius": 0,
		    	    					"type": "column",
		    	    					"valueField": "percents"
		    	    				}
		    	    			],
		    	    			"guides": [],
		    	    			"valueAxes": [
		    	    				{
		    	    					"id": "va-affinity-reach",
		    	    					"position": "right",
		    	    					"stackType": "regular",
		    	    					"zeroGridAlpha": 1,
		    	    					"autoGridCount": false,
		    	    					"axisAlpha": 0,
		    	    					"axisColor": "#FFFFFF",
		    	    					"axisThickness": 0,
		    	    					"gridColor": "",
		    	    					"gridCount": 0,
		    	    					"gridThickness": 0,
		    	    					"title": ""
		    	    				}
		    	    			],
		    	    			"allLabels": [],
		    	    			"balloon": {},
		    	    			"titles": [],
		    	    			"dataProvider": dataAffinity 
		    	    		});
		    	    
		    	    //get links in
		    	    gapi.client.request({
				        path: '/v4/reports:batchGet',
				        root: 'https://analyticsreporting.googleapis.com/',
				        method: 'POST',
				        body: {
				          reportRequests: [
				            {
				              viewId: VIEW_ID,
				              dateRanges: [
				                {
				                  startDate: '2016-01-01',
				                  endDate: 'today'
				                }
				              ],                       
				              dimensions:[
				                 {
				                	 name: 'ga:fullReferrer'            	             	  
				                 }
				  			]          	                    
				            }
				          ]
				        }
				      }).then(function (response) {				    	  
							var data = response.result.reports[0].data.rows;				    	  
							var html = '';				    	  
							var result = [];
							for(key in data) {       
							    var items = {name: data[key].dimensions[0], value: data[key].metrics[0].values[0] }
							    result.push(items);
							}
							result.sort(function(a, b) {
								return parseFloat(a.value) - parseFloat(b.value);
							});
					
							var countlink = 0;
							for(i = result.length - 1 ; i >= 0 ; i--) {
								if(countlink == window.CONFIG.DASHBOARD_ADMIN_MAX_LINKSIN)
									break;
							 html+='<tr>';
				    			html+='<td><p class="pleft-intble"><strong>'+result[i].name+'</strong></p></td>';
				    			html+='<td><p class="justify-numb">'+result[i].value+'</p></td>';
			    			html+='</tr>';
			    			countlink++;
							}					 
				    	  $('tbody.linksin').html('');
				    	  $('tbody.linksin').html(html);
				      });
		    	    		    	     
		      });
		    
		     
	    }, console.error.bind(console));
}

function dataAnalyticsHtml (number, total) {
	if(number == 0)
		return 0;
	return ((number/total)*100).toFixed(2);
}

// Add an event listener to the 'auth-button'.
	document.getElementById('auth-button').addEventListener('click', authorize);  