$(document).ready(function() {
	$(document)
	.on('click', '.btn-drp', toggleDropdown)
	.on('click', '.dropdown-lst li', selectDropdown)
	.on('click', '.question-form .save-btn', submitQuestionForm)
	.on('click', '.question-form .post-btn', submitQuestionForm)
	.on('click', '.bonus-form .save-btn', submitBonusForm)
	.on('click', '.bonus-form .post-btn', submitBonusForm)
	.on('click', '.preview-question', previewQuestion)
	.on('click', '.preview-bonus', previewBonus)
	.on('click', '.question-table .icon-icon-menu, .bonus-table .icon-icon-menu', toggleQuestion)
	.on('click', '.cancel-btn', function() { $(this).closest('tr').toggle(); })
	.on('submit', '.question-delete-form', submitDeleteQuestion)
	.on('click' , 'a.header-main', function(){
		if ($('a.header-main').hasClass('active')){
			$('#navMenus').slideDown();
			$(this).removeClass('active');
		} else {
			$('#navMenus').slideUp();
			$(this).addClass('active');
		}
	})
	.on('click', '.crop-btn', cropQuestionImg)
	.on('shown.bs.modal', '#modal-upload', showCropAfterOpenModal)

	$( ".sortable-list" ).sortable({
		axis: "y",
		stop: function( event, ui ) {
			var item = ui.item[0]
			if ($(item).hasClass('question-table')) {
				sortableData('question');
			} else if ($(item).hasClass('bonus-table')) {
				sortableData('bonus');
			}
		},
		helper: function(event, ui){
			var $clone =  $(ui).clone();
			$clone .css('position','absolute');
			return $clone.get(0);
		},
	}).disableSelection();

	$(".question-form input:file[name=file], .bonus-form input:file[name=file]").change(function(e) {
		var form = $(this).closest('form');
		form.find('.file-name').html('');
		form.find('.error-message').html('');
		form.find('input[name=img_base64]').val('');
		$('.croppie-container').html('');
	    var image, file;

	    if ((file = this.files[0])) {
	    	var acceptMimeType = ['image/jpeg', 'image/pjpeg', 'image/png', 'image/gif'];

			if (acceptMimeType.indexOf(file.type) == -1) {
				alert('Image file type is not accept.');
				form.find("input:file[name=file]").val('');
				return false;
			}

	        // Check image Dimensions
	        image = new Image();
	        image.onload = function() {
	        	//if (this.width < CONFIG.questionImageWidth || this.height < CONFIG.questionImageHeight) {
	        	//	alert("Dimensions of image must be at least "+ CONFIG.questionImageWidth +" x "+ CONFIG.questionImageHeight);
	        	//	form.find("input:file[name=file]").val('');
	        	//	if (form.find('input[name=qs_current_image]').length) {
	        	//		form.find('.question-image').attr('src', form.find('input[name=qs_current_image]').val());
	        	//	}
	        	//} else {
	        		// Show image name
	        		form.find('.file-name').html(file.name);
	        		var reader = new FileReader();
	    	        reader.onload = function (e) {
	    	        	$('#modal-upload').modal('show');
	    	        	// Reset input file to choose same image if not crop
	    	        	form.find('input[name=file]').replaceWith( form.find('input[name=file]').clone( true ) );
						
	    	        	// Delay 1 second to wait modal load finish then init crop preview
						setTimeout(function() {
							$uploadCrop.croppie('bind', {
					    		url: e.target.result
					    	}).then(function(){
					    		console.log('jQuery bind complete');
					    	});
						}, 1000)
	    	        }
	    	        reader.readAsDataURL(file);
	        //	}

	        };
	        image.src = _URL.createObjectURL(file);
	    } else if (form.find('input[name=qs_current_image]').length) {
	    	form.find('.question-image').attr('src', form.find('input[name=qs_current_image]').val());
	    }
	});

	// Start change image in general page
	$('input:file[name=profile_file], input:file[name=intro_file]').change(function(e) {
		var file,
			showImgElement = $(this).attr("name") == 'profile_file' ? "#img_profile" : "#img_intro";

		if ((file = this.files[0])) {
    		// Show preview image
    		var reader = new FileReader();
	        reader.onload = function (e) {
	        	$(showImgElement).css('display','block');
	        	$(showImgElement).attr('src', e.target.result);
	        }
	        reader.readAsDataURL(file);
		} else {
			$(showImgElement).attr('src', $(this).closest('div').find('input[type=hidden]').val());
		}

	});
	// End change image in general page

	// Start change image in theme page
	$('input:file[name=theme_destop_bg]').change(function(e) {
		var file;

		if ((file = this.files[0])) {
    		// Show preview image
    		var reader = new FileReader();
	        reader.onload = function (e) {
	        	$('.img-theme img').attr('src', e.target.result);
	        }
	        reader.readAsDataURL(file);
		} else {
			$('.img-theme img').attr('src', $('input[name=currentThemeImg]').val());
		}
	});
	//end upload image function


	// End change image in theme page

	function toggleDropdown(e) {
		$(this).next('ul').fadeToggle();
	}

	function selectDropdown(e) {
		$(this).closest('div').find('button').text($(this).text());
		$(this).closest('ul').fadeToggle();
		$(this).siblings().removeClass('active');
		$(this).addClass('active');
	}
	function submitQuestionForm(e) {
		if (window.submitted != undefined && window.submitted) {
			e.preventDefault();
			return false;
		}
		
		var status = 0;
		if ($(this).hasClass('post-btn')) { status = 1 }
		var form = $(this).closest('form');
		form.find('input[name=qs_status]').val(status);
		var validForm = validQuestionForm(form);

		if (! validForm) {
			e.preventDefault();
		} else {
			window.submitted = true;
		}
	}

	function validQuestionForm(form) {
		var title = form.find('input[name=qs_title]').val().trim(),
			question = form.find('textarea[name=qs_question]').val().trim(),
			explain = form.find('textarea[name=qs_explain]').val().trim(),
			answer = form.find('input[name=qs_answer]:checked').val(),
			font_color = form.find('input[name=font_color]:checked').val(),
			file = form.find('input[name=img_base64]').val(),
			valid = true;

		form.find('.error-message').html('');

		if (! title) {
			form.find('.error-message').append('<p class="text-danger">The title field is empty.</p>');
			valid = false;
		}

		if (! question) {
			form.find('.error-message').append('<p class="text-danger">The question field is empty.</p>');
			valid = false;
		}

		if (! explain) {
			form.find('.error-message').append('<p class="text-danger">The Explanation for answer field is empty.</p>');
			valid = false;
		}

		if (! answer) {
			form.find('.error-message').append('<p class="text-danger">Please select correct answer.</p>');
			valid = false;
		}
		
		if (! font_color) {
			form.find('.error-message').append('<p class="text-danger">Please select font colour.</p>');
			valid = false;
		}

		if (! form.find('input[name=qs_id]').length && ! file) {
			form.find('.error-message').append('<p class="text-danger">Please select question image and crop.</p>');
			valid = false;
		}

		return valid;
	}

	function previewQuestion(e) {
		e.preventDefault();
		var form = $('.question-form'),
			validForm = validQuestionForm(form);

		if (validForm) {
			var title = form.find('input[name=qs_title]').val().trim(),
				question = form.find('textarea[name=qs_question]').val().trim(),
				explain = form.find('textarea[name=qs_explain]').val().trim(),
				answer = form.find('input[name=qs_answer]:checked').val(),
				font_color = form.find('input[name=font_color]:checked').val(),
				file = form.find('input[name=img_base64]').val(),
				reader = new FileReader();

			// Preview with new choose image
			if (file) {
				$.ajax({
                    type: 'POST',
                    url: window.location.origin +'/'+ appUrl +'/preview-question',
                    data: {
                    	title: title,
                    	question: question,
                    	explain: explain,
                    	answer: answer,
                    	font_color: font_color,
                    	img: file
                    },
                    dataType: 'json',
                    success: function(data) {
                        window.open(window.location.origin +'/'+ appUrl +'/preview-question');
                    }
                });
		    // Preview with current image
			} else {
				file = form.find('input[name=qs_current_image]').val();
				$.ajax({
                    type: 'POST',
                    url: window.location.origin +'/'+ appUrl +'/preview-question',
                    data: {
                    	title: title,
                    	question: question,
                    	explain: explain,
                    	answer: answer,
                    	font_color: font_color,
                    	img: file
                    },
                    dataType: 'json',
                    success: function(data) {
                        window.open(window.location.origin +'/'+ appUrl +'/preview-question');
                    }
                });
			}
		}
	}

	function previewBonus(e) {
		e.preventDefault();
		var form = $('.bonus-form'),
			validForm = validBonusForm(form);

		if (validForm) {
			var title = form.find('input[name=bonus_title]').val().trim(),
				caption = form.find('textarea[name=bonus_caption]').val().trim(),
				fb_link = form.find('textarea[name=bonus_fb_link]').val().trim(),
				required_action = form.find('input[name=bonus_action]:checked').val(),
				background = form.find('input[name=background]:checked').val(),
				point = form.find('input[name=bonus_point]').val().trim(),
				file = form.find('input[name=img_base64]').val(),
				reader = new FileReader();

			// Preview with new choose image
			if (file) {
				$.ajax({
                    type: 'POST',
                    url: window.location.origin +'/'+ appUrl +'/preview-bonus',
                    data: {
                    	title: title,
                    	caption: caption,
                    	fb_link: fb_link,
                    	required_action: required_action,
                    	background: background,
                    	point: point,
                    	img: file,
                    },
                    dataType: 'json',
                    success: function(data) {
                        window.open(window.location.origin +'/'+ appUrl +'/preview-bonus');
                    }
                });
		    // Preview with current image
			} else {
				file = form.find('input[name=qs_current_image]').val();
				$.ajax({
                    type: 'POST',
                    url: window.location.origin +'/'+ appUrl +'/preview-bonus',
                    data: {
                    	title: title,
                    	caption: caption,
                    	fb_link: fb_link,
                    	required_action: required_action,
                    	background: background,
                    	point: point,
                    	img: file,
                    },
                    dataType: 'json',
                    success: function(data) {
                        window.open(window.location.origin +'/'+ appUrl +'/preview-bonus');
                    }
                });
			}
		}
	}

	function submitBonusForm(e) {
		if (window.submitted != undefined && window.submitted) {
			e.preventDefault();
			return false;
		}
		
		var status = 0;
		if ($(this).hasClass('post-btn')) { status = 1 }
		var form = $(this).closest('form');
		form.find('input[name=bonus_status]').val(status);
		var validForm = validBonusForm(form);

		if (! validForm) {
			e.preventDefault();
		} else {
			window.submitted = true;
		}
	}

	function validBonusForm(form) {
		var title = form.find('input[name=bonus_title]').val().trim(),
			caption = form.find('textarea[name=bonus_caption]').val().trim(),
			fb_link = form.find('textarea[name=bonus_fb_link]').val().trim(),
			required_action = form.find('input[name=bonus_action]:checked').val(),
			background = form.find('input[name=background]:checked').val(),
			point = form.find('input[name=bonus_point]').val().trim(),
			file = form.find('input[name=img_base64]').val(),
			valid = true;

		form.find('.error-message').html('');

		if (! title) {
			form.find('.error-message').append('<p class="text-danger">The title field is empty.</p>');
			valid = false;
		}

		if (! caption) {
			form.find('.error-message').append('<p class="text-danger">The caption field is empty.</p>');
			valid = false;
		}

		if (! fb_link) {
			form.find('.error-message').append('<p class="text-danger">The facebook Link field is empty.</p>');
			valid = false;
		}

		if (! required_action) {
			form.find('.error-message').append('<p class="text-danger">Please select required action.</p>');
			valid = false;
		}
		
		if (! background) {
			form.find('.error-message').append('<p class="text-danger">Please select background.</p>');
			valid = false;
		}

		if (! point) {
			form.find('.error-message').append('<p class="text-danger">The bonus points field is empty.</p>');
			valid = false;
		}

		if (! form.find('input[name=bonus_id]').length && ! file) {
			form.find('.error-message').append('<p class="text-danger">Please select bonus image and crop.</p>');
			valid = false;
		}

		return valid;
	}

	function toggleQuestion(e) {
		var $this = $(this);

		$this.closest('tr').next('tr').fadeToggle();
	}

	function submitDeleteQuestion(e) {
		if (confirm("Do you want delete it?") != true) {
			return false;
		}
	}

	function sortableData(table) {
		var tables = '',
			listOrder = [];

		if (table == 'question') {
			tables = $('table.list-question');
		} else if (table == 'bonus') {
			tables = $('table.list-bonus');
		}
		tables.each(function(index, item) {
			listOrder.push($(item).attr('data-id'));
		});
		$.ajax({
            type: 'POST',
            url: window.location.origin +'/'+ appUrl +'/admin/sortable-data',
            data: {
            	list_order: listOrder,
            	table: table,
            	qs_index_list: qsIndexList,
            	bn_index_list: bnIndexList
            },
            dataType: 'json',
            success: function(data) {
                //console.log(data)
            }
        });
	}

	/* Phuc add export */

	function tableToJson(table) {
	    var data = [];
	    var headers = [];
	    for (var i=0; i<table.rows[0].cells.length; i++) {
	        headers[i] = table.rows[0].cells[i].innerHTML.toLowerCase().replace(/ /gi,'');
	    }
	    for (var i=1; i<table.rows.length; i++) {
	        var tableRow = table.rows[i];
	        var rowData = {};
	        for (var j=0; j<tableRow.cells.length; j++) {
	            rowData[ headers[j] ] = tableRow.cells[j].innerHTML;
	        }
	        data.push(rowData);
	    }
	    return data;
	}

	$('.btnexportquestion li a').click(function (e) {
	    if($(this).text() == 'PDF') {
	    	$('#export_type').val('PDF');
	    	$( "#frmapplication_question" ).submit();
        	
	    }else if($(this).text() == 'Excel') {
	    	var isSafari = navigator.vendor && navigator.vendor.indexOf('Apple') > -1 &&
	    	navigator.userAgent && !navigator.userAgent.match('CriOS');
	    	 if(isSafari) {
			    	$( "#frmapplication_question" ).submit();
			    }else {			    
					var blobURL = tableToExcel('exportquestion', 'table_export');
					$(this).attr('download','Export-Question-XLS.xls')
					$(this).attr('href',blobURL);
			    }
		}
	});
	
	$('.btnexportbonus li a').click(function (e) {
		if($(this).text() == 'PDF') {
	    	$('#export_type_bonus').val('PDF');
	    	$( "#frmapplication_bonus" ).submit();
	    }else {
	    	$('#export_type').val('EXCEL')		   
		    var isSafari = navigator.vendor && navigator.vendor.indexOf('Apple') > -1 &&
            navigator.userAgent && !navigator.userAgent.match('CriOS');
		    
		    if(isSafari) {
		    	$( "#frmapplication_bonus" ).submit();
		    }else {
				    var blobURL = tableToExcel('exportbonus', 'tab_export');
				    $(this).attr('download','Export-Bonus-Round-XLS.xls')
				    $(this).attr('href',blobURL);			  
		    } 
	    }
	});


	$('#today-pusers-export li a').click(function(e) {
		if($(this).text() == 'PDF') {
			$('#export_type').val('PDF')
			$( "#frmexport" ).submit();
			
			
		}else if($(this).text() == 'Excel') {
			$('#export_type').val('EXCEL')		   
		    var isSafari = navigator.vendor && navigator.vendor.indexOf('Apple') > -1 &&
            navigator.userAgent && !navigator.userAgent.match('CriOS');
		 
		    if(isSafari) {
		    	$("#frmexport" ).submit();
		    }else {
				    var blobURL = tableToExcel('xls', 'tabl_export');
				    $(this).attr('download','Export-Power-User-XLS.xls')
				    $(this).attr('href',blobURL);			  
		    } 
		    		
		}
		
	});
	
	var tableToExcel = (function() {
		  var uri = 'data:application/vnd.ms-excel;base64,'
		    , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--><meta http-equiv="content-type" content="text/plain; charset=UTF-8"/></head><body>{table}</body></html>'
		    , base64 = function(s) { return window.btoa(unescape(encodeURIComponent(s))) }
		    , format = function(s, c) { return s.replace(/{(\w+)}/g, function(m, p) { return c[p]; }) }
		  return function(table, name) {
		    if (!table.nodeType) table = document.getElementById(table)
		    var ctx = {worksheet: name || 'Worksheet', table: table.innerHTML}
		    var blob = new Blob([format(template, ctx)]);
		  var blobURL = window.URL.createObjectURL(blob);
		    return blobURL;
		  }
		})()

	function printDiv(divId) {

	       var printContents = document.getElementById(divId).innerHTML;
	       var originalContents = document.body.innerHTML;
	       document.body.innerHTML = "<html><head><title></title></head><body>" + printContents + "</body>";
	       window.print();
	       document.body.innerHTML = originalContents;
	}
	
	function cropQuestionImg() {
		$uploadCrop.croppie('result', {
			type: 'canvas',
			size: {width: 606,height: 1080},
		}).then(function (resp) {
			$('.question-image').attr('src', resp);
			$('.resuft-crop').removeClass('hidden');
			$('#modal-upload').modal('hide');
			$('.question-form input[name=img_base64]').val(resp);
			$('.bonus-form input[name=img_base64]').val(resp);
		});
	}
	
	function showCropAfterOpenModal() {
		$uploadCrop = $('.croppie-container').croppie({
			viewport: {
				width: 309,
				height: 550,
			},
			boundary: {
				width: 'auto',
				height: 550,
			},
			enableExif: true,
		});
	}

	$('.btnexportdashboard').click(function() {
		var htmlRow = '<tr class="rowprint"><td></td>';
	       htmlRow+= '<td>Points</td>';
	       htmlRow+='<td>Friends</td>';
	       htmlRow+='<td>Views</td>';
	       htmlRow+='<td>Shares</td>';
	       htmlRow+='<td>Likes</td></tr>';
	       $('#pwusertb thead').append(htmlRow);
		  var printContents = document.getElementById('contextExport').innerHTML;
		  window.print();
		  $('#pwusertb thead tr').last().remove();
	});
});
var _URL = window.URL || window.webkitURL;
var $uploadCrop;
