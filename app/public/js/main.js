$(document).ready(function() {
	//store the element
	var $cache = $('.you').css('height');

	//store the initial position of the element
	var vTop = $cache;
	  $("#everyone").scroll(function (event) {
	    // what the y position of the scroll is
	    var listItem = $( ".you" ).index( ".va-slice" );
		var top = vTop.slice(0, -2)*(listItem);

		var $heightenyone = $('#everyone').css('height').slice(0,-2);
	    var y = parseInt($(this).scrollTop()) + parseInt($heightenyone) - parseInt(vTop.slice(0, -2));
	    // whether that's below the form
	    if (y >= top) {
	      // if so, ad the fixed class
	      $('.you').addClass('stuck');
	    } else {
	      // otherwise remove it
	      $('.you').removeClass('stuck');
	    }
	});
	  
	$(document)
		.on('submit', '#invite-form', inviteFriend)
		.on('click', '#va-accordion .leaderboard-font, #friends .nolist img', function() {
			$('#inviteModal').modal('show');
		})
		
	function inviteFriend(e) {
		e.preventDefault();
		$('img.loading').removeClass('hidden');
		$("#invite-form .invite-btn input").prop('disabled', true);
		$("#invite-form .text-success").addClass("hidden");
		
		if(!isEmail($('#email').val())) {
			$('#email').val('');
			$("#invite-form .text-danger").removeClass("hidden").text("Email not valid.");
			$('img.loading').addClass('hidden');
			$("#invite-form .invite-btn input").prop('disabled', false);
			return false;
		} else {
			var app_url = $('input[name="app_url"]').val();
			$.ajax({
				method: "POST",
				url: app_url+ "/invite-friend",
				data: { email: $('#email').val() }
			})
			.done(function( msg ) {
				if (msg.success == -1) {
					$("#invite-form .text-danger").removeClass("hidden").text("Registered email.");
				} else if (! msg.success) {
					$("#invite-form .text-danger").removeClass("hidden").text("Send email not success.");
				} else if (msg.success == 1) {
					$("#invite-form .text-danger").addClass("hidden");
					$("#invite-form .text-success").removeClass("hidden").text("Email sent successfully.");
				}
				$('img.loading').addClass('hidden');
				$("#invite-form .invite-btn input").prop('disabled', false);
			});
		}
	}
	
	//check has friends on leaderboard
	if( $('#friends').children('.va-slice').length == 0 ) {
		$('.tab-leaderboard li a').click(function(){
			var hrefFriends = $(this).attr("href");
			if( $(this).attr("href") == '#friends' ){
				$('.leaderboard-font').hide();
			} else {
				$('.leaderboard-font').show();
			} 
	  });
	}
});



/* Set the width of the side navigation */
function openNav() {
	if (! $('#mySidenav').length) {	return false; }

	document.getElementsByClassName("icon-icon-menu")[0].style.display = "none";	// Hide menu icon

	// For device width small then ipad we define slide nav width fit with device
	if ($(window).width() < 480 ) {
		document.getElementById("mySidenav").style.width = "85%";
		return;
	}
	else if ($(window).width() < 768) {
		document.getElementById("mySidenav").style.width = "80%";
		return;
	}

    document.getElementById("mySidenav").style.width = "70%";
}

/* Set the width of the side navigation to 0 */
function closeNav() {
	if (! $('#mySidenav').length) {	return false; }
	document.getElementsByClassName("icon-icon-menu")[0].style.display = "block";	// show menu icon
    document.getElementById("mySidenav").style.width = "0";
}

// Close sidenav for mobile when click outside
document.documentElement.addEventListener("touchend", function(e){
	if (! $(e.target).closest('#mySidenav').length && e.target.id != "menu-icon") {
    	closeNav();
    }
}, true);

$('#iconChecksubmit').click(function() {
	$( "#formsubmit" ).submit();
});




$('#OpenImgUpload').click(function(){ $('input[name="profile_file"]').trigger('click'); });

$('input:file[name=profile_file]').change(function(e) {
	if ((file = this.files[0])) {
		var reader = new FileReader();
        reader.onload = function (e) {
        	//$('#img_intro').css('display','block');
        	$('#OpenImgUpload').attr('src', e.target.result);
        }
        reader.readAsDataURL(file);
	}else {
		$('#OpenImgUpload').attr('src', $('#img_profile').attr('src'));
	}
});