$('.btnexportdashboard').click(function() {
	$('.dataTables_scrollBody').css('overflow','inherit');
	$('.dataTables_scrollHead').css('overflow','inherit');
	$('#today-pusers').css('display','none');
	
	var htmlRow = '<tr class="rowprint"><td></td>';
    htmlRow+= '<td>Points</td>';
    htmlRow+='<td>Friends</td>';
    htmlRow+='<td>Views</td>';
    htmlRow+='<td>Shares</td>';
    htmlRow+='<td>Likes</td></tr>';
    $('#pwusertb thead').append(htmlRow);
	
	var printContents = document.getElementById('contextExport').innerHTML;     
	window.print();
	$('#pwusertb thead tr').last().remove();
	$('.dataTables_scrollBody').css('overflow','auto');
	$('.dataTables_scrollHead').css('overflow','hidden');
	$('#today-pusers').css('display','block');
});

//analytics  
var CLIENT_ID = window.CONFIG.GA_CLIENT_ID_API_SUPERADMIN;
var VIEW_ID = window.CONFIG.GA_VIEW_ID_SUPERADMIN;

var SCOPES = ['https://www.googleapis.com/auth/analytics.readonly'];

function authorize(event) {   
    var useImmdiate = event ? false : true;
    var authData = {
      client_id: CLIENT_ID,
      scope: SCOPES,
      immediate: useImmdiate
    };

    gapi.auth.authorize(authData, function(response) {
      var authButton = document.getElementById('auth-button');
      if (response.error) {
        authButton.hidden = false;
      }
      else {
        authButton.hidden = true;
        queryAccounts();
      }
    });
}

function queryAccounts() {
	gapi.client.load('analytics', 'v3').then(function() {
		gapi.client.analytics.management.accounts.list().then(handleAccounts);
	});
}

function handleAccounts(response) {  
	if (response.result.items && response.result.items.length) {   
		var firstAccountId = response.result.items[0].id;
		queryProperties(firstAccountId);
	} else {
		alert('No accounts found for this user.');
	}
}

function queryProperties(accountId) {	 
	gapi.client.analytics.management.webproperties.list({'accountId': accountId})
		.then(handleProperties)
		.then(null, function(err) {
			console.log(err);
		});
}

function handleProperties(response) {
	if (response.result.items && response.result.items.length) {
    var firstAccountId = response.result.items[0].accountId;
    var firstPropertyId = response.result.items[0].id;
    queryProfiles(firstAccountId, firstPropertyId);
	} else {
		console.log('No properties found for this user.');
	}
}

function queryProfiles(accountId, propertyId) {
	gapi.client.analytics.management.profiles.list({
		'accountId': accountId,
		'webPropertyId': propertyId
	})
	.then(handleProfiles)
	.then(null, function(err) {	    
      console.log(err);
	});
}

function handleProfiles(response) {
	if (response.result.items && response.result.items.length) {
		var firstProfileId = response.result.items[0].id;
	    queryCoreReportingApi(firstProfileId);
	} else {
	    console.log('No views (profiles) found for this user.');
	}
}

function queryCoreReportingApi(profileId) {
	gapi.client.request({
	      path: '/v4/reports:batchGet',
	      root: 'https://analyticsreporting.googleapis.com/',
	      method: 'POST',
	      body: {
	        reportRequests: [
	          {
	            viewId: VIEW_ID,
	            dateRanges: [
               	{
               		startDate: '2016-01-01',
               		endDate: 'today'
           		}
	            ],           
	            metrics:[
		                 {
		                   expression:"ga:bounceRate"            	             	  
		                 }
	  			]               	
	          }
	        ]
	      }
	    }).then(function(res) {
	    	$('#auth-button').hide();
	    	 var dataBounceRate = Math.round(res.result.reports[0].data.totals[0].values[0]);
	    	 $('#dataBounceRate').text(dataBounceRate + '%');
	    });
	
	gapi.client.request({
	      path: '/v4/reports:batchGet',
	      root: 'https://analyticsreporting.googleapis.com/',
	      method: 'POST',
	      body: {
	        reportRequests: [
	          {
	            viewId: VIEW_ID,
	            dateRanges: [
             	{
             		startDate: '2016-01-01',
             		endDate: 'today'
         		}
	            ],           
	            dimensions:[
	                    	{
	                    		name:"ga:userAgeBracket"
	                		}
              	]                	
	          }
	        ]
	      }
	    }).then(function(res) {
	    	var total = res.result.reports[0].data.totals[0].values[0];
		    var dataRows = res.result.reports[0].data.rows;
		    var data18_24 = 0;		    
		    var data25_34 = 0;
		    var data35_44 = 0;
		    var data45_54 = 0;
		    var data55_64 = 0;
		    var data65 = 0;
		    if( dataRows != undefined && dataRows.length != undefined ) {
			    for(i = 0; i<dataRows.length; i ++) {
			    	if(dataRows[i]['dimensions'][0] == "18-24") {
			    		data18_24 = dataRows[i]['metrics'][0].values[0];
			    	}
			    	
			    	if(dataRows[i]['dimensions'][0] == "25-34") {
			    		data25_34 = dataRows[i]['metrics'][0].values[0];
			    	}
			    	
			    	if(dataRows[i]['dimensions'][0] == "35-44") {
			    		data35_44 = dataRows[i]['metrics'][0].values[0];
			    	}
	
			    	if(dataRows[i]['dimensions'][0] == "45-54") {
			    		data45_54 = dataRows[i]['metrics'][0].values[0];
			    	}
			    	
			    	if(dataRows[i]['dimensions'][0] == "55-64") {
			    		data55_64 = dataRows[i]['metrics'][0].values[0];
			    	}
			    	
			    	if(dataRows[i]['dimensions'][0] == "65+") {
			    		data65 = dataRows[i]['metrics'][0].values[0];
			    	}
			    	 
			    }
			    		     
			    //fill chart		    		   		    		  
			    $('#data1824').text( Math.round(dataAnalyticsHtml(data18_24,total)) + "%" );
			    $('#data2534').text( Math.round(dataAnalyticsHtml(data25_34,total)) + "%" );
			    $('#data3544').text( Math.round(dataAnalyticsHtml(data35_44,total)) + "%" );
			    $('#data4554').text( Math.round(dataAnalyticsHtml(data45_54,total)) + "%" );
			    $('#data5564').text( Math.round(dataAnalyticsHtml(data55_64,total)) + "%" );
			    $('#data65').text( dataAnalyticsHtml(data65,total) + "%" );
			    
	
			    var agebreak = AmCharts.makeChart( "agebreak", {
			      "type": "pie",
			      "theme": "light",
			      "dataProvider": [ {
			        "title": "18 - 24",
			        "value": data18_24,
			        "color": "#FAE3BF"
			      },
			      {
			        "title": "25 - 34",
			        "value": data25_34,
			        "color": "#FAC8BF"
			      },
			      {
			        "title": "35 - 44",
			        "value": data35_44,
			        "color": "#6C9C7F"
			      },
			      {
			        "title": "45 - 54",
			        "value": data45_54,
			        "color": "#6A8D9D"
			      },
			      {
			        "title": "55 - 63",
			        "value": data55_64,
			        "color": "#6C9C7F"
			      },
			      {
			        "title": "65 and Above",
			        "value": data65,
			        "color": "#FCB02A"
			      },],
			      "titleField": "title",
			      "valueField": "value",
			      "labelRadius": 5,
			      "balloon": {
			    		"adjustBorderColor": false,
			    		"borderColor": "#515974",
			    		"borderThickness": 0,
			    		"color": "#EAEEF5",
			    		"cornerRadius": 8,
			    		"disableMouseEvents": false,
			    		"fadeOutDuration": 0,
			    		"fillAlpha": 1,
			    		"fillColor": "#515974",
			    		"fontSize": 15,
			    		"horizontalPadding": 14,
			    		"pointerWidth": 0,
			    		"shadowAlpha": 0.04,
			    		"verticalPadding": 10,
			    		"fixedPosition": false,
			    		"pointerOrientation": "up",
			    	},
			      "radius": "42%",
			      "innerRadius": "65%",
			      "labelText": "",
			      "export": {
			        "enabled": true
			      }
			    } );
		    }
		    
		    gapi.client.request({
		        path: '/v4/reports:batchGet',
		        root: 'https://analyticsreporting.googleapis.com/',
		        method: 'POST',
		        body: {
		          reportRequests: [
		            {
		              viewId: VIEW_ID,
		              dateRanges: [
		                {
		                  startDate: '2016-01-01',
		                  endDate: 'today'
		                }
		              ],                       
		  			metrics:[
		                 {
		                   expression:"ga:bounceRate"            	             	  
		                 }
		  			]          	                    
		            }
		          ]
		        }
		      }).then(function (res) {		    	  
		    	  var dataBounceRate = Math.round(res.result.reports[0].data.totals[0].values[0]);
		    	  console.log(dataBounceRate);
		    	  $('#dataBounceRate').text(dataBounceRate + '%');
		    			    	  
		    	  var chart = AmCharts.makeChart( "bouncerate", {
		    		  "type": "pie",
		    		  "theme": "light",
		    		  "balloonText": "<span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
		    		  "dataProvider": [
		    				{
		    					"category": "op-1",
		    					"column-1": dataBounceRate,
		    					"color": "#9DDDEE"
		    				},
		    				{
		    					"category": "op-2",
		    					"column-1": (100 - dataBounceRate),
		    					"color": "#00BBEC"
		    				}
		    			],
		    		   "titleField": "title",
		    		 "valueField": "column-1",
		    		  "labelRadius": 5,
		    		  "colorField": "color",
		    		  "radius": "42%",
		    		  "innerRadius": "65%",
		    		  "labelText": "",
		    		  "balloon": {
		    				"adjustBorderColor": false,
		    				"borderColor": "#515974",
		    				"borderThickness": 0,
		    				"color": "#EAEEF5",
		    				"cornerRadius": 8,
		    				"disableMouseEvents": false,
		    				"fadeOutDuration": 0,
		    				"fillAlpha": 1,
		    				"fillColor": "#515974",
		    				"fontSize": 15,
		    				"horizontalPadding": 14,
		    				"pointerWidth": 0,
		    				"shadowAlpha": 0.04,
		    				"verticalPadding": 10,
		    				"fixedPosition": false,
		    				"pointerOrientation": "up",
		    			},
		    		  "export": {
		    		    "enabled": true
		    		  }
		    		} );		    	 
		      });
		 
	    	
		    gapi.client.request({
		        path: '/v4/reports:batchGet',
		        root: 'https://analyticsreporting.googleapis.com/',
		        method: 'POST',
		        body: {
		          reportRequests: [
		            {
		              viewId: VIEW_ID,
		              dateRanges: [
		                {
		                  startDate: '2016-01-01',
		                  endDate: 'today'
		                }
		              ],                       
		              dimensions:[
		                 {
		                	 name: 'ga:fullReferrer'            	             	  
		                 }
		  			]          	                    
		            }
		          ]
		        }
		      }).then(function (response) {				    	  
					var data = response.result.reports[0].data.rows;				    	  
					var html = '';				    	  
					var result = [];
					for(key in data) {       
					    var items = {name: data[key].dimensions[0], value: data[key].metrics[0].values[0] }
					    result.push(items);
					}
					result.sort(function(a, b) {
						return parseFloat(a.value) - parseFloat(b.value);
					});
					
					var countlink = 0;
					for(i = result.length - 1 ; i >= 0 ; i--) {						
						if(countlink == window.CONFIG.DASHBOARD_SUPERADMIN_MAX_LINKSIN)
							break;
						html+='<tr>';
			    			html+='<td><p class="pleft-intble">'+result[i].name+'</p></td>';
			    			html+='<td><p class="center-text">'+result[i].value+'</p></td>';
		    			html+='</tr>';
		    			countlink++;
					}				
		    	  $('tbody.linksin').html('');
		    	  $('tbody.linksin').html(html);
		      });
		    
		    
		    
	    });
}
document.getElementById('auth-button').addEventListener('click', authorize);

function dataAnalyticsHtml (number, total) {
	if(number == 0)
		return 0;
	return ((number/total)*100).toFixed(2);
}