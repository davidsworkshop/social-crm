$(document).ready(function() {
	$(document)
	//accounts table
	var accounts = $('#accounts').removeAttr('width').DataTable( {
	    scrollCollapse: true,
	    paging:         false,
	    columnDefs: [           
	         { 
	        	 "width": "35%", 
	         	"targets": 0,
	             "orderable": false
	         },
	         { 
	        	 "width": "12%",
	         	"targets": 1,
	            
	         },
	         { 
	        	 "width": "12%",
	         	"targets": 2,
	         },
	         { 
	        	 "width": "12%",
	         	"targets": 3,
	            
	         },
	         { 
	        	 "width": "12%",
	         	"targets": 4,
	             
	         },
	         { 
	        	 "width": "12%",
	         	"targets": 5,
	             
	         },
	    ],
	    order: [[ 1, 'desc' ]],
	    fixedColumns: true
	} );
	$("#accounts_wrapper .dataTables_length, #accounts_info, #accounts_filter ,#accounts_paginate").hide();
	
	
});