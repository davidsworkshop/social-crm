$(document).ready(function() {
	$(document)
	.on('click', '.btn-drp', toggleDropdown)
	.on('click', '.dropdown-lst li', selectDropdown)
	.on('click', 'body', function(e) {
		if (! $(e.target).hasClass("btn-drp")) {
			$('ul.dropdown-lst').fadeOut();
		}
	})
	.on('click' , 'a.header-main', function(){
		if ($('a.header-main').hasClass('active')){
			$('#navMenus').slideDown();
			$(this).removeClass('active');
		} else {
			$('#navMenus').slideUp();
			$(this).addClass('active');
		}
	})
	.on('click', '.crop-btn', cropQuestionImg)
	.on('shown.bs.modal', '#modal-upload', showCropAfterOpenModal)
	
		
	$('input:file[name=profile_file]').change(function(e) {
		var form = $(this).closest('form');
		var image, file;
		if(this.files[0].type != 'image/gif') {
			
			
			if ((file = this.files[0])) {
				
				$('#modal-upload').modal('show');
				
				
	    		// Show preview image
	    		var reader = new FileReader();
		        reader.onload = function (e) {
		        	console.log(e);
		        	setTimeout(function() {
						$uploadCrop.croppie('bind', {
				    		url: e.target.result
				    	}).then(function(){
				    		console.log('jQuery bind complete');
				    	});
					}, 1000)
		        	
		        }
		        reader.readAsDataURL(file);	     
			} else {
				$('#img_profile').attr('src', $('#profile_img').val());
			}
			
			
		} else {
			alert('Please upload file with PNG or JPG format');
			return false;
		}
		
	});
	
	
	$('input:file[name=intro_file]').change(function(e) {
		var form = $(this).closest('form');
		var image, file;
		
		if(this.files[0].type != 'image/gif') {
			if ((file = this.files[0])) {
				var reader = new FileReader();
		        reader.onload = function (e) {	
		        	$('#img_intro').css('display','block');
		        	$('#img_intro').attr('src', e.target.result);
		        	$('input[name=intro_file1]').val(e.target.result);
		        }
		        reader.readAsDataURL(file);	     
		        
			} else {
				$('#img_intro').attr('src', $('#intro_file').val());
			}
		} else {
			alert('Please upload file with PNG or JPG format');
			return false;
		}
		
		
		
	});
		
	$('#btnactive').click(function() {
		var appStatus = $('#status').val();
		if(appStatus == 0) {
			$('#status').val(1);
			$('#btnactive').css('background','#F2190A');
			$('#btnactive').val('Deactivate Account');
		} else {
			$('#status').val(0);
			$('#btnactive').css('background','#0AE3F2');
			$('#btnactive').val('Activate Account');
		}
	});
	
	$(':checkbox').checkboxpicker().change(function() {
		if ($(this).prop('checked')) {
			$('input#app-password').val('').closest('div.form-group').addClass('hidden');
			$('input#app-repassword').val('').closest('div.form-group').addClass('hidden');
		} else {
			$('input#app-password').val('').closest('div.form-group').removeClass('hidden');
			$('input#app-repassword').val('').closest('div.form-group').removeClass('hidden');
		}
	});
	
	$('#add-account input[type=submit]').click(function(e) {
		if (window.submitted != undefined && window.submitted) {
			e.preventDefault();
			return false;
		}
		
		window.submitted = true;
	});
});

function toggleDropdown(e) {
	$(this).next('ul').fadeToggle();
}

function selectDropdown(e) {
	$(this).closest('div').find('button').text($(this).text());
	$(this).closest('ul').fadeToggle();
	$(this).siblings().removeClass('active');
	$(this).addClass('active');
} 

function showCropAfterOpenModal() {
	$uploadCrop = $('.croppie-container').croppie({
		viewport: {
			width: 400,
			height: 400,
		},
		boundary: {
			width: 'auto',
			height: 550,
		},
		enableExif: true,
	});
}
function cropQuestionImg() {
	$uploadCrop.croppie('result', {
		type: 'canvas',
		size: {width: 400,height: 400},
	}).then(function (resp) {
		
		$('#img_profile').css('display','block');
    	$('#img_profile').attr('src', resp);
    	
		$('#modal-upload').modal('hide');
		$('input[name=profile_file1]').val(resp);
	});
}
var $uploadCrop;