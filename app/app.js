var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var flash    = require('connect-flash');
var session  = require('express-session');
var expressValidator = require('express-validator');
var Raven = require('raven');

require('dotenv').config();
require('./models/initdb');	// Connect Mongo Lab
require('./config/passport');

//Must configure Raven before doing anything else with it
Raven.config(process.env.SENTRY_DSN, {
	environment: process.env.SENTRY_ENVIRONMENT
}).install();

global.app = express();

process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

//The request handler must be the first middleware on the app
app.use(Raven.requestHandler());

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({ extended: true, limit: '50mb' }));
app.use(expressValidator({
 customValidators: {
    adminPassword: function(value) {
      if (value.match(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{6,}$/)) {
        return true;
      } else {
        return false;
      }
    },
 }
}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

//required for passport
app.use(session({ secret: 'TyBfU2Rvuy', resave: true, saveUninitialized: true })); // session secret
app.use(passport.initialize());
app.use(passport.session()); // persistent login sessions
app.use(flash()); // use connect-flash for flash messages stored in session

// Load routes
app.use('/', [require('./routes/main'), require('./routes/admin'), require('./routes/superadmin'), require('./routes/ajax')]);

//The error handler must be before any other error middleware
app.use(Raven.errorHandler());

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  res.render('404.jade');
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});
//customize listen port 80
app.listen(80);
module.exports = app;
