var mongoose = require('mongoose');
var bonusSchema = new mongoose.Schema({  
  app_id: String,
  title: String,
  caption: String,
  fb_link: String,
  required_action: String,
  image: String,
  order: Number,
  bonus_status: String,
  point: Number,
  background: String,
  count_views: { type: Number, default: 0 },
  count_share_like: { type: Number, default: 0 },
  index: Number,
  created: { type: Date, default: Date.now },
  updated: { type: Date, default: Date.now },
});

module.exports = mongoose.model('Bonus', bonusSchema);