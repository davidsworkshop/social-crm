var mongoose = require('mongoose');
var bcrypt   = require('bcrypt-nodejs');
var moment = require('moment-timezone');
var scoredSchema = new mongoose.Schema({  
  app_id: String,
  user_id: { type: String, ref: 'User' },
  point: { type: Number, default: 0 },
  fb_gender: String,
  share: { type: Number, default: 0 },
  like: { type: Number, default: 0 },
  count_view: { type: Number, default: 0 },
  last_qs_index_played: { type: Number, default: 0 },
  last_bn_index_played: { type: Number, default: 0 },
  user_created: Date,
  user_lastlogin: Date,
  created: { type: Date, default: Date.now },
  updated: { type: Date, default: Date.now },
});

scoredSchema.statics.increaseView = function(app_id, user_id) {
	this.findOne({ 'app_id' : app_id, 'user_id' : user_id }, function(err, scored) {
		if (scored) {
			if (typeof(scored.count_view) == 'undefined' || ! scored.count_view) {
				scored.count_view = 1;
			} else {
				scored.count_view++;
			}
			
			scored.save();
		} else {
			var newScored = mongoose.model('Scored', scoredSchema);
			newScored.create({ 'app_id': app_id, 'user_id': user_id, 'count_view': 1 });
		}
	})
};

//Get number user sign up of month
scoredSchema.statics.getSignUpOfMonth = function(app_id, month, cb) {
	var from = '',
		to = '',
		numDay = 1,
		result = {};
	
	if (moment(month).isSame( moment(), 'month')) {
		from = moment().startOf('month')._d.toISOString();
		to = moment().endOf('day')._d.toISOString();
		numDay = moment().daysInMonth();
	} else {
		from = moment(month).startOf('month')._d.toISOString();
		to = moment(month).endOf('month')._d.toISOString();
		numDay = moment(month).daysInMonth();
	}
	
	var condition = {
		"user_created": {
			"$gte": from,
			"$lte": to
		}	
	}
	
	if (app_id) {
		condition['app_id'] = app_id;
	}
	
	this.find(condition)
	.select("user_created")
	.sort({ user_created: 1 })
	.exec(function(err, scoreds) {
		cb(scoreds, numDay)
	});
};

module.exports = mongoose.model('Scored', scoredSchema);