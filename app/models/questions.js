var mongoose = require('mongoose');
var questionSchema = new mongoose.Schema({  
  app_id: String,
  title: String,
  question: String,
  correct_answer: String,
  explain: String,
  image: String,
  order: Number,
  qs_status: String,
  font_color: String,
  count_correct_answers: { type: Number, default: 0 },
  count_views: { type: Number, default: 0 },
  index: Number,
  created: { type: Date, default: Date.now },
  updated: { type: Date, default: Date.now },
});

module.exports = mongoose.model('Question', questionSchema);