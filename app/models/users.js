var mongoose = require('mongoose');
var bcrypt   = require('bcrypt-nodejs');
var userSchema = new mongoose.Schema({  
  name: String,
  email: String,
  password: String,
  avatar: String,
  fb_id: String,
  fb_token: String,
  fb_gender: String,
  fb_friends: Number,
  role: Number, // 1 - Super Admin; 2 - Admin; 3 - Normal user
  resetpass: { type: Number, default: 0 },
  countplay: { type: Number, default: 0 },
  dateplay: { type: Date, default: Date.now },
  lastlogin: { type: Date, default: Date.now },
  firstlogin: { type: Number, default: 0 },
  created: { type: Date, default: Date.now },
  updated: { type: Date, default: Date.now },
});

userSchema.methods.generateHash = function(password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

// checking if password is valid
userSchema.methods.validPassword = function(password) {
    return bcrypt.compareSync(password, this.password);
};


module.exports = mongoose.model('User', userSchema);