var mongoose = require('mongoose');
var helpers = require('../config/helpers')
var AppView = require('../models/appviews');
var appSchema = new mongoose.Schema({  
  app_name: String,
  brand_name: String,
  url: String,
  profile_img: String,
  intro_file: String,
  admin_id: { type: String, ref: 'User' },
  app_status: Number,
  user_role_save_status: Number,
  term_condition: { type: String, default: '' },
  privacy: { type: String, default: '' },
  term_service: { type: String, default: '' },
  rule: { type: String, default: '' },
  completion_message: { type: String, default: '' },
  point_adwarded: { type: Number, default: 0 },
  point_deducted: { type: Number, default: 0 },
  invite_point: { type: Number, default: 100 },
  web_api: String,
  theme_destop_bg: String,
  font_color: { type: String, default: 'white' },
  completion_background: { type: String, default: 'light' },
  total_round: { type: Number, default: 0 },
  total_scored: { type: Number, default: 0 },
  ga_tracking_id: { type: String, default: '' },
  ga_view_id: { type: String, default: '' },
  only_login_fb: { type: Boolean, default: false },
  visited: { type: Number, default: 0 },
  created: { type: Date, default: Date.now },
  updated: { type: Date, default: Date.now },
});

appSchema.statics.findByNameOrUrl = function(name, url, cb) {
	return this.find({ $or: [ { app_name: name }, { url: url } ] }, cb);
};

appSchema.statics.updateData = function(id, data , cb) {
	this.findOne({ '_id' : id }, function(err, app) {
		for(var field in data) {
			if (field == 'profile_img') {
				helpers.deleteFile(app['profile_img']);
			}
			
			// Delay to continue delete file
			if (field == 'intro_file') {
				var intro_file = app['intro_file'];
				setTimeout(function(){
					helpers.deleteFile(intro_file);
				}, 2000);
			}
			
			// Delay to continue delete file
			if (field == 'theme_destop_bg') {
				var theme_destop_bg = app['theme_destop_bg'];
				setTimeout(function(){
					helpers.deleteFile(theme_destop_bg);
				}, 3000);
			}
			
			app[field] = data[field];
		}
		
		app.save(function() {
			return cb(app);
		})
	});
};

module.exports = mongoose.model('App', appSchema);