var mongoose = require('mongoose');
var inviteFriendSchema = new mongoose.Schema({  
  user_invite_friend_id: String,
  email_invite: String,
  user_invite_success_id: String
});

module.exports = mongoose.model('InviteFriend', inviteFriendSchema);