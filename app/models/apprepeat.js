var mongoose = require('mongoose');
var moment = require('moment-timezone');
var appRepeatSchema = new mongoose.Schema({  
  app_id: String,
  date: { type: Date },
  user_signup_count: Number,
  user_login_count: Number,
  created: { type: Date, default: Date.now },
  updated: { type: Date, default: Date.now },
});

appRepeatSchema.statics.upsert = function(appId, firstTime, date, increaseUnique, cb) {
	var todayTypeOne = moment().startOf('day').format('YYYY-MM-DDT00:00:00.000') + 'Z',
		todayTypeTwo = moment().startOf('day').subtract(1, 'day').format('YYYY-MM-DDT17:00:00.000') + 'Z',
		todayFormat = moment().format('YYYY-MM-DD'),
		addUnique = false,
		addRepeat = false;
	
	// First time visit app
	if (increaseUnique == "1") {
		addUnique = true;
	// Second time visit app on same date
	} else if(firstTime == "1" && date == todayFormat) {
		addRepeat = true;
	// Visit app again
	} else if(date != todayFormat) {
		addUnique = true,
		addRepeat = true;
	}
	
	this.findOne({ 'app_id' : appId, $or: [ { date: todayTypeOne }, { date: todayTypeTwo } ] }, function(err, appRepeat) {
		if (appRepeat) {
			if (addUnique || addRepeat) {
				if (addUnique) { appRepeat.user_signup_count++; }
				if (addRepeat) { appRepeat.user_login_count++; }
				appRepeat.save(function() {
					cb(appRepeat);
				});
			} else {
				cb(appRepeat);
			}
		} else {
			var modelAppRepeat = mongoose.model('AppRepeat', appRepeatSchema),
			newAppRepeat = new modelAppRepeat();
			newAppRepeat.app_id = appId;
			newAppRepeat.date = todayTypeOne;
			newAppRepeat.user_signup_count = addUnique ? 1 : 0;
			newAppRepeat.user_login_count = addRepeat ? 1 : 0;
			newAppRepeat.save(function() {
				cb(newAppRepeat);
			});
		}
	})
};

module.exports = mongoose.model('AppRepeat', appRepeatSchema);