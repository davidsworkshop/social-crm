var mongoose = require('mongoose');
var moment = require('moment-timezone');
var appViewsSchema = new mongoose.Schema({  
  app_id: String,
  count_view: { type: Number, default: 0 },
  date: { type: Date }
});

appViewsSchema.statics.increaseView = function(app_id, cb) {
	var todayTypeOne = moment().startOf('day').format('YYYY-MM-DDT00:00:00.000') + 'Z',
		todayTypeTwo = moment().startOf('day').subtract(1, 'day').format('YYYY-MM-DDT17:00:00.000') + 'Z';
	this.findOne({ 'app_id' : app_id, $or: [ { date: todayTypeOne }, { date: todayTypeTwo } ] }, function(err, appView) {
		if (appView) {
			appView.count_view++;
			appView.save(function() {
				cb(appView);
			});
		} else {
			var modelAppView = mongoose.model('AppViews', appViewsSchema),
				newAppView = new modelAppView();
			newAppView.app_id = app_id;
			newAppView.count_view = 1;
			newAppView.date = todayTypeOne;
			newAppView.save(function() {
				cb(newAppView);
			});
		}
	})
};

module.exports = mongoose.model('AppViews', appViewsSchema);