var express = require('express'),
    router = express.Router(),
    mongoose = require('mongoose'), //mongo connection
    bodyParser = require('body-parser'), //parses information from POST
    methodOverride = require('method-override'), //used to manipulate POST
    User = require('../models/users'),
    App = require('../models/apps'),
    Question = require('../models/questions'),
    Scored = require('../models/scoreds'),
    Bonus = require('../models/bonus'),
    AppViews = require('../models/appviews'),
    AppRepeat = require('../models/apprepeat'),
    InviteFriend = require('../models/invitefriend'),
    multer  = require('multer'),
    util = require('util'),
    async = require("async"),
    configs = require('../config/configs'),
    helpers = require('../config/helpers');

var uploadImageApp = multer({
	storage: configs.multer.storageApp,
	fileFilter: configs.multer.imageFilter
});

app.locals.moment = require('moment-timezone');
app.locals.moment.tz.setDefault("Asia/Singapore");
app.locals.numberWithCommas = helpers.numberWithCommas;
app.locals.getRankingLeaderboard = helpers.getRankingLeaderboard;

router.use(bodyParser.urlencoded({ extended: true }))
router.use(methodOverride(function(req, res){
      if (req.body && typeof req.body === 'object' && '_method' in req.body) {
        // look in urlencoded POST bodies and delete it
        var method = req.body._method
        delete req.body._method
        return method
      }
}));


//route middleware to validate :appUrl
router.param('appurl', function(req, res, next, appurl) {
	// Set admin menu flag
	req.app.locals.activeMenu = {parent: '', child: ''};
	if (appurl == 'shanghai') {
		next();
	} else if (appurl == 'superadmin') {
		next();
	} else {
		App.findOne({ 'url' : appurl }, function(err, app) {
			if (err || ! app) {
				res.status(404);
	            res.render('404.jade');
			} else {
				var avatar = '';
				if(typeof req.user != 'undefined') {
					avatar = req.user.avatar;
				}
				else {
					avatar = '/images/no-image.jpg';
				}
				req.app.locals.currentApp = {
					url : app.url,
					name : app.app_name,
					id : app._id,
					admin_id: app.admin_id,
					admin_access: app.app_status == 0 && app.user_role_save_status == 1 ? false : true,
					font_color: app.font_color,
					theme_destop_bg: app.theme_destop_bg,
					profile_img: app.profile_img ? app.profile_img : '/images/logo.png',
					intro_file: app.intro_file,
					ga_app_tracking_id: app.ga_tracking_id,
					point_adwarded: app.point_adwarded ? app.point_adwarded : 0,
					point_deducted: app.point_deducted ? app.point_deducted : 0,
					invite_point: app.invite_point,
					user_avatar: avatar,
					app_status: app.app_status,
					completion_background: app.completion_background,
					completion_message: app.completion_message.replace(/{score}/g, ' <span class="today-score">0</span> '),
					only_login_fb: app.only_login_fb != undefined ? app.only_login_fb : false
					
				};
				req.app.locals.appUrl = '/'+ app.url;

				next();
			}
		});
	}
});

/* HOME PAGE */
router.get('/', function(req, res) {
	App.find({}, function (err, apps) {
        if (err) {
            return console.error(err);
        } else {
        	res.format({
	                html: function(){
	              	  res.render('home', {
	              		  title: 'QUIZZU',
	              		  apps : apps,
	              	  });
	                }
	    	  });
        }
	});
});

/* USER LOGIN */
router.get('/:appurl/', function(req, res) {
	if (req.params['appurl'] == 'shanghai') {
		return res.render('web/shanghai.jade');
	} else if (req.params['appurl'] == 'superadmin' ) {
		if (req.isAuthenticated()) {
			if (req.user.role == 1) {
				return res.redirect('/superadmin/add-account');
			} else {
				req.logout();
				return res.redirect('/superadmin');
			}
		} else {
			return res.redirect('/superadmin/sp-login');
		}
	} else if(req.params['appurl'] == 'newsletter') {
		next();
	} else {
		if (req.isAuthenticated() && req.user.role == 2) {
			App.findOne({ 'admin_id' : req.user.id }, function(err, app) {
				if (app) {
					return res.redirect('/'+ app.url +'/admin/dashboard');
				} else {
					return res.redirect('/'+ req.params['appurl'] +'/question');
				}
			});
		} else {
			// Increase 1 visit for this application
			App.findOne({url: req.params['appurl']}).exec(function (err, app) {
				if (app) {
					app.visited++;
					app.save();
					
					if (req.isAuthenticated()) {
						return res.redirect('/'+ req.params['appurl'] +'/questions');
					}	
					var font_color = '';
					var icon_user = 'icon-person.png';
					var icon_password = 'icon-password.png';
					if( app.font_color !='' ) {
						font_color = app.font_color;			
					}
					
					if(app.font_color != 'white') {
						icon_user = 'icon-person-black.png';
						icon_password = 'icon-password-black.png';
					}
					
					
				 
				    res.render('web/login.jade', {
				    	title: 'Login',
				    	isAuthenticated: req.isAuthenticated(),
				    	user: req.user,
				    	appUrl: '/'+ req.params['appurl'],
				    	message: req.flash('loginMessage'),
				    	backgroundStyle1: true,
				    	font_color: font_color,
				    	icon_user: icon_user,
				    	icon_password: icon_password
				    });
				}
			});
		}
	}
});

/* POST LOGIN */
router.post('/:appurl/login', function(req, res, next) {
	passport.authenticate('local', function(err, user, info) {
		if (! req.body.email.trim()) {
			req.flash('loginMessage', 'The email field is empty.');
			return res.redirect('/'+ req.params['appurl'] +'/');
		} else if (! req.body.password.trim()) {
			req.flash('loginMessage', 'The password field is empty.');
			return res.redirect('/'+ req.params['appurl'] +'/');
		}

		if (err) { return next(err); }
		if (!user) { return res.redirect('/'+ req.params['appurl'] +'/'); }
		req.logIn(user, function(err) {
			if (err) { return next(err); }

			if (user.role == 3) {
				return res.redirect('/'+ req.params['appurl'] +'/questions');
			} else if (user.role == 2) {
				App.findOne({ 'admin_id' : user.id }, function(err, app) {
					if (app) {
						return res.redirect('/'+ app.url +'/admin/dashboard');
					} else {
						return res.redirect('/'+ req.params['appurl'] +'/');
					}
				});
			} else if (user.role == 1) {
				return res.redirect('/superadmin/dashboard');
			}

	    });
	})(req, res, next);
});

/* SIGN UP */
router.get('/:appurl/sign-up', function(req, res) {
	if (req.isAuthenticated()) {
		return res.redirect('/'+ req.params['appurl'] +'/questions');
	}
	
	App.findOne({ 'url' : req.params['appurl'] }, function(err, app) {
		if (app) {
			// Don't allow sign up if app only login on FB
			if (app.only_login_fb) {
				return res.redirect('/'+ req.params['appurl'] +'/');
			}
			
		    res.render('web/sign-up.jade', {
		    	title: 'Sign up',
		    	message: req.flash('signupMessage'),
		    	appUrl: '/'+ req.params['appurl'],
		    	backgroundStyle1: true,
		    });
		} else {
			res.status(404);
			res.render('404.jade');
		}
	});
	
});

/* SIGN UP WITH INVITE FRIEND TOKEN */
router.get('/:appurl/sign-up/token/:tokeninvite', function(req, res) {
	if (req.isAuthenticated()) {
		return res.redirect('/'+ req.params['appurl'] +'/questions');
	}
	
	var emailDefault = '';
	
	if (typeof(req.params['tokeninvite']) != 'undefined') {
		var buffer = new Buffer(req.params['tokeninvite'], 'base64');
		var stringDecode = buffer.toString('ascii');
		try {
		    JSON.parse(stringDecode);
		    var inviteArray = JSON.parse(stringDecode);
			
			if (typeof(inviteArray) == "object" && typeof(inviteArray['user_id']) && typeof(inviteArray['email'])) {
				emailDefault = inviteArray['email'];
				
				// Insert to invitefriend document
				InviteFriend.findOne({email_invite: inviteArray['email']}, function(err, result) {
					if (! err && ! result) {
						var newIF = new InviteFriend();
						newIF.user_invite_friend_id = inviteArray['user_id'];
						newIF.email_invite = inviteArray['email'];
						newIF.save();
					}
				})
			}
		} catch (e) {
			emailDefault = '';
		}
	}
	
	App.findOne({ 'url' : req.params['appurl'] }, function(err, app) {
		if (app) {
			if (app.only_login_fb) {
				return res.redirect('/'+ req.params['appurl'] +'/');
			}
			
		    res.render('web/sign-up.jade', {
		    	title: 'Sign up',
		    	message: req.flash('signupMessage'),
		    	appUrl: '/'+ req.params['appurl'],
		    	backgroundStyle1: true,
		    	emailDefault: emailDefault
		    });
		} else {
			res.status(404);
			res.render('404.jade');
		}
	});
	
	
});

/* POST SIGN UP */
router.post('/:appurl/sign-up', function(req, res, next) {
	passport.authenticate('local-signup', function(err, user, info) {
		if (! req.body.name.trim()) {
			req.flash('signupMessage', 'The name field is empty.');
			return res.redirect('/'+ req.params['appurl'] +'/sign-up');
		} else if (! req.body.email.trim()) {
			req.flash('signupMessage', 'The email field is empty.');
			return res.redirect('/'+ req.params['appurl'] +'/sign-up');
		} else if (! req.body.password.trim()) {
			req.flash('signupMessage', 'The password field is empty.');
			return res.redirect('/'+ req.params['appurl'] +'/sign-up');
		}

		if (err) { return next(err); }
		if (!user) { return res.redirect('/'+ req.params['appurl'] +'/sign-up'); }
		req.logIn(user, function(err) {
			if (err) { return next(err); }
			return res.redirect('/'+ req.params['appurl'] +'/questions');
	    });
	})(req, res, next);
});

// =====================================
// FACEBOOK ROUTES =====================
// =====================================
// route for facebook authentication and login
router.get('/auth/facebook/:id', function(req,res,next) {
	passport.authenticate(
		'facebook', 
		{callbackURL: '/auth/facebook/callback/'+req.params.id, authType: 'rerequest', scope : ['email', 'user_friends'] }
	
	)(req,res,next);
});

router.get('/auth/facebook/callback/:id', function(req,res,next) {
	passport.authenticate(
		'facebook',
		{
			callbackURL:"/auth/facebook/callback/"+req.params.id,
			successRedirect:"/"+ req.params.id,
			failureRedirect:"/"+ req.params.id
		}
	) (req,res,next);
});


/*
router.get('/auth/facebook', passport.authenticate('facebook', {authType: 'rerequest', scope : ['email', 'user_friends'] }));

//handle the callback after facebook has authenticated the user
router.get('/auth/facebook/callback', passport.authenticate('facebook'), function(req, res) {
	if (req.user.role == 2) {
		App.findOne({ 'admin_id' : req.user._id }, function(err, app) {
			if (app) {
				return res.redirect('/'+ app.url +'/admin/dashboard');
			} else {
				return res.redirect(req.app.locals.appUrl +'/');
			}
		});
	} else {
		console.log(req.app.locals.appUrl);
		return res.redirect(req.app.locals.appUrl +'/questions');
	}
});

/* User profile */
router.get('/:appurl/profile', isLoggedIn, checkAppActive, function(req, res) {
	App.findOne({url: req.params['appurl']}, function(err,app) {
		User.findOne({_id: req.user.id}, function(err,user) {
			if(user) {
				var avatar = '';
				if(!user.avatar) {
					avatar = '/images/no-avatar.jpg';
				} else {
					avatar = user.avatar
				}
				res.render('web/profile.jade', {
			    	title: 'Profile',
			    	avatar: avatar,
			    	email: user.email,
			    	name: user.name,
			    	fb_id: user.fb_id != undefined && user.fb_id ? user.fb_id : '',
			    	message: req.flash('errorMessage'),
			    	successful: req.flash('successfulMessage'),
			    	app: app
			    });
			}
		});
	});
	
});

router.post('/:appurl/profile', isLoggedIn, checkAppActive,
		uploadImageApp.fields([{
	    	name: 'profile_file', maxCount: 1
		}]),
		function(req, res) {

	var avartar = '';
	if (typeof req.files.profile_file != 'undefined') {
		if(req.body.current_image !='/images/no-avatar.jpg') {
			helpers.deleteFile(req.body.current_image);
		}
		avartar = '/uploads/'+req.files.profile_file[0].filename;
	} else {
		avartar = req.body.current_image;
	}

	if(req.body.name == '') {
		helpers.deleteFile(req.files.profile_file[0].path,true);
		req.flash('errorMessage', ['Name is empty']);
		return res.redirect('back');
	}

	if(req.body.name != '' && req.body.password == '' && req.body.newpassword == '' && req.body.vertifypassword =='' ) {
		User.findOne({_id: req.user.id}, function(err,user) {
			if(user) {
				user.name = req.body.name;
				user.avatar = avartar;
				user.save(function(err) {
					req.flash('successfulMessage', ['Update successfull']);
					req.user.name = req.body.name;
					req.user.avatar = avartar;
					return res.redirect('back');
				});
			}
		});
	}else if(req.body.name != '' && req.body.password != '' && req.body.newpassword != '' && req.body.vertifypassword !='' ) {
		User.findOne({_id: req.user.id}, function(err,user) {
			if(user.validPassword(req.body.password)){
				if(req.body.newpassword != req.body.vertifypassword) {
					if (typeof req.files.profile_file != 'undefined') {
						helpers.deleteFile(req.files.profile_file[0].path,true);
					}
					req.flash('errorMessage', ['Password is mismatch.']);
					return res.redirect('back');
				}else {
					user.name = req.body.name;
					user.password = user.generateHash(req.body.newpassword);
					user.avatar = avartar;
					user.save(function(err) {
						req.flash('successfulMessage', ['Update successfull']);
						req.user.name = req.body.name;
						req.user.avatar = avartar;
						return res.redirect('back');
					});
				}
			}else {
				if (typeof req.files.profile_file != 'undefined') {
					helpers.deleteFile(req.files.profile_file[0].path,true);
				}
				req.flash('errorMessage', ['Current password not match.']);
				return res.redirect('back');
			}
		});
	}else {
		req.checkBody('password', 'Current password is empty').notEmpty();
		req.checkBody('newpassword', 'New password is empty').notEmpty();
		req.checkBody('vertifypassword', 'Verify password is empty').notEmpty();
		var errors = req.validationErrors();
		if (errors) {
			var errors_list = [];
			for(var i in errors) {
				errors_list.push(errors[i]['msg']);
			}
			if (typeof req.files.profile_file != 'undefined') {
				helpers.deleteFile(req.files.profile_file[0].path,true);
			}
			req.flash('errorMessage', errors_list);
			return res.redirect('back');
		}
	}
});


router.get('/:appurl/terms-and-conditions', isLoggedIn, checkAppActive, function(req, res) {
	App.findOne({url: req.params['appurl']}, function(err,app) {
		res.render('web/terms-and-conditions.jade', {
	    	title: 'Terms and Conditions',
	    	bodytext: helpers.convertToHtml(app.term_condition),
	    	app: app,
	    	avatar: req.user != undefined && req.user.avatar ? req.user.avatar : '/images/no-image.jpg'
	    });
	});
});

router.get('/:appurl/privacy-policy', isLoggedIn, checkAppActive, function(req, res) {
	App.findOne({url: req.params['appurl']}, function(err,app) {
		res.render('web/privacy-policy.jade', {
	    	title: 'Privacy Policy',
	    	bodytext: helpers.convertToHtml(app.privacy),
	    	app: app,
	    	avatar: req.user != undefined && req.user.avatar ? req.user.avatar : '/images/no-image.jpg'
	    });
	});
});

router.get('/:appurl/how-to-win', isLoggedIn, checkAppActive, function(req, res) {
	App.findOne({url: req.params['appurl']}, function(err,app) {
		res.render('web/how-to-win.jade', {
	    	title: 'How to Win',
	    	bodytext: helpers.convertToHtml(app.rule),
	    	app: app,
	    	avatar: req.user != undefined && req.user.avatar ? req.user.avatar : '/images/no-image.jpg'
	    });
	});
});

router.get('/:appurl/terms-of-service', isLoggedIn, checkAppActive, function(req, res) {
	App.findOne({url: req.params['appurl']}, function(err,app) {
		
		res.render('web/terms-of-service.jade', {
	    	title: 'Terms of Service',
	    	bodytext: helpers.convertToHtml(app.term_service),
	    	app: app,
	    	avatar: req.user != undefined && req.user.avatar ? req.user.avatar : '/images/no-image.jpg'
	    });
	});
	
});

router.get('/:appurl/leaderboard', isLoggedIn, checkAppActive, function(req, res) {
	var data = {},
		check_num = 0,
		is_user_in_list = false,
		user_scored_result = [],
		map_reduce = '';

	async.waterfall([
	     getApp,
	     deleteScoredNotValid,
	     listSuperAdminAndAdmin,
	     getScoredMapReduce,
	     getTopScored,
	     getUserScored,
	     getInviteFriend,
	     getInviteFriendScore
    ], function (err) {
		res.render('web/leaderboard.jade', {
	    	title: 'Leaderboard',
	    	scoreds: data['scoreds'],
	    	user: req.user,
	    	check_num: check_num,
	    	is_user_in_list: is_user_in_list,
	    	user_scored: user_scored_result,
	    	map_reduce: map_reduce,
	    	invite_friends_scored: data['inviteFriendsScored'],
	    	app: data['app'],
	    	avatar: req.user != undefined && req.user.avatar ? req.user.avatar : '/images/no-image.jpg'
	    });
	})
	
	function getApp(callback) {
		var appurl = req.params['appurl'] != undefined && req.params['appurl'] ? req.params['appurl']: '';
		
		App.findOne({ 'url' : appurl }, function(err, app) {
			if (app) {
				data['app'] = app;
				return callback(null);
			} else {
				return callback(true, "Can not found application.")
			}
		});
	}
	
	function deleteScoredNotValid(callback) {
		Scored.find({app_id: data['app']._id})
		.populate('user_id', 'name')
		.select('user_id')
		.exec(function(err, allScoreds) {
			var listScoredDel = [];
			for (var i in allScoreds) {
				if (! allScoreds[i].user_id) {
					listScoredDel.push(allScoreds[i]._id);
				}
			}
			if (listScoredDel.length) {
				Scored.remove({ '_id': {
					$in: listScoredDel
				}}, function() {
					callback(null);
				});
			} else {
				callback(null);
			}
		});
	}

	function listSuperAdminAndAdmin(callback) {
		User.find({
			role : {$in: [1,2]}
		})
		.select("role")
		.exec(function(err, results) {
			data['listAdmin'] = [];
			if (results.length) {
				data['listAdmin'] = results.map(function(item){return item['_id']});
			}
			return callback(null);
		});
	}
	
	function getScoredMapReduce(callback) {
		var o = {};
		o.map = function () { emit(null, this.user_id) }
		o.reduce = function (key, values) {
			return values.toString();
		}
		o.out = { inline: 1 };
		o.verbose = true;
		o.query = {app_id: data['app']._id, user_id: {$nin: data['listAdmin']}};
		o.sort = { point: -1 }
		
		Scored.mapReduce(o, function (err, results, stats) {
			//console.log('map reduce took %d ms', stats.processtime);
			map_reduce = results[0]['value'];
			callback(null);
		})
	}
	
	function getTopScored(callback) {
		Scored.find({app_id: data['app']._id, user_id: {$nin: data['listAdmin']}})
		.populate('user_id', 'name avatar')
		.sort({ point: -1 })
		.select('user_id point')
		.limit(200)
		.exec(function(err, scoreds) {
			data['scoreds'] = scoreds;
			
			// Check if user in list scoreds
			for (var i in scoreds) {
				if (req.user.id == scoreds[i]['user_id']['_id']) {
					check_num = parseInt(i) + 1;
					is_user_in_list = true;
				}
			}
			
			callback(null);
		});
	}
	
	function getUserScored(callback) {
		Scored.findOne({
			app_id: data['app']._id,
			user_id: req.user.id
		})
		.select('user_id point')
		.exec(function(err, user_scored) {
			user_scored_result = user_scored;
			callback(null);
		})
	}
	
	function getInviteFriend(callback) {
		data['inviteFriends'] = [];
		InviteFriend.find({
			user_invite_friend_id: req.user.id,
			user_invite_success_id: {$ne : null}
		})
		.select('user_invite_success_id')
		.limit(200)
		.exec(function (err, inviteResults) {
			if (! err && inviteResults.length) {
				data['inviteFriends'] = inviteResults;
				callback(null);
			} else {
				callback(null);
			}
		})
	}
	
	function getInviteFriendScore(callback) {
		data['inviteFriendsScored'] = [];
		if (data['inviteFriends'].length) {
			var list_friend_id = [req.user.id];
			for(var i in data['inviteFriends']) {
				list_friend_id.push(data['inviteFriends'][i]['user_invite_success_id']);
			}
			
			Scored.find({
				app_id: data['app']._id,
				user_id: {$in: list_friend_id}
			})
			.populate('user_id', 'name avatar')
			.sort({ point: -1 })
			.select('user_id point')
			.exec(function(err, scoreds) {
				if (! err && scoreds.length) {
					data['inviteFriendsScored'] = scoreds
				}
				callback(null);
			})
		} else {
			callback(null);
		}
	}
});

/* Question page */
router.get('/:appurl/questions', isLoggedIn, checkAppActive, function(req, res) {
	req.user['scoredGame'] = 0;
	var data = {}

	async.waterfall([
	     getApp,
         getScored,
	     getListQuestion,
         getUser,
         getListBonus,
         checkFirstVisit
    ], function (err, msg) {
		if (err) {
			return res.render(msg);
		}
		var totalQuestionAndRound = data.questions.length + data.bonus.length;

		var userScored = 0;
		if (data.scored) {
			userScored = data.scored.point;
		}
		res.render('web/questions.jade', {
	    	title: 'Questions',
	    	questions: data.questions,
	    	bonus: data.bonus,
	    	like: data.like,
	    	userScored: userScored,
	    	role:	data.user.role,
	    	totalQuestionAndRound: totalQuestionAndRound,
	    	backgroundStyle1: true,
	    	firstvisit: data.firstvisit,
	    	app: data['app'],
	    	avatar: req.user != undefined && req.user.avatar ? req.user.avatar : '/images/no-image.jpg'
	    });
    });
	
	function getApp(callback) {
		var appurl = req.params['appurl'] != undefined && req.params['appurl'] ? req.params['appurl']: '';
		
		App.findOne({ 'url' : appurl }, function(err, app) {
			if (app) {
				data['app'] = app;
				return callback(null);
			} else {
				return callback(true, "Can not found application.")
			}
		});
	}

	function getListQuestion(callback) {
		Question.find({app_id: data['app']._id, qs_status: '1', index: {$gt: data['scored'].last_qs_index_played}})
			.sort({ order: 1 })
			.exec(function(err, questions) {
				data['questions'] = questions;
				callback(null);
			});
	}

	function getScored(callback) {
		Scored.findOne({ 'app_id' : data['app']._id, 'user_id': req.user.id }, function(err, scored) {
			if(scored == null) {
				var new_scored = new Scored();
				new_scored.app_id = data['app']._id;
				new_scored.user_id = req.user.id;
				new_scored.fb_gender = req.user.fb_gender;
				new_scored.save(function(err, newScored) {
					data['scored'] = newScored;
					callback(null);
				});
			} else {
				data['scored'] = scored;
				callback(null);
			}
			/*
			if (typeof(data['scored'].last_qs_index_played) == "undefined") {
				data['scored'].last_qs_index_played = 0;
			}
			if (typeof(data['scored'].last_bn_index_played) == "undefined") {
				data['scored'].last_bn_index_played == 0;
			}
			*/
		});
	}

	function getUser(callback) {
		User.findOne({ '_id' : req.user.id}, function(err, user) {
			data['user'] = user;
			callback(null);
		});
	}

	function getListBonus(callback) {
		Bonus.find({app_id: data['app']._id, 'bonus_status': '1', index: {$gt: data['scored'].last_bn_index_played}})
			.sort({ order: 1 })
			.exec(function(err, bonus) {
				data['bonus'] = bonus;
				callback(null);
			});
	}
	
	function checkFirstVisit(callback) {
		data['firstvisit'] = false;
		User.findOne({'_id': req.user.id})
		.exec(function(err, result) {
			if (result.firstlogin == undefined || result.firstlogin == 0) {
				data['firstvisit'] = true;
				result.firstlogin = 1;
				result.save();
			}
			callback(null);
		});
	}
});

router.post('/:appurl/answer-question', isLoggedIn, checkAppActive, function(req, res) {
	var questionId = req.body.question_id,
		answer = req.body.answer,
		appurl = req.params['appurl'] != undefined && req.params['appurl'] ? req.params['appurl']: '';
	
	App.findOne({ 'url' : appurl }, function(err, app) {
		if (app) {
			Question.findOne({ '_id' : questionId }, function(err, question) {
				if (! question || question.index == undefined || ! question.index) {
					return res.json({correct: 0});
				}
		        question.count_views = question.count_views + 1;
		        var qsIndex = question.index;

		        Scored.findOne({ 'app_id' : app._id, 'user_id': req.user.id }, function(err, scored) {
		        	if (scored) {
		        		if (scored.last_qs_index_played < qsIndex) {
		        			scored.last_qs_index_played = qsIndex;	// Save last question index user played
		            		if (question.correct_answer == answer) {
		            			question.count_correct_answers++;
		            			req.user['scoredGame'] += app.point_adwarded;
		            			scored.point += app.point_adwarded;
		    					scored.updated = new Date();
		    					scored.save(function() {
		    						question.save();
		    						return res.json({correct: 1});
		    					});
		            		} else {
		            			req.user['scoredGame'] -= app.point_deducted;
		            			scored.point -= app.point_deducted;
		            			scored.updated = new Date();
		    					scored.save(function() {
		    						question.save();
		    						return res.json({correct: 0});
		    					});
		            		}
		        		} else {
		        			return res.json({correct: 0});
		        		}
		        	} else {
		        		var newScored = new Scored();
						newScored.app_id = app._id;
						newScored.user_id = req.user.id;
						newScored.last_qs_index_played = qsIndex;	// Save last question index user played

		        		if (question.correct_answer == answer) {
		        			req.user['scoredGame'] += app.point_adwarded;
		        			question.count_correct_answers++;

							newScored.point = app.point_adwarded;
							newScored.save(function() {
								question.save();
								return res.json({correct: 1});
							});
		        		} else {
		        			req.user['scoredGame'] -= app.point_deducted;
		        			newScored.point = 0 - app.point_deducted;
		        			newScored.save(function() {
								question.save();
								return res.json({correct: 0});
							});
		        		}
		        	}
		        });
			});
		} else {
			return res.json({correct: 0});
		}
		
	});
	
});

router.post('/:appurl/answer-total', isLoggedIn, checkAppActive, function(req, res) {
    var data = {},
    	appurl = req.params['appurl'] != undefined && req.params['appurl'] ? req.params['appurl']: '';

	async.waterfall([
         getApp,
         getUser,
    ], function (err) {
		if (data['app']) {
			data['app'].total_round = data['app'].total_round + 1;
			data['app'].total_scored = data['app'].total_scored + req.user['scoredGame'];
			data['app'].save(function(err) {
				data['user'].countplay = 1;
				data['user'].dateplay = new Date();
				data['user'].save(function(){
        			res.json({result: 1});
        		});
            });
			// Clean scoredGame when finished game
			req.user['scoredGame'] = 0;
		} else {
            res.json({result: 0});
		}
    });

	function getApp(callback) {
		App.findOne({ 'url' : appurl }, function(err, app) {
			data['app'] = app;
			callback(null);
		});
	}

	function getUser(callback) {
		User.findOne({ '_id' : req.user.id}, function(err, user) {
			data['user'] = user;
			callback(null);
		});
	}
});

router.post('/:appurl/bonus-proccess', isLoggedIn, function(req, res) {
	var bonusId = req.body.bonus_id,
		proccess = req.body.proccess,
		appurl = req.params['appurl'] != undefined && req.params['appurl'] ? req.params['appurl']: '';

	App.findOne({ 'url' : appurl }, function(err, app) {
		if (app) {
			Bonus.findOne({ '_id' : bonusId }, function(err, bonus) {
				if (bonus) {
					if (bonus.index == undefined || ! bonus.index) {
						return res.json({point: 0});
					}
					
					var addPoint = 0,
						bsIndex = bonus.index;
					bonus.count_views++;
					if (proccess == 1) {
						bonus.count_share_like++;
						addPoint = bonus.point;

						Scored.findOne({ 'app_id' : app._id, 'user_id': req.user.id }, function(err, scored) {
							if (scored) {
								if (scored.last_bn_index_played < bsIndex) {
									scored.point += bonus.point;
									scored.updated = new Date();
									scored.last_bn_index_played = bsIndex;	// Save last bonus index user played
									if (bonus.required_action == 'share') {
										scored.share++;
									} else {
										scored.like++;
									}
									scored.save(function() {
										res.json({point: addPoint});
									});
								} else {
									return res.json({point: 0});
								}
							} else {
								var newScored = new Scored();
								newScored.app_id = app._id;
								newScored.user_id = req.user.id;
								newScored.point = bonus.point;
								newScored.last_bn_index_played = bsIndex;
								if (bonus.required_action == 'share') {
									newScored.share = 1;
								} else {
									newScored.like = 1;
								}
								newScored.save(function() {
									res.json({point: addPoint});
								});
							}
						});
						req.user['scoredGame'] += bonus.point;
					} else {
						// Save last bonus index user played
						Scored.findOne({ 'app_id' : app._id, 'user_id': req.user.id }, function(err, scored) {
							scored.last_bn_index_played = bsIndex;
							scored.save();
						})
						res.json({point: addPoint});
					}

					bonus.save();
				} else {
		            res.json({error: "Can't not find Bonus"});
				}
			});
		} else {
			res.json({error: "Can't not find Application"});
		}
	});
});

// ====================================
// RESET PASS =========================


/*fogot password*/
router.get('/:appurl/forget-password', function(req, res) {
	res.render('web/forget-password.jade', {
    	title: 'Forget password',
    	backgroundStyle1: true
    });

});

/*reset password*/
router.get('/:appurl/reset-password/:tokenreset', function(req, res) {
	var buffer = new Buffer(req.params['tokenreset'], 'base64');
	var stringDecode = buffer.toString('ascii');
	var disabled = '';
	var titlebutton = '';
	var message = '';
	var expired = 0;
	var classdisable = '';
	var token = req.params['tokenreset'];
	stringDecode = stringDecode.split(',');
	if(stringDecode.length !=2 || req.app.locals.moment(stringDecode[1])._d == "Invalid Date") {
		titlebutton = 'Session is expired';
		disable = 'disabled';
		message = 'Please reset password again';
		expired = 1;
		classdisable = 'disable';

		res.render('web/reset-password.jade', {
	    	title: 'Reset password',
	    	disabled: disable,
	    	titlebutton: titlebutton,
	    	message: message,
	    	expired: expired,
	    	classdisable: classdisable,
	    	token: token,
	    	backgroundStyle1: true
	    });
		return ;
	}

	User.findOne({email: stringDecode[0]}, function(err, user) {
		var title = '';
		if(!user) {
			title = 'Invalid email';
		}else {
			if(user.resetpass == 0) {
				titlebutton = 'Session is expired';
				disable = 'disabled';
				message = 'Please reset password again';
				expired = 1;
				classdisable = 'disable';

			}else {
				var oneDay = 86400000;
				if( oneDay < req.app.locals.moment() - req.app.locals.moment(stringDecode[1]) ) {
					titlebutton = 'Session is expried'
					disable = 'disabled';
					message = 'Please reset password again';
					expired = 1;
					classdisable = 'disable';
				}else {
					title = 'Reset password';
					disable = '';
					message = '';
					titlebutton = 'Reset my password';
					classdisable = '';
				}
			}
		}

		res.render('web/reset-password.jade', {
	    	title: 'Reset password',
	    	disabled: disable,
	    	titlebutton: titlebutton,
	    	message: message,
	    	expired: expired,
	    	classdisable: classdisable,
	    	token: token,
	    	backgroundStyle1: true
	    });

	});
});

// =====================================
// LOGOUT ==============================
// =====================================
router.get('/:appurl/logout', function(req, res) {
    req.logout();
    if (req.params['appurl'] != 'superadmin') {
    	res.redirect(req.app.locals.appUrl +'/');
    } else {
    	res.redirect('/superadmin');
    }

});

//route middleware to make sure a user is logged in
function isLoggedIn(req, res, next) {

    // if user is authenticated in the session, carry on
    if (req.isAuthenticated())
        return next();

    // if they aren't redirect them to the home page
    res.redirect(req.app.locals.appUrl +'/');
}

//route middleware to check application is active
function checkAppActive(req, res, next) {
	var appurl = req.params['appurl'] != undefined && req.params['appurl'] ? req.params['appurl']: '';

	App.findOne({ 'url' : appurl }, function(err, app) {
		if (app && app.app_status) {
			return next();
		} else {
			// if app not active redirect them to unavailble app
			return res.render('web/unavailableapp.jade', {
				title: 'Unavailable App'
			});
		}
	});
}

module.exports = router;