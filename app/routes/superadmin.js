var express = require('express'),
    router = express.Router(),
    mongoose = require('mongoose'), //mongo connection
    bodyParser = require('body-parser'), //parses information from POST
    methodOverride = require('method-override'), //used to manipulate POST
    User = require('../models/users'),
    App = require('../models/apps'),
    Question = require('../models/questions'),
    Scored = require('../models/scoreds'),
    Bonus = require('../models/bonus'),
    AppViews = require('../models/appviews'),
    AppRepeat = require('../models/apprepeat'),
    multer  = require('multer'),
    util = require('util'),
    async = require("async"),
    configs = require('../config/configs'),
    excel = require('node-excel-export'),
    fis = require('file-system'),
    path = require('path'),
    pdf = require('html-pdf'),
    moment = require('moment'),
    fs = require("fs"),
    helpers = require('../config/helpers');
var appRoot = path.resolve(__dirname);
var uploadImageApp = multer({ 
	storage: configs.multer.storageApp, 
	fileFilter: configs.multer.imageFilter
});

router.use(bodyParser.urlencoded({ extended: true }))
router.use(methodOverride(function(req, res){
      if (req.body && typeof req.body === 'object' && '_method' in req.body) {
        // look in urlencoded POST bodies and delete it
        var method = req.body._method
        delete req.body._method
        return method
      }
}));

//Export
var stylesXLS = {
			  headerDark: {
			    fill: {
			      fgColor: {
			        rgb: 'FFFFFF'
			      }
			    },
			    font: {
			      color: {
			        rgb: '000000'
			      },			     
			      bold: false,
			      underline: false
			    }
			  },
			  cellPink: {
			    fill: {
			      fgColor: {
			        rgb: 'FFFFCCFF'
			      }
			    }
			  },
			  cellGreen: {
			    fill: {
			      fgColor: {
			        rgb: 'FF00FF00'
			      }
			    }
			  }
			};

router.post('/:appurl/exportpowerusersuperadmin', function(req,res) {
	if(req.body.app_type == 'PDF') {
		var file = fs.readFileSync(appRoot+'/head_powerusersuperadmin.html', "utf8");
		var html='';
		Scored.find()
    	.populate('user_id', 'name avatar fb_friends')
    	.sort({ point: -1 })
    	.limit(2000)
    	.exec(function(err, poweruser) {
			for(i=0;i<poweruser.length;i++) {
				 
				var fb_friends = '-';
				if( typeof(poweruser[i]['user_id']['fb_friends']) != 'undefined' ) {
					fb_friends = poweruser[i]['user_id']['fb_friends'];
				}
				var points = '-';
				if( parseInt(poweruser[i]['point']) != 0) {
					points = poweruser[i]['point'] ;
				}
				var count_view = '-';
				if( parseInt(poweruser[i]['count_view']) != 0) {
					count_view = poweruser[i]['count_view'];
				}
				
				var share = '-';
				if( parseInt(poweruser[i]['share']) != 0 ) {
					share = poweruser[i]['share'];
				}
				
				var like = '-';
				if( parseInt(poweruser[i]['like']) != 0 ) {
					like = poweruser[i]['like'];
				}
				
				if( (i % 35) == 0 && i > 0 ) {
					html =  html + '</table>';
					html = html + '<div class="heightspace"></div>';
					html+='<table  border="1" width="80%" style="margin: auto">';
				}
								
				html+='<tr>';
				html+='<td style="width:5%">'+(i+1)+'</td>';
				html+='<td style="width:25%">'+poweruser[i]['user_id']['name']+'</td>';
				html+='<td style="width:14%">'+points+'</td>';
				html+='<td style="width:14%">'+fb_friends+'</td>';
				html+='<td style="width:14%">'+count_view+'</td>';
				html+='<td style="width:14%">'+share+'</td>';
				html+='<td style="width:14%">'+like+'</td>';
				html+='</tr>';
			}
			file = file + html + '</table></html>';
			fis.writeFile(appRoot + '/index_powerusersuperadmin.html', file, function(err) {
				if(err == null) {
					html = fs.readFileSync(appRoot+'/index_powerusersuperadmin.html', 'utf8');
					var options = { format: 'Letter' };
					var file_path = appRoot+'/Export-Power-User-Superadmin-Pdf.pdf';
					pdf.create(html, options).toFile(file_path, function(err, respone_pdf) {
						if (err) return console.log(err);
						  res.download(file_path);
						  return ;
					});
				}
			});
		});
	}else {		
		Scored.find()
    	.populate('user_id', 'name avatar fb_friends')
    	.sort({ point: -1 })
    	.limit(2000)
    	.exec(function(err, poweruser) {
    		var dataset =[];
    		var specification = {
    				no: { 
    			    displayName: 'No.',
    			    headerStyle: stylesXLS.headerDark,
    			    width: 120
    			  },
    			  users: {
    				    displayName: 'Users',
    				    headerStyle: stylesXLS.headerDark,	   
    				    width: 120 
    				  },
    			  points: {
    				    displayName: 'Points',
    				    headerStyle: stylesXLS.headerDark,   	   
    				    width: 120
    				  },
    			  friends: {
    				    displayName: 'Friends',
    				    headerStyle: stylesXLS.headerDark,
    				    width: 120
    				  },
    			  visits: {
    				    displayName: 'Visits',
    				    headerStyle: stylesXLS.headerDark,
    				    width: 120
    				  },
    			  shares: {
    				    displayName: 'Shares',
    				    headerStyle: stylesXLS.headerDark,
    				    width: 120 
    				  },
    			  likes: {
    				    displayName: 'Likes',
    				    headerStyle: stylesXLS.headerDark, 	   	   
    				    width: 120 
    				  }
    			  
    			} 
    		
    		
    		
    		for(i = 0; i < poweruser.length; i++) {
    			
    			
    			var fb_friends = '-';
				if(typeof(poweruser[i]['user_id']['fb_friends'])!='undefined') {
					fb_friends = poweruser[i]['user_id']['fb_friends'];
				}
				
				var points = '-';
				if(poweruser[i]['point']!=0) {
					points =  poweruser[i]['point'];
				}
				
				var count_view = '-';
				if(poweruser[i]['count_view']!=0) {
					count_view =  poweruser[i]['count_view'];
				}
				
				var share = '-';
				if(poweruser[i]['share']!=0) {
					share =  poweruser[i]['share'];
				}
				
				var like = '-';
				if(poweruser[i]['like']!=0) {
					like =  poweruser[i]['like'];
				}
				 
				dataset.push({
					no: (i+1),
					users: poweruser[i]['user_id']['name'],
					points: points,
					friends: fb_friends,
					visits: count_view,
					shares: share,
					likes: like
				});
    		}
    			
    		var report = excel.buildExport(
					  [ 
					    {
					      name: 'Sheet1',    
					      specification: specification,
					      data: dataset 
					    }
					  ]
					);
			res.attachment('Export-Power-User-Superadmin-XLS.xls');
			return res.send(report);
    		
    	});
	}
});

router.get('/:appurl/sp-login', function(req, res) {
	if (req.isAuthenticated()) {
		if (req.user.role == 1) {
			return res.redirect('/superadmin');
		} else {
			req.logout();
			return res.redirect('/superadmin');
		}
	} else {
		return res.render('superadmin/login.jade', {
	    	title: 'Login',
	    	message: req.flash('loginMessage'),
	    });
	}
})

router.post('/:appurl/sp-login', function(req, res, next) {
	passport.authenticate('local', function(err, user, info) {
		if (! req.body.email.trim()) {
			req.flash('loginMessage', 'The email field is empty.');
			return res.redirect('back');
		} else if (! req.body.password.trim()) {
			req.flash('loginMessage', 'The password field is empty.');
			return res.redirect('back');
		}
		
		if (err) { return next(err); }
		if (!user) { return res.redirect('back'); }
		req.logIn(user, function(err) {
			if (err) { return next(err); }
			
			if (user.role == 1) {
				return res.redirect('/superadmin/dashboard');
			} else {
				return res.redirect('back');
			}
	    });
	})(req, res, next);
});

router.get('/:appurl/add-account', isSuperAdminLoggedIn,function(req, res) {	
	req.app.locals.activeMenu = {parent: 'add-account'};
	var retainingInfo = ['','','','','',''];
	req.flash('retainingInfo', retainingInfo);
	res.render('superadmin/addaccount.jade', { 
    	title: 'Add account',
    	errorMessage: req.flash('errorMessage'),
    	retainingInfo: req.flash('retainingInfo')    	 
    }); 
})

router.post('/:appurl/add-account', isSuperAdminLoggedIn, function(req, res) {
	
	var data = {};
	var errors_list = [];
	var retainingInfo = [];
	var app_name = req.body.app_name;
	var brand_name = req.body.brand_name;
	var url = req.body.url;	
	var flag = 1;
	var only_login_fb = req.body.only_login_fb == 'on' ? true : false;
	retainingInfo.push(req.body.app_name);
	retainingInfo.push(req.body.brand_name);
	retainingInfo.push(req.body.ga_tracking_id);
	retainingInfo.push(req.body.ga_view_id);
	retainingInfo.push(req.body.url);
	retainingInfo.push(req.body.app_email);
	retainingInfo.push(req.body.profile_file1);
	retainingInfo.push(req.body.intro_file1);
	retainingInfo.push(true);
		
	async.waterfall([checkEmpty, checkAppicaltionName, checkBrandName, checkUrlValidate, checkUrl, checkEmail, checkPassword], function(err) {
		if(data['checkEmpty']) {
			for(i = 0; i < data['data_empty'].length; i++) {
				errors_list.push(data['data_empty'][i]);
			}
		}
		
		if(data['checkAppName']) {			
			errors_list.push("Application Name is empty");
		}
		
		if(data['checkBrandName']) {			
			errors_list.push("Brand Name is empty");
		}
		
		if(data['checkUrlValidate']) {			
			errors_list.push("Application URL can only contain alphanumeric and !$-_+*'(), characters.");
		}
				
		if(data['checkUrl']) {			
			errors_list.push("Url is exist");
		}
		
		if(data['checkEmail']) {			
			errors_list.push(data['emailError']);
		}
		
		if(data['checkPassword']) {
			errors_list.push('The passwords do not match');
		}
		
		if (req.body.profile_file1 == '') {
			errors_list.push('Profile Image is empty');			
		}
		
		if (req.body.intro_file1 == '') {
			errors_list.push('Application Introduction is empty');			
		} 
								
		if(errors_list.length != 0) {
			
			/*
			if (typeof req.files.profile_file != 'undefined') {
				helpers.deleteFile(req.files.profile_file[0].path,true);
			}
			
			if (typeof req.files.intro_file != 'undefined') {
				helpers.deleteFile(req.files.intro_file[0].path,true)
			} 		
			*/						
			req.flash('errorMessage', errors_list);
			req.flash('retainingInfo', retainingInfo);
			return res.redirect('back');
		} else {
			
			var newUser = new User();
			newUser.role = 2;
			newUser.name = "Admin";
			newUser.email = req.body.app_email;
			newUser.password = newUser.generateHash(req.body.app_password);
			newUser.save(function(err, user) {
				if(user) {					
					var newApp = new App();
					newApp.url = req.body.url;
					newApp.brand_name = req.body.brand_name;
					newApp.app_name = req.body.app_name;
					newApp.app_status = 1;
					newApp.user_role_save_status = 1;
					newApp.admin_id = user._id;
					newApp.ga_tracking_id = req.body.ga_tracking_id;
					newApp.ga_view_id = req.body.ga_view_id;
					newApp.only_login_fb = only_login_fb;
					
					if(req.body.profile_file1 != "" && req.body.intro_file1 =="") {
						var base64Data = req.body.profile_file1.replace(/^data:image\/png;base64,/, "");
						var imgName = Date.now() +'-pro_file.png';
						var imgPath = '../app/public/uploads/'+ imgName;
						
						fs.writeFile(imgPath, base64Data, 'base64', function(err) {
							if (! err) {
								
								newApp.profile_img = '/uploads/'+imgName;
								newApp.save(function(err, app) {
									if(app) {
										return res.redirect('/superadmin/accounts');
									}
								});
								
								
							} else {
								return res.send('Upload image error');
							}
						});
					} else if (req.body.intro_file1 !="" && req.body.profile_file1 == "") {
						
						var base64Data = req.body.intro_file1.replace(/^data:image\/png;base64,/, "");
						var imgName = Date.now() +'-intro_file.png';
						var imgPath = '../app/public/uploads/'+ imgName;
						
						fs.writeFile(imgPath, base64Data, 'base64', function(err) {
							if (! err) {
								
								newApp.intro_file = '/uploads/'+imgName;
								newApp.save(function(err, app) {
									if(app) {
										return res.redirect('/superadmin/accounts');
									}
								});
								
								
							} else {
								return res.send('Upload image error');
							}
						});
					} else if(req.body.profile_file1 !="" && req.body.intro_file1 != "") {
						
						var base64Data = req.body.profile_file1.replace(/^data:image\/png;base64,/, "");
						var imgName = Date.now() +'-pro_file.png';
						var imgPath = '../app/public/uploads/'+ imgName;
						
						fs.writeFile(imgPath, base64Data, 'base64', function(err) {
							if (! err) {
								
								newApp.profile_img = '/uploads/'+imgName;
								
								base64Data = req.body.intro_file1.replace(/^data:image\/png;base64,/, "");
								imgName = Date.now() +'-intro_file.png';
								imgPath = '../app/public/uploads/'+ imgName;
								fs.writeFile(imgPath, base64Data, 'base64', function(err) {
									if (! err) {
										
										newApp.intro_file = '/uploads/'+imgName;
										newApp.save(function(err, app) {
											if(app) {
												return res.redirect('/superadmin/accounts');
											}
										});
										
									} else {
										return res.send('Upload image error');
									}
								});
								
								
							} else {
								return res.send('Upload image error');
							}
						});
						
						
						
					} else {
						newApp.save(function(err, app) {
							if(app) {
								return res.redirect('/superadmin/accounts');
							}
						});
					}
					
					
					
					
					
				}
			});	
		}
		
	});
	
	function checkEmpty(callback) {
		
		req.checkBody('app_name', 'Application Name is empty').notEmpty();
		req.checkBody('brand_name', 'Brand Name is empty').notEmpty();
		req.checkBody('url', 'Application URL is empty').notEmpty();		
		req.checkBody('app_email', 'Admin Email is empty').notEmpty();
		if (! only_login_fb) {
			req.checkBody('app_password', 'Admin Password is empty').notEmpty();
			req.checkBody('app_repassword', 'The Admin verify password is empty').notEmpty();
		}
		
		var errors = req.validationErrors();
		var data_empty = [];
		if (errors) {
			for(var i in errors) {
				data_empty.push(errors[i]['msg']);
			}			
			data['data_empty'] = data_empty;
			data['checkEmpty'] = 1;
		}
		callback(null);
	}
	
	function checkAppicaltionName(callback) {
		App.findOne({app_name: app_name}, function(error, app) {
			if(app) {
				data['checkAppName'] = 1;
			}
			callback(null);
		});		
	}
	
	function checkBrandName(callback) {
		
		App.findOne({brand_name: brand_name}, function(error, app) {
			if(app) {
				data['checkBrandName'] = 1;
			}
			callback(null);
		});
		
	}
	
	function checkUrlValidate(callback) {
		
		var regexp = /^[a-z0-9-_]+$/;
		if(req.body.url != "") {
			if (req.body.url.search(regexp) == -1) {
				data['checkUrlValidate'] = 1;
			}
		}else {
			data['checkUrlValidate'] = 0;
		}
		
		callback(null);
	}
			
	function checkUrl(callback) {
		App.findOne({url: url}, function(error, app) {
			if(app) {
				data['checkUrl'] = 1;
			}else {
				data['checkUrl'] = 0;
			}
			callback(null);
		});
		
	}
			
	function checkEmail(callback) {
		var app_email = req.body.app_email;				
		if(!isEmail(app_email) || app_email == '') {			
			data['emailError'] = 'Admin Email is in an invalid format';
			data['checkEmail'] = 1;			
			return callback(null);
		}else {	
			
			User.findOne({email: app_email}, function(err, user) {
				if(user) {
					data['emailError'] = 'Admin Email is exist';
					data['checkEmail'] = 1;						
				}
				return callback(null);
			});	
		}
		
	}
	
	function checkPassword(callback) {
		// Pass check password if only login fb is true
		if (only_login_fb) {
			return callback(null);
		}
		
		
		if(req.body.app_password != req.body.app_repassword) {
			data['checkPassword'] = 1;				
		}		
		callback(null);
	}
	
 	
});
 
//accounts overview
router.get('/:appurl/accounts', isSuperAdminLoggedIn,function(req, res) {
	req.app.locals.activeMenu = {parent: 'accounts'};
	var data = {};
	var dataCountUser = [];
	async.waterfall([getListApp, getAppActive, getNumQuestionOfApp, getNumBonusOfApp, calculateTotalAppPage], function(err) {
		var sumUser = 0;
		for(index in data['dataCountUserApp']) {			
			sumUser+=data['dataCountUserApp'][index];
		}
		
		res.render('superadmin/accounts.jade', {
	    	title: 'Accounts Overview',
	    	listApp: data['listApp'],
	    	countAppActive: data['countAppActive'],
	    	dataCountUserApp: data['dataCountUserApp'],
	    	sumUser: sumUser,
	    	totalPages: data['totalPages'],
	    	avgPages: sumUser && data['totalPages'] ? (data['totalPages'] / sumUser).toFixed(2) : 0,
	    	totalVisits: data['totalVisits']
	    });
	});
	
	function getListApp(callback) {
		App.find(function(error, listApp) {
			data['listApp'] = listApp;
			if (listApp.length) {
				var process = true;
				var count = 0;
				var handle = setInterval(function() {				
					if(process) {
						process = false;
						Scored.where({app_id: listApp[count]._id}).count(function(err, countUser ) {
							dataCountUser.push(countUser);
							process = true;
							count++;
							if(listApp.length == count) {
								data['dataCountUserApp'] = dataCountUser;							
								clearInterval(handle);
								callback(null);
							}
						});
					}				
				},20);	
			} else {
				callback(null);
			}
		});
	}
	
	function getAppActive(callback) {
		App.where({app_status: 1}).count(function(err, count) {
			data['countAppActive'] = count;
			callback(null);
		});		
	}
	
	function getNumQuestionOfApp(callback) {
		Question.aggregate([
		    { $match: {
		    	"qs_status": "1"
		    }},
   			{ $group: {
   				_id: "$app_id",
   				num_question: { $sum: 1 }
   			}}
   		], function (err, result) {
       		data['numQuestionOfApp'] = {};
       		for(var i in result) {
       			var item = result[i];
       			data['numQuestionOfApp'][item['_id']] = item['num_question'];
       		}
   			callback(null);
   		});
	}
	
	function getNumBonusOfApp(callback) {
		Bonus.aggregate([
		    { $match: {
		    	"bonus_status": "1"
		    }},
   			{ $group: {
   				_id: "$app_id",
   				num_bonus: { $sum: 1 }
   			}}
   		], function (err, result) {
       		data['numBonusOfApp'] = {};
       		for(var i in result) {
       			var item = result[i];
       			data['numBonusOfApp'][item['_id']] = item['num_bonus'];
       		}
   			callback(null);
   		});
	}
	
	function calculateTotalAppPage(callback) {
		data['totalPages'] = 0;
		data['totalVisits'] = 0;
		if (typeof(data['listApp']) != 'undefined') {
			for (var i in data['listApp']) {
				var appItem = data['listApp'][i];
				appItem['totalQuestion'] = data['numQuestionOfApp'].hasOwnProperty(appItem['_id']) ? data['numQuestionOfApp'][appItem['_id']] : 0;
				appItem['totalBonus'] = data['numBonusOfApp'].hasOwnProperty(appItem['_id']) ? data['numBonusOfApp'][appItem['_id']] : 0;
				appItem['totalPages'] =  appItem['totalQuestion'] + appItem['totalBonus'];
				data['totalPages'] += appItem['totalPages'];
				data['totalVisits'] += appItem.visited ? appItem.visited : 0;
			}
		}
		callback(null);
	}
})

router.get('/:appurl/edit-account/:urlapp', isSuperAdminLoggedIn,function(req, res) {
	req.app.locals.activeMenu = {parent: 'accounts'};
	App.findOne({
		_id: req.params['urlapp']
	})
	.populate('admin_id', 'email')
	.exec(function(err, app) {
		if (app) {
			res.render('superadmin/updateaccount.jade', { 
		    	title: 'Edit account',
		    	dataApp: app,
		    	userEmail: app['admin_id']['email'],
		    	successfulMessage: req.flash('successfulMessage'),
		    	errorMessage: req.flash('errorMessage')
		    });	
		} else {
			res.redirect('back');
		}
	});
})

router.post('/:appurl/edit-account/', isSuperAdminLoggedIn, function(req, res) {
			var data = {};
			var errors_list = [];
			var app_id = req.body.app_id;
			var url = req.body.url;
			var only_login_fb = req.body.only_login_fb == 'on' ? true : false;
			
			//console.log(req.body);
			//return ;
	async.waterfall([checkEditEmpty, checkEditApp, checkEditBrand, checkEditUrl,checkUpdateUrlValidate, checkEditPass, updateUser, updateApp], function(err) {
		 
		if(data['checkEmpty']) {			
			for(i = 0; i < data['data_empty'].length; i++) {
				errors_list.push(data['data_empty'][i]);
			}
		}
		
		if(data['checkEditApp']) errors_list.push("The application name is exist");
		if(data['checkEditBrand']) errors_list.push("The brand name is exist");
		if(data['checkEditUrl']) errors_list.push("The url is exist");
		if(data['checkUrlValidate']) {			
			errors_list.push("Application Url expect alphabet, number, dash and underscore character.");
		}
		if(data['checkEditPass']) errors_list.push("The password mismatch");
				
		
		if(errors_list.length != 0) {
			req.flash('errorMessage', errors_list);
			return res.redirect('back');
		}else {
			req.flash('successfulMessage', ['Update successfully']);
			return res.redirect('back');
		}
	});
	
		function checkUpdateUrlValidate(callback) {		
			var regexp = /^[a-z0-9-_]+$/;
			if (req.body.url.search(regexp) == -1) {
				data['checkUrlValidate'] = 1;				
			}
			callback(null);
		}
		
		function checkEditEmpty(callback) {					
			req.checkBody('app_name', 'The application name is empty').notEmpty();
			req.checkBody('brand_name', 'The brand name is empty').notEmpty();
			req.checkBody('url', 'The url is empty').notEmpty();		
			req.checkBody('app_email', 'The email is empty').notEmpty();
			var errors = req.validationErrors();
			var data_empty = [];		
			if (errors) {
				for(var i in errors) {
					data_empty.push(errors[i]['msg']);
				}			
				data['data_empty'] = data_empty;
				data['checkEmpty'] = 1;				
			}
			callback(null);
		}
		
		function checkEditApp(callback) {		
			App.findOne({_id: app_id}, function(err, app) {
				if(app) {
					if(app.app_name == req.body.app_name || req.body.app_name == '') {
						data['checkEditApp'] = 0;
						return callback(null);
					}					
					if(app.app_name != req.body.app_name) {
						var process = true;
						var handle = setInterval(function() {				
							if(process) {
								process = false;			
								App.findOne({app_name: req.body.app_name}, function(err, app2) {
									if(!app2) {
										data['checkEditApp'] = 0;							
									}
									else {
										data['checkEditApp'] = 1;							
									}
									clearInterval(handle);
									callback(null);
									 
								});
							}
						},20);									
					}
					
				}
			});
		}
		
		function checkEditBrand(callback) {			
			App.findOne({_id: app_id}, function(err, app) {
				if(app) {
					if(app.brand_name == req.body.brand_name || req.body.brand_name == '' ) {
						data['checkEditBrand'] = 0;
						return callback(null);
					}					
					if(app.brand_name != req.body.brand_name) {
						var process = true;
						var handle = setInterval(function() {				
							if(process) {
								process = false;			
								App.findOne({brand_name: req.body.brand_name}, function(err, app2) {					
									if(!app2) {
										data['checkEditBrand'] = 0;									
									}
									else {
										data['checkEditBrand'] = 1;							
									}
									clearInterval(handle);
									callback(null);
								});	
							}
						},20);
					}
					
				}
			});
		}
		
		function checkEditUrl(callback) {		
			if(url != '') {
				App.findOne({url: url}, function(err, app) {
					if(app) {
						if(app.url == req.body.url || req.body.url == '' )
							{
								data['checkEditUrl'] = 0;
								return callback(null);
							}
						if(app.url != req.body.url) {
							var process = true;
							var handle = setInterval(function() {				
								if(process) {
									process = false;			
									App.findOne({url: req.body.url}, function(err, app2) {					
										if(!app2) {
											data['checkEditUrl'] = 0;									
										}
										else {
											data['checkEditUrl'] = 1;							
										}
										clearInterval(handle);
										callback(null);
									});	
								}
							},20);
						}
						
					}else {
						data['checkEditUrl'] = 0;
						callback(null);
					}
				});
			}else {
				data['checkEditUrl'] = 0;
				callback(null);
			}
		}
		
		function checkEditPass(callback) {		
			if(req.body.app_password != '' || req.body.app_repassword != '') {
				if(req.body.app_password != req.body.app_repassword ) {
					data['checkEditPass'] = 1;			
					callback(null);
				}else {
					data['checkEditPass'] = 0;
					callback(null);
				}		
			} else {			
				data['checkEditPass'] = 0;
				callback(null);
			}		
		}
		
		
		function updateUser(callback) {	
			var process = true;
					User.findOne({email: req.body.app_email}, function(err, user) {		
						//change email
						if(!user)
						{						
							var handle = setInterval(function() {				
								if(process) {
									process = false;	
										User.findOne({_id: req.body.app_user_id }, function(err, user2) {
											user2.email = req.body.app_email;
											if(req.body.app_password) {	
												user2.password = user2.generateHash(req.body.app_password);
											}
											user2.save(function(err) {
												data['adminId'] = 1;
												clearInterval(handle);
												callback(null);
											});
											
											
										});
								}
							},20);																
						}else{					
							if(user._id == req.body.app_user_id) {
								user.email = req.body.app_email;
								if (req.body.app_password != '') {
									user.password = user.generateHash(req.body.app_password);
								}
								
								user.save(function(err) {
									data['adminId'] = 1;
									callback(null);
								});	
							} else {
								if (typeof data['data_empty'] == "undefined") {
									data['data_empty'] = [];
								}
								data['data_empty'].push('Email exist.');
								data['checkEmpty'] = 1;	
								callback(true);
							}						
						}			
					});		
			 
		}
		
		function updateApp(callback) {	
			
			App.findOne({_id: app_id}, function(err,app) {
				if(app) {
					var dataSave = {};
					if(req.body.profile_file1 == "") {
						dataSave = {
								brand_name:	req.body.brand_name,
								app_name:	req.body.app_name,
								url:	req.body.url,
								ga_tracking_id: req.body.ga_tracking_id,
								ga_view_id: req.body.ga_view_id,
								app_status: req.body.status,
								user_role_save_status: 1,
								only_login_fb: only_login_fb
							}
						App.updateData(app_id, dataSave, function() {
							data['updateApp'] = 1;
							callback('null');
						});
					} else if( req.body.profile_file1 != "" && req.body.intro_file1 == "" ){

						var base64Data = req.body.profile_file1.replace(/^data:image\/png;base64,/, "");
						var imgName = Date.now() +'-pro_file.png';
						var imgPath = '../app/public/uploads/'+ imgName;
						
						fs.writeFile(imgPath, base64Data, 'base64', function(err) {
							if (! err) {
								dataSave['profile_img'] = '/uploads/'+imgName;
								App.updateData(app_id, dataSave, function() {
									data['updateApp'] = 1;
									callback('null');
								});	
							} else {
								return res.send('Upload image error');
							}
						});
					} else if(req.body.intro_file1 == "") {
						dataSave = {
								brand_name:	req.body.brand_name,
								app_name:	req.body.app_name,
								url:	req.body.url,
								ga_tracking_id: req.body.ga_tracking_id,
								ga_view_id: req.body.ga_view_id,
								app_status: req.body.status,
								user_role_save_status: 1,
								only_login_fb: only_login_fb
							}
						App.updateData(app_id, dataSave, function() {
							data['updateApp'] = 1;
							callback('null');
						});
					} else if(req.body.intro_file1 != "" && req.body.profile_file1 == "") {
						var base64Data = req.body.intro_file1.replace(/^data:image\/png;base64,/, "");
						var imgName = Date.now() +'-intro_file.png';
						var imgPath = '../app/public/uploads/'+ imgName;
						
						fs.writeFile(imgPath, base64Data, 'base64', function(err) {
							if (! err) {
								dataSave['intro_file'] = '/uploads/'+imgName;
								App.updateData(app_id, dataSave, function() {
									data['updateApp'] = 1;
									callback('null');
								});	
							} else {
								return res.send('Upload image error');
							}
						});
					} else if(req.body.profile_file1 != "" && req.body.intro_file1 != "") {
						
						var base64Data = req.body.profile_file1.replace(/^data:image\/png;base64,/, "");
						var imgName = Date.now() +'-pro_file.png';
						var imgPath = '../app/public/uploads/'+ imgName;
						
						fs.writeFile(imgPath, base64Data, 'base64', function(err) {
							if (! err) {
								dataSave['profile_img'] = '/uploads/'+imgName;
								
								base64Data = req.body.intro_file1.replace(/^data:image\/png;base64,/, "");
								imgName = Date.now() +'-intro_file.png';
								imgPath = '../app/public/uploads/'+ imgName;
								
								
								fs.writeFile(imgPath, base64Data, 'base64', function(err) {
									if (! err) {
										dataSave['intro_file'] = '/uploads/'+imgName;
										
										App.updateData(app_id, dataSave, function() {
											data['updateApp'] = 1;
											callback('null');
										});	
										
										
									} else {
										return res.send('Upload image error');
									}
								});
								
								
							} else {
								return res.send('Upload image error');
							}
						});
					}

					
					
					
					
					
					
					/*
					if (typeof req.files.profile_file != 'undefined') {
						dataSave['profile_img'] = '/uploads/'+req.files.profile_file[0].filename;
					}
					
					if (typeof req.files.intro_file != 'undefined') {
						dataSave['intro_file'] = '/uploads/'+req.files.intro_file[0].filename;
					}
					*/
					
					
				}
			});
		}
})

// Dashboard
router.get('/:appurl/dashboard', isSuperAdminLoggedIn,function(req, res) {
	req.app.locals.activeMenu = {parent: 'dashboard'};
	var data = {},
		signupThisMonth = [],
		numDayFromBeginThisMonth = 1;
	async.waterfall([
	     getAppViews,
	     getPowerUser,
         getNumMale,
         getNumFemale,
         getTotalPageView,
         getTotalSignup,
         getUniqueToday,
         getSignUpThisMonth,
         getTotalUnique,
         getAppRepeat,
         getAvgRepeatRatio,
    ], function (err) {
    	var totalGender = data['numMale'] + data['numFemale'];
		res.render('superadmin/dashboard.jade', { 
	    	title: 'Dashboard',
	    	appViews: JSON.stringify(data['appViews']),
	    	poweruser: data['poweruser'],
	    	numMale: data['numMale'],
	    	numFemale: data['numFemale'],
	    	percentMale: totalGender == 0 ? 0 : (data['numMale'] / totalGender * 100) % 1 == 0.5 ? Math.floor((data['numMale'] / totalGender * 100)): (data['numMale'] / totalGender * 100).toFixed(0),
			percentFemale: totalGender == 0 ? 0 : (data['numFemale'] / totalGender * 100).toFixed(0),
			totalPageView: data['totalPageView'],
			totalNewSignup: data['totalNewSignup'],
			appViewToDate: data['appViewToDate'],
			avgPageVisits: data['totalPageView'] && data['totalUnique'] ? (data['totalPageView']/data['totalUnique']).toFixed(1) : 0,
			uniqueToday: data['uniqueToday'],
			signupThisMonth: JSON.stringify(signupThisMonth),
			numDayFromBeginThisMonth: numDayFromBeginThisMonth,
			totalUnique: data['totalUnique'],
			appRepeats: JSON.stringify(data['appRepeats']),
			appRepeatToDate: data['appRepeatToDate'],
			avgRepeatRatio: data['avgRepeatRatio']
	    }); 
    });
	
	function getAppViews(callback) {
    	var toDate = req.app.locals.moment().endOf('day')._d.toISOString(),
    		fromDate = req.app.locals.moment().subtract(8,'d').endOf('day')._d.toISOString();
    	
    	AppViews.find({
    		"date": {
    			"$gte": fromDate,
    			"$lte": toDate
    		}
    	})
		.sort({ date: 1 })
		.exec(function(err, appViews) {
			data['appViews'] = appViews;
			data['appViewToDate'] = req.app.locals.moment().format('YYYY-MM-DD');
    		callback(null);
		});
    }
	
	function getNumMale(callback) {
    	Scored.count({
    		"fb_gender": "male"
    	})
    	.exec(function(err, numMale) {
    		data['numMale'] = numMale
    		callback(null);
    	});
    }
    
    function getNumFemale(callback) {
    	Scored.count({
    		"fb_gender": "female"
    	})
    	.exec(function(err, numFemale) {
    		data['numFemale'] = numFemale
    		callback(null);
    	});
    }
    function getPowerUser(callback) {
    	Scored.find()
    	.populate('user_id', 'name avatar fb_friends')
    	.sort({ point: -1 })
    	.limit(2000)
    	.exec(function(err, scoreds) {
    		data['poweruser'] = scoreds;
    		callback(null);
    	});
    }
    
    function getTotalPageView(callback) {
    	AppViews.aggregate([
	    	{ $match: {}},
			{ $group: {
				_id: null,
				total_views: { $sum: "$count_view"  }
			}}
		], function (err, result) {
			data['totalPageView'] = 0;
			if (! err && result.length) {
				data['totalPageView'] = result[0]['total_views'];
		    }
			callback(null);
		});
    }
    
    function getTotalSignup(callback) {
    	Scored.count({})
    	.exec(function(err, totalNewSignup) {
    		data['totalNewSignup'] = totalNewSignup;
    		callback(null);
    	});
    }
    
    function getUniqueToday(callback) {
    	AppRepeat.aggregate([
	    	{ $match: {
	    		"date": {"$gte": req.app.locals.moment().startOf('day')._d}
			}},
			{ $group: {
				_id: null,
				total_unique: { $sum: "$user_signup_count"  }
			}}
		], function (err, result) {
    		data['uniqueToday'] = 0;
			if (! err && result.length) {
				data['uniqueToday'] = result[0]['total_unique'];
		    }
			callback(null);
		});
    }
    
    function getSignUpThisMonth(callback) {
    	Scored.getSignUpOfMonth(false, req.app.locals.moment().format('YYYY-MM'), function(scoreds, numDay) {
    		signupThisMonth = scoreds;
    		numDayFromBeginThisMonth = numDay;
    		callback(null);
    	})
    }
    
    function getTotalUnique(callback) {
    	AppRepeat.aggregate([
 	    	{ $match: {}},
 			{ $group: {
 				_id: null,
 				total_unique: { $sum: "$user_signup_count"  }
 			}}
 		], function (err, result) {
     		data['totalUnique'] = 0;
 			if (! err && result.length) {
 				data['totalUnique'] = result[0]['total_unique'];
 		    }
 			callback(null);
 		});
    }
    
    function getAppRepeat(callback) {
    	var toDate = req.app.locals.moment().endOf('day')._d.toISOString(),
			fromDate = req.app.locals.moment().subtract(8,'d').endOf('day')._d.toISOString();

    	AppRepeat.aggregate([
  	    	{ $match: {
  	    		"date": {
  					"$gte": req.app.locals.moment().subtract(7,'d').endOf('day')._d,
  					"$lte": req.app.locals.moment().endOf('day')._d
  				}
  	    	}},
  			{ $group: {
  				_id: "$date",
  				user_signup_count: { $sum: "$user_signup_count" },
  				user_login_count: { $sum: "$user_login_count" }
  			}}
  		], function (err, result) {
      		data['appRepeats'] = result;
			data['appRepeatToDate'] = req.app.locals.moment().format('YYYY-MM-DD');
  			callback(null);
  		});
    }
    
    function getAvgRepeatRatio(callback) {
    	AppRepeat.aggregate([
			{ $group: {
				_id: null,
				total_unique: { $sum: "$user_signup_count"  },
				total_repeat: { $sum: "$user_login_count"  }
			}}
		], function (err, result) {
    		data['avgRepeatRatio'] = 0;
			if (! err && result.length) {
				data['avgRepeatRatio'] = result[0]['total_repeat'] && result[0]['total_unique'] ? (result[0]['total_repeat'] / result[0]['total_unique']).toFixed(2) : 0; 
		    }
			callback(null);
		});
    }
    
})

//settting

router.get('/:appurl/setting', isSuperAdminLoggedIn, function(req, res) {	
    res.render('superadmin/setting.jade', { 
    	title: 'Setting',
    	user: req.user,
    	message: req.flash('settingMessage'),
    	successful: req.flash('successfulMessage')
    }); 
});

router.post('/:appurl/setting', isSuperAdminLoggedIn, function(req, res) {
	
	var app_password = req.body.app_password;
	var app_newpassword = req.body.app_newpassword;
	var app_verpassword = req.body.app_verpassword;	
	
	req.checkBody('app_password', 'Current password is empty').notEmpty();
	req.checkBody('app_newpassword', 'New password is empty').notEmpty();
	req.checkBody('app_verpassword', 'Verify password is empty').notEmpty();
	var errors = req.validationErrors();			
	if (errors) {
		var errors_list = [];
		for(var i in errors) {
			errors_list.push(errors[i]['msg']);
		}
		req.flash('settingMessage', errors_list);		
		return res.redirect('back');
	} else {	
		if(req.body.app_newpassword != req.body.app_verpassword) {
			req.flash('settingMessage', ['The new password is mismatch']);		
			return res.redirect('back');
		}
	}
	
	User.findOne({_id: req.user.id}, function(error, user) {
		if(user) {											
			if(user.validPassword(app_password)) {							
				user.password = user.generateHash(app_newpassword);
				user.save(function(err) {
					req.flash('successfulMessage', ['Update successfully']);			
					return res.redirect('back');
				});
			} else {
				req.flash('settingMessage', ['Current password not match.']);
				return res.redirect('back');
			}
		} else {
			req.flash('settingMessage', ['Cannot find user!']);		
			return res.redirect('back');
		}
	});	
});

//route middleware to make sure super admin is logged in
function isSuperAdminLoggedIn(req, res, next) {
	if (req.isAuthenticated() && req.user.role == 1) {
		req.app.locals.user = {id: req.user.id, name: req.user.name};
		return next();
	}
	
	//req.logout();
	return res.redirect('/superadmin');
}

function isEmail(email) { 
    return /^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))$/i.test(email);
}

module.exports = router;