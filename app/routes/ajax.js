var express = require('express'),
    router = express.Router(),
    AppViews = require('../models/appviews'),
    App = require('../models/apps'),
    User = require('../models/users'),
    AppRepeat = require('../models/apprepeat'),
    Scored = require('../models/scoreds'),
    nodemailer = require('nodemailer'),
    configs = require('../config/configs'),
    smtpTransport = require('nodemailer-smtp-transport');


//config STMP

var transporter = nodemailer.createTransport(smtpTransport({
	host: configs.smtpconfig.host,
	port: configs.smtpconfig.port,
	auth: {
		user: configs.smtpconfig.user,
		pass: configs.smtpconfig.pass
	}
}));

router.post('/:appurl/apiGetPageView/:type', function(req, res) {
	var type = req.params['type'],
		toDate = '',
		fromDate = '',
		numDate = 0,
		formatTodate = '',
		appurl = req.params['appurl'] != undefined && req.params['appurl'] ? req.params['appurl']: '';
		
	if (type != 'custom') {
		var	subtractNum = type == 'week' ? 8 : 31;
		numDate = req.app.locals.moment().diff(req.app.locals.moment().subtract(subtractNum,'d'), 'days');
		fromDate = req.app.locals.moment().subtract(subtractNum,'d').endOf('day')._d.toISOString();
		toDate = req.app.locals.moment().endOf('day')._d.toISOString();
		formatTodate = req.app.locals.moment().format('YYYY-MM-DD');
	} else {
		numDate = req.app.locals.moment(req.body['to']).diff(req.app.locals.moment(req.body['from']), 'days') + 1;
		fromDate = req.app.locals.moment(req.body['from']).startOf('day')._d.toISOString();
		toDate = req.app.locals.moment(req.body['to']).endOf('day')._d.toISOString();
		formatTodate = req.app.locals.moment(req.body['to']).format('YYYY-MM-DD');
	}
	
	var condition = {
		"date": {
			"$gte": fromDate,
			"$lte": toDate
		}
	}
	App.findOne({ 'url' : appurl }, 'url', function(err, app) {
		if (req.params['appurl'] != 'superadmin') {
			condition['app_id'] = app._id;
		}

		AppViews.find(condition)
		.sort({ date: 1 })
		.exec(function(err, appViews) {
			res.json({data: appViews, datenum: numDate, toDate: formatTodate});
		});
	})
});

router.post('/:appurl/apiGetAppRepeat/:type', function(req, res) {
	var type = req.params['type'],
		toDate = '',
		fromDate = '',
		numDate = 0,
		formatTodate = '';
	
	if (type != 'custom') {
		var	subtractNum = type == 'week' ? 8 : 31;
		numDate = req.app.locals.moment().diff(req.app.locals.moment().subtract(subtractNum,'d'), 'days');
		fromDate = req.params['appurl'] != 'superadmin' ? req.app.locals.moment().subtract(subtractNum,'d').endOf('day')._d.toISOString() : req.app.locals.moment().subtract(subtractNum,'d').endOf('day')._d;
		toDate = req.params['appurl'] != 'superadmin' ? req.app.locals.moment().endOf('day')._d.toISOString() : req.app.locals.moment().endOf('day')._d;
		formatTodate = req.app.locals.moment().format('YYYY-MM-DD');
	} else {
		numDate = req.app.locals.moment(req.body['to']).diff(req.app.locals.moment(req.body['from']), 'days') + 1;
		fromDate = req.params['appurl'] != 'superadmin' ? req.app.locals.moment(req.body['from']).startOf('day')._d.toISOString() : req.app.locals.moment(req.body['from']).startOf('day')._d;
		toDate = req.params['appurl'] != 'superadmin' ? req.app.locals.moment(req.body['to']).endOf('day')._d.toISOString() : req.app.locals.moment(req.body['to']).endOf('day')._d;
		formatTodate = req.app.locals.moment(req.body['to']).format('YYYY-MM-DD');
	}
	
	if (req.params['appurl'] != 'superadmin') {
		App.findOne({ 'url' : req.params['appurl'] }, 'url', function(err, app) {
			AppRepeat.find({
				"app_id": app._id,
				"date": {
					"$gte": fromDate,
					"$lte": toDate
				}
			})
			.sort({ date: 1 })
			.exec(function(err, appViews) {
				res.json({data: appViews, datenum: numDate, toDate: formatTodate});
			});
		})
		
	} else {
		AppRepeat.aggregate([
   	    	{ $match: {
   	    		"date": {
   					"$gte": fromDate,
   					"$lte": toDate
   				}
   	    	}},
   			{ $group: {
   				_id: "$date",
   				user_signup_count: { $sum: "$user_signup_count" },
   				user_login_count: { $sum: "$user_login_count" }
   			}}
   		], function (err, result) {
			res.json({data: result, datenum: numDate, toDate: formatTodate});
   		});
	}
});

router.post('/:appurl/apiGetSignupByMonth/:month', function(req, res) {
	var month = req.params['month'];
	if (req.params['appurl'] != 'superadmin') {
		App.findOne({ 'url' : req.params['appurl'] }, 'url', function(err, app) {
			Scored.getSignUpOfMonth(app._id, month, function(scoreds, numDay) {
				res.json({data: scoreds, datenum: numDay});
			});
		});
	} else {
		Scored.getSignUpOfMonth(false, month, function(scoreds, numDay) {
			res.json({data: scoreds, datenum: numDay});
		});
	}
	
});

router.post('/:appurl/increasePageView', function(req, res) {
	var appurl = req.params['appurl'] != undefined && req.params['appurl'] ? req.params['appurl']: '';
	
	App.findOne({ 'url' : appurl }, 'url', function(err, app) {
		if (app) {
			// Increase count user view quesion, bonus
			Scored.increaseView(app._id, req.user.id);
			
			AppViews.increaseView(app._id, function(appview) {
				return res.json({success: 1, appview: appview});
			});
		} else {
			return res.json({success: 0, msg: 'Can not found application.'});
		}
	});
    
});

// Forget pass

router.post('/:appurl/forget-password', function(req, res) {
	User.findOne({email: req.body.email}, function(err, user){		
		if(!user) {			
			return res.json({success: -1});
		}else{		 
			if(!user.fb_id) {
				var stringInput = req.body.email+',' + req.app.locals.moment().format('YYYY-MM-DD HH:mm:ss');
				var buffer = new Buffer(stringInput);
				var stringEncode = buffer.toString('base64');			 
				var linkReset = req.protocol +'://'+req.headers.host+'/'+req.params['appurl']+'/reset-password/'+stringEncode;
				var	htmlemail='<span>Hey there,</span><br/><br/>';
					htmlemail +='<span>Someone requested a new password for your Quizzu Account. Please click on the link below to reset your password.</span><br/><br/>';
					htmlemail +='<a href="'+linkReset+'"><strong><u>Reset Password</u></strong></a><br/><br/>';
					htmlemail +='<span>You can safely ignore this email if you did not request for this change.</span><br/><br/>';
					htmlemail +='<span>PS: We would love to hear from you. Please do reply this email or drop us a note at <a href="mailto:#">support@quizzu.rocks</a> if you wish to ask something or just to say hello!</span><br/><br/>';
					htmlemail +='<span>Yours,</span><br/>';
					htmlemail +='<span>The Quizzu team</span><br/>';
				var message = {
					    // sender info
					    from: 'Quizzu team <support@quizzu.rocks>',
					    to: '"User" <'+req.body.email+'>',
					    subject:'Reset Password',  
					    html: htmlemail	 
					};
				
				transporter.sendMail(message, function(error){					
						if(error == null) {						
							user.resetpass = 1;
							user.save();
							 return res.json({success: 1});						 
						}

						if(error){
							return res.json({success: 0});
						}
						else {					  
							  user.resetpass = 1;
							  user.save(function(err) {
								  if(!err) {
									  return res.json({success: 1});
								  }
							  });
						  }		  
						});
			}else {
				return res.json({success: -2});
			}
		}
	});
	 
});

router.post('/:appurl/reset-password', function(req, res) {
	var token = req.body.token;
	var buffer = new Buffer(token, 'base64');
	var stringDecode = buffer.toString('ascii');
	stringDecode = stringDecode.split(',');
	if(stringDecode.length !=2 || req.app.locals.moment(stringDecode[1])._d == "Invalid Date") {
		res.json({code: 0, message: 'Error'});
	}
	 
	User.findOne({email: stringDecode[0]}, function(err, user) {
		if(user) {
			user.password = user.generateHash(req.body.password);
			user.resetpass = 0;
			user.save(function(err) {
				if(!err) {
					res.json({code: 1, message: 'Success'});
				}
			});
		}else {			
			res.json({code: 0, message: 'Error'});
		}
	});
	 
});

//newsletter

router.post('/newsletter', function(req, res) {
	 
	req.assert('email', 'required').notEmpty();
	req.assert('email', 'valid email required').isEmail();
	var errors = req.validationErrors();
	
	if(!errors) {
		var	htmlemail= '<h1>New register - email: '+req.body.email+'</h1>';
		var message = {
				from: 'No-reply <no-reply@'+configs.domain+'>',
				to: '"User" <'+configs.adminemail+'>',
				subject: 'New register ✔',  
				html: htmlemail	 
			};
		transporter.sendMail(message, function(error){
			if(error == null) {
				 return res.json({success: 1});
			}

			if(error){
				console.log(error);
				return res.json({success: 0});
			}
			else {
				user.resetpass = 1;
				user.save(function(err) {
					if(!err) {
						return res.json({success: 1});
					}
				  });
			  }
			});
	}else {
		return res.json({success: "-1"});
	}
	
});

function isLoggedIn(req, res, next) {

    // if user is authenticated in the session, carry on 
    if (req.isAuthenticated())
        return next();

    // if they aren't redirect them to the home page
    res.redirect(req.app.locals.appUrl +'/');
}

router.post('/:appurl/apiLandingPage', function(req, res) {
	var response = {success: 1, message: '', timeServer: req.app.locals.moment().format('YYYY-MM-DD HH:mm:ss'), getTZ: req.app.locals.moment.tz.guess()};
	App.findOne({url: req.params['appurl']}, function(err, app) {
		if(app) {
			AppRepeat.upsert(app._id, req.body.firstTime, req.body.date, req.body.increaseUnique, function(appRepeat) {
				res.json(response);
			});
		} else {
			response.success = 0;
			response.message = "Can not found application.";
			res.json(response);
		}
	});
});

router.post('/:appurl/apiGetUniqueToday', function(req, res) {
	var uniqueToday = 0;
	
	if (req.params['appurl'] != 'superadmin') {
		App.findOne({ 'url' : req.params['appurl'] }, 'url', function(err, app) {
			var beginToday = req.app.locals.moment().startOf('day').format('YYYY-MM-DDT00:00:00.000') + 'Z';
	    	
	    	AppRepeat.findOne({
	    		"app_id": app._id,
	    		"date": {"$gte": beginToday}
	    	}, function(err, appRepeat) {
	    		if (appRepeat) {
	    			uniqueToday = appRepeat.user_signup_count;
	    		}
	    		res.json({uniqueToday: uniqueToday});
	    	});
		});
	} else {
		AppRepeat.aggregate([
  	    	{ $match: {
  	    		"date": {"$gte": req.app.locals.moment().startOf('day')._d}
  			}},
  			{ $group: {
  				_id: null,
  				total_unique: { $sum: "$user_signup_count"  }
  			}}
  		], function (err, result) {
  			if (! err && result.length) {
  				uniqueToday = result[0]['total_unique'];
  		    }
  			res.json({uniqueToday: uniqueToday});
  		});
	}
});

router.post('/:appurl/invite-friend', function(req, res) {
	String.prototype.capitalize = function() {
	    return this.replace(/(?:^|\s)\S/g, function(a) { return a.toUpperCase(); });
	};
	var nameofFriend = req.user.name.capitalize(),
		nameofApp = req.app.locals.currentApp.name;
		
		
	User.findOne({email: req.body.email}, function(err, user){		
		if(user) {			
			return res.json({success: -1});
		}else{
			var stringInput = {user_id: req.user.id, email: req.body.email};
			var buffer = new Buffer(JSON.stringify(stringInput));
			var stringEncode = buffer.toString('base64');			 
			var linkInvite = req.protocol +'://'+req.headers.host+'/'+req.params['appurl']+'/sign-up/token/'+stringEncode;
			var linkbtnChallenge = req.protocol +'://'+req.headers.host+'/images/btn-challenge.png';
			var htmlemail = "<h1 style='text-align:center;font-size:26px;'><center><b>You’ve been challenged!</b></center></h1><br/>";	
				htmlemail += "<p>" + nameofFriend + " believes you have what it takes to the top the ranks of the quiz for " + nameofApp + ".</p><br/>";
				htmlemail += "<p>All you have to so is to click on the button below to showcase your prowess as the quiz expert that the world has been waiting for!</p><br/>";
				htmlemail += "<div style='text-align:center'><a style='text-align:center;' href='"+ linkInvite +"'><center><img src='" + linkbtnChallenge + "' /></center></a></div><br/><br/>";
				htmlemail += "<p>You can also copy/paste this link into your browser:</p>";
				htmlemail += "<p><a href='"+ linkInvite +"'>" + linkInvite + "</a></p>";
				htmlemail += "<p>We’ll see you there!</p><br/>";
				htmlemail += "<p>Sincerely,</p>";
				htmlemail += "<p>The Quizzu! Team</p><br/>";
			
			var message = {
				    // sender info
				    from: '<invites@quizzu.rocks>',
				    to: '"User" <'+req.body.email+'>',
				    subject: "You've been challenged",  
				    html: htmlemail	 
				};
			
			transporter.sendMail(message, function(error){
				if(error == null) {
					 return res.json({success: 1});
				} else {
					return res.json({success: 0});
				}
			})
		}
	});
	 
});

module.exports = router;