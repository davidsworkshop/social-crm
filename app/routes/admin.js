var express = require('express'),
    router = express.Router(),
    mongoose = require('mongoose'), //mongo connection
    bodyParser = require('body-parser'), //parses information from POST
    methodOverride = require('method-override'), //used to manipulate POST
    User = require('../models/users'),
    App = require('../models/apps'),
    Question = require('../models/questions'),
    Scored = require('../models/scoreds'),
    Bonus = require('../models/bonus'),
    AppViews = require('../models/appviews'),
    AppRepeat = require('../models/apprepeat'),
    multer  = require('multer'),
    util = require('util'),
    async = require("async"),
    configs = require('../config/configs'),    
    helpers = require('../config/helpers'),
    excel = require('node-excel-export'),
    fis = require('file-system'),
    path = require('path'),
    pdf = require('html-pdf'),
    moment = require('moment'),
    fs = require("fs");
var appRoot = path.resolve(__dirname);

app.locals.previewQuestion = {};
app.locals.previewBonus = {};

router.use(bodyParser.urlencoded({ extended: true }))
router.use(methodOverride(function(req, res){
      if (req.body && typeof req.body === 'object' && '_method' in req.body) {
        // look in urlencoded POST bodies and delete it
        var method = req.body._method
        delete req.body._method
        return method
      }
}));

//route middleware to validate app url
router.param('appurl', function(req, res, next, appurl) {
	// Set admin menu flag
	req.app.locals.activeMenu = {parent: '', child: ''};
	if (appurl == 'superadmin') {
		next();
	} else {
		App.findOne({ 'url' : appurl }, function(err, app) {
			if (err || ! app) {
	            res.status(404);
	            res.render('404.jade');
			} else {
				req.app.locals.currentApp = {
					url : app.url,
					name : app.app_name,
					id : app._id,
					admin_id: app.admin_id,
					admin_access: app.app_status == 0 && app.user_role_save_status == 1 ? false : true,
					font_color: app.font_color,
					theme_destop_bg: app.theme_destop_bg,
					profile_img: app.profile_img,
					intro_file: app.intro_file,
					ga_view_id: app.ga_view_id,
					point_adwarded: app.point_adwarded ? app.point_adwarded : 0,
					point_deducted: app.point_deducted ? app.point_deducted : 0
					
				};
				req.app.locals.appUrl = '/'+ app.url;
				
				next();
			}
		});
	}
});

var uploadImage = multer({ 
	storage: configs.multer.storageQuestion, 
	fileFilter: configs.multer.imageFilter
})

var uploadImageApp = multer({ 
	storage: configs.multer.storageApp, 
	fileFilter: configs.multer.imageFilter
})

//Export
var stylesXLS = {
			  headerDark: {
			    fill: {
			      fgColor: {
			        rgb: 'FFFFFF'
			      }
			    },
			    font: {
			      color: {
			        rgb: '000000'
			      },			     
			      bold: false,
			      underline: false
			    }
			  },
			  cellPink: {
			    fill: {
			      fgColor: {
			        rgb: 'FFFFCCFF'
			      }
			    }
			  },
			  cellGreen: {
			    fill: {
			      fgColor: {
			        rgb: 'FF00FF00'
			      }
			    }
			  }
			};

router.post('/:appurl/admin/exportbonus',isAdminLoggedIn, function(req,res) {
	if(req.body.app_type == 'PDF') {
		App.findOne({url: req.params['appurl']}, 'url', function(err,app) {
			Bonus
			.find({app_id: app._id})
			.sort({ order: 1 })
			.exec(function(err, bonus) {
				var file = fs.readFileSync(appRoot+'/head_bonus.html', "utf8");
				file = file.replace("{app_name}", req.body.app_name.toUpperCase());
				var html = '';
				 for(i=0;i<bonus.length;i++) {
					 var score = 0;
					 if(bonus[i]['count_views'] != 0)  {
						 score = Math.round((parseInt(bonus[i]['count_share_like'])/parseInt(bonus[i]['count_views']))*100)+'%';
					 }else {
						 score = Math.round((parseInt(bonus[i]['count_share_like']/1)*100))+'%';
					 }
					 
					 var share = '-';
					 var like = '-';				 
					 if(bonus[i]['required_action'] == 'share' ) {
						 share = bonus[i]['count_share_like'];
						 like = '-';
					 }else {
						 share = '-';
						 like = bonus[i]['count_share_like'];
					 }
					 
					 if( (i % 35) == 0 && i > 0 ) {
							html =  html + '</table>';
							html = html + '<div class="heightspace"></div>';
							html+='<table  border="1" width="80%" style="margin: auto">';
					 }
					  
					 	html+='<tr>';
						html+='<td style="width: 5%;">'+(i+1)+'</td>';
						html+='<td style="width: 30%;">'+bonus[i]['title']+'</td>';
						html+='<td style="width: 20%;">'+moment(bonus[i]['created']).format('DD MMM YYYY')+'</td>';
						html+='<td style="width: 11%;">'+bonus[i]['count_views']+'</td>';
						html+='<td style="width: 11%;">'+score+'</td>';
						html+='<td style="width: 11%;">'+share+'</td>';
						html+='<td style="width: 11%;">'+like+'</td>';
						html+='</tr>';
				 }
				 file = file + html + '</table></body></html>';
				 fis.writeFile(appRoot + '/index_bonus.html', file, function(err) {
						if(err == null) {
							html = fs.readFileSync(appRoot+'/index_bonus.html', 'utf8');
							var options = { format: 'Letter' };
							var file_path = appRoot+'/Export-Bonus-Round-PDF.pdf';
							pdf.create(html, options).toFile(file_path, function(err, respone_pdf) {
								if (err) return console.log(err);
								  res.download(file_path);
								  return ;
							});
						}
					});
			});
		});
	}else {
		App.findOne({url: req.params['appurl']}, 'url', function(err,app) {
			Bonus
			.find({app_id: app._id})
			.sort({ order: 1 })
			.exec(function(err, bonus) {
				var dataset =[];
				var specification = {
						no: { 
					    displayName: 'No.',
					    headerStyle: stylesXLS.headerDark,
					    width: 120
					  },
					  title: {
						    displayName: 'Title',
						    headerStyle: stylesXLS.headerDark,	   
						    width: 120 
						  },
					  datepost: {
						    displayName: 'Date Posted',
						    headerStyle: stylesXLS.headerDark,
						    width: 120
						  },
					  views: {
						    displayName: 'Views',
						    headerStyle: stylesXLS.headerDark,
						    width: 120
						  },
					  scored: {
						    displayName: 'Score',
						    headerStyle: stylesXLS.headerDark,
						    width: 120 
						  },
					  shares: {
						    displayName: 'Shares',
						    headerStyle: stylesXLS.headerDark,
						    width: 120 
						  },
					  likes: {
						    displayName: 'Likes',
						    headerStyle: stylesXLS.headerDark,
						    width: 120 
						  }
					} 
				for(i=0;i<bonus.length;i++) {
					var score = 0;
					 if(bonus[i]['count_views'] != 0)  {
						 score = Math.round((parseInt(bonus[i]['count_share_like'])/parseInt(bonus[i]['count_views']))*100)+'%';
					 }else {
						 score = Math.round((parseInt(bonus[i]['count_share_like']/1)*100))+'%';
					 }
					 
					 var share = '-';
					 var like = '-';				 
					 if(bonus[i]['required_action'] == 'share' ) {
						 share = bonus[i]['count_share_like'];
						 like = '-';
					 }else {
						 share = '-';
						 like = bonus[i]['count_share_like'];
					 }
					 
					 dataset.push({
									no: (i+1),
									title: bonus[i]['title'],
									datepost: moment(bonus[i]['created']).format('DD MMM YYYY'),
									views: bonus[i]['count_views'],
									scored: score,
									shares: share,
									likes: like
								});
					 
				}
				var report = excel.buildExport(
						  [ 
						    {
						      name: 'Sheet1',    
						      specification: specification,
						      data: dataset 
						    }
						  ]
						);
				res.attachment('Export-Bonus-Round-XLS.xls');
				return res.send(report);
			});
		});
	}
});

router.post('/:appurl/admin/exportapplication',isAdminLoggedIn, function(req,res) {
	if(req.body.app_type == 'PDF') {
		App.findOne({url: req.params['appurl']}, 'url', function(err,app) {
			Question.find({app_id: app._id})
			.sort({ order: 1 })
			.exec(function(err, questions) {
				var file = fs.readFileSync(appRoot+'/head_question.html', "utf8");
				file = file.replace("{app_name}", req.body.app_name.toUpperCase());
				var html='';
				for(i=0;i<questions.length;i++) {
					var scored = 0;
					if(questions[i].count_views != 0 ) {
						scored = Math.round((parseInt(questions[i]['count_correct_answers'])/parseInt(questions[i]['count_views']))*100)+'%';
					}else {
						scored = '0%';
					}
					
					if( i == 18) {
						html =  html + '</table>';
						html = html + '<div class="heightspace"></div>';
						html+='<table  border="1" width="80%" style="margin: auto">';
					}
					
					if((i % 20) == 0 && i > 39 ) {
						html =  html + '</table>';
						html = html + '<div class="heightspace"></div>';
						html+='<table  border="1" width="80%" style="margin: auto">';
					}
					
					html+='<tr class="rowdata">';
					html+='<td style="width: 5%">'+(i+1)+'</td>';
				 
					if(questions[i]['title'].length > 25 ) {
						html+='<td style="width: 20%">'+questions[i]['title'].substring(0,25)+'...'+'</td>';
					}else {
						html+='<td style="width: 20%">'+questions[i]['title']+'</td>';
					}
					
					if(questions[i]['question'].length > 70 ) {
						html+='<td style="width: 45%">'+questions[i]['question'].substring(0,70)+'...'+'</td>';
					}else {
						html+='<td style="width: 45%">'+questions[i]['question']+'</td>';
					}
					
					html+='<td style="width: 10%">'+moment(questions[i]['created']).format('DD MMM YYYY')+'</td>';
					html+='<td style="width: 10%">'+questions[i]['count_views']+'</td>';
					html+='<td style="width: 10%">'+scored+'</td>';
					html+='</tr>';
				}
				
				file = file + html + '</table></html>';
				fis.writeFile(appRoot + '/index_question.html', file, function(err) {
					if(err == null) {
						html = fs.readFileSync(appRoot+'/index_question.html', 'utf8');
						var options = { format: 'Letter' };
						var file_path = appRoot+'/Export-Question-PDF.pdf';
						pdf.create(html, options).toFile(file_path, function(err, respone_pdf) {
							if (err) return console.log(err);
							  res.download(file_path);
							  return ;
						});
					}
				});
				
			});
		});
	}else {
		App.findOne({url: req.params['appurl']}, 'url', function(err,app) {
			Question.find({app_id: app._id})
			.sort({ order: 1 })
			.exec(function(err, questions) {
				 
				var dataset =[];
				
				var specification = {
						no: { 
					    displayName: 'No.',
					    headerStyle: stylesXLS.headerDark,
					    width: 120
					  },
					  title: {
						    displayName: 'Title',
						    headerStyle: stylesXLS.headerDark,	   
						    width: 120 
						  },
					  question: {
						    displayName: 'Question',
						    headerStyle: stylesXLS.headerDark,   	   
						    width: 120
						  },
					  datepost: {
						    displayName: 'Date Posted',
						    headerStyle: stylesXLS.headerDark,
						    width: 120
						  },
					  views: {
						    displayName: 'Views',
						    headerStyle: stylesXLS.headerDark,
						    width: 120
						  },
					  scored: {
						    displayName: 'Score',
						    headerStyle: stylesXLS.headerDark,
						    width: 120 
						  }				  
					} 
				
				for(i=0;i<questions.length;i++) {
					var scored = 0;
					if(questions[i].count_views != 0 ) {
						scored = Math.round((parseInt(questions[i]['count_correct_answers'])/parseInt(questions[i]['count_views']))*100)+'%';
					}else {
						scored = '0%';
					}
					dataset.push(
						{
							no: (i+1),
							title: questions[i]['title'],
							question: questions[i]['question'],
							datepost: moment(questions[i]['created']).format('DD MMM YYYY'),
							views: questions[i]['count_views'],
							scored: scored
						});	
				}
				
				var report = excel.buildExport(
						  [ 
						    {
						      name: 'Sheet1',    
						      specification: specification,
						      data: dataset 
						    }
						  ]
						);
				res.attachment('Export-Question-XLS.xls');
				return res.send(report);
				
				
			});
		});
		
		
	}
});


router.post('/:appurl/admin/export',isAdminLoggedIn, function(req,res) {	 
	if(req.body.app_type == 'PDF') {
		var file = fs.readFileSync(appRoot+'/head.html', "utf8");
		file = file.replace("{app_name}", req.body.app_name.toUpperCase());
		var html='';
		Scored.find({
			"app_id": req.body.app_id
		})
		.populate('user_id', 'name fb_friends')
		.sort({ point: -1 })
		.limit(2000)
		.exec(function(err, scoreds) {
			for( i = 0 ; i < scoreds.length; i++) {
				 
				var fb_friends = '-';
				if( typeof(scoreds[i]['user_id']['fb_friends']) != 'undefined' ) {
					fb_friends = scoreds[i]['user_id']['fb_friends'];
				}
				var points = '-';
				if( parseInt(scoreds[i]['point']) != 0) {
					points = scoreds[i]['point'] ;
				}
				var count_view = '-';
				if( parseInt(scoreds[i]['count_view']) != 0) {
					count_view = scoreds[i]['count_view'];
				}
				
				var share = '-';
				if( parseInt(scoreds[i]['share']) != 0 ) {
					share = scoreds[i]['share'];
				}
				
				var like = '-';
				if( parseInt(scoreds[i]['like']) != 0 ) {
					like = scoreds[i]['like'];
				}
				
				
				
				if( (i % 35) == 0 && i > 0 ) {
					html =  html + '</table>';
					html = html + '<div class="heightspace"></div>';
					html+='<table  border="1" width="80%" style="margin: auto">';
				}
				
				
				html+='<tr>';
				html+='<td style="width:5%">'+(i+1)+'</td>';
				html+='<td style="width:25%">'+scoreds[i]['user_id']['name']+'</td>';
				html+='<td style="width:14%">'+points+'</td>';
				html+='<td style="width:14%">'+fb_friends+'</td>';
				html+='<td style="width:14%">'+count_view+'</td>';
				html+='<td style="width:14%">'+share+'</td>';
				html+='<td style="width:14%">'+like+'</td>';
				html+='</tr>';
			}
			
			file = file + html + '</table></html>';
			fis.writeFile(appRoot + '/index.html', file, function(err) {
				if(err == null) {
					html = fs.readFileSync(appRoot+'/index.html', 'utf8');
					var options = { format: 'Letter' };
					var file_path = appRoot+'/Export-Power-User-Pdf.pdf';
					pdf.create(html, options).toFile(file_path, function(err, respone_pdf) {
						if (err) return console.log(err);
						  res.download(file_path);
						  return ;
					});
				}
			});
		});
		
	}else {
		var styles = {
				  headerDark: {
				    fill: {
				      fgColor: {
				        rgb: 'FFFFFF'
				      }
				    },
				    font: {
				      color: {
				        rgb: '000000'
				      },			     
				      bold: false,
				      underline: false
				    }
				  },
				  cellPink: {
				    fill: {
				      fgColor: {
				        rgb: 'FFFFCCFF'
				      }
				    }
				  },
				  cellGreen: {
				    fill: {
				      fgColor: {
				        rgb: 'FF00FF00'
				      }
				    }
				  }
				};
		
		var specification = {
			no: { 
		    displayName: 'No.',
		    headerStyle: styles.headerDark,
		    width: 120
		  },
		  users: {
			    displayName: 'Users',
			    headerStyle: styles.headerDark,	   
			    width: 120 
			  },
		  points: {
			    displayName: 'Points',
			    headerStyle: styles.headerDark,   	   
			    width: 120
			  },
		  friends: {
			    displayName: 'Friends',
			    headerStyle: styles.headerDark,
			    width: 120
			  },
		  visits: {
			    displayName: 'Visits',
			    headerStyle: styles.headerDark,
			    width: 120
			  },
		  shares: {
			    displayName: 'Shares',
			    headerStyle: styles.headerDark,
			    width: 120 
			  },
		  likes: {
			    displayName: 'Likes',
			    headerStyle: styles.headerDark, 	   	   
			    width: 120 
			  }
		  
		} 
		Scored.find({
			"app_id": req.body.app_id
		})
		.populate('user_id', 'name fb_friends')
		.sort({ point: -1 })
		.limit(2000)
		.exec(function(err, scoreds) {
			var dataset =[];
			for(i=0;i<scoreds.length;i++) {
			 
				var fb_friends = '-';
				if(typeof(scoreds[i]['user_id']['fb_friends'])!='undefined') {
					fb_friends = scoreds[i]['user_id']['fb_friends'];
				}
				
				var points = '-';
				if(scoreds[i]['point']!=0) {
					points =  scoreds[i]['point'];
				}
				
				var count_view = '-';
				if(scoreds[i]['count_view']!=0) {
					count_view =  scoreds[i]['count_view'];
				}
				
				var share = '-';
				if(scoreds[i]['share']!=0) {
					share =  scoreds[i]['share'];
				}
				
				var like = '-';
				if(scoreds[i]['like']!=0) {
					like =  scoreds[i]['like'];
				}
				 
				dataset.push(
				{
					no: (i+1),
					users: scoreds[i]['user_id']['name'],
					points: points,
					friends: fb_friends,
					visits: count_view,
					shares: share,
					likes: like
				}		
				);
				
			}
			
			var report = excel.buildExport(
					  [ 
					    {
					      name: 'Sheet1',    
					      specification: specification,
					      data: dataset 
					    }
					  ]
					);
			res.attachment('Export-Power-User-XLS.xls');
			return res.send(report);
			
		});
	}
	 
});

router.get('/:appurl/admin/dashboard', isAdminLoggedIn, function(req, res) {
	req.app.locals.activeMenu = {parent: 'dashboard'};
	var data = {},
		signupThisMonth = [],
		numDayFromBeginThisMonth = 1;
	
	async.waterfall([
	     getApp,
	     deleteScoredNotValid,
         getAppViews,
         getPowerUser,
         getNumMale,
         getNumFemale,
         getSignUpThisMonth,
         getTotalSignup,
         getTotalPageView,
         getAppRepeat,
         getUniqueToday,
         getAvgRepeatRatio,
         getTotalUnique,
    ], function (err) {
    	var totalGender = data['numMale'] + data['numFemale'];
		res.render('admin/dashboard.jade', { 
	    	title: 'Dashboard',
	    	appViews: JSON.stringify(data['appViews']),
	    	poweruser: data['poweruser'],
	    	numMale: data['numMale'],
	    	numFemale: data['numFemale'],
	    	app: data['app'],
	    	percentMale: totalGender == 0 ? 0 : (data['numMale'] / totalGender * 100) % 1 == 0.5 ? Math.floor((data['numMale'] / totalGender * 100)): (data['numMale'] / totalGender * 100).toFixed(0),
			percentFemale: totalGender == 0 ? 0 : (data['numFemale'] / totalGender * 100).toFixed(0),
			totalPageView: data['totalPageView'],
			signupThisMonth: JSON.stringify(signupThisMonth),
			numDayFromBeginThisMonth: numDayFromBeginThisMonth,
			totalNewSignup: data['totalNewSignup'],
			appViewToDate: data['appViewToDate'],
			appRepeats: JSON.stringify(data['appRepeats']),
			appRepeatToDate: data['appRepeatToDate'],
			uniqueToday: data['uniqueToday'],
			avgPageVisits: data['totalPageView'] && data['totalUnique'] ? (data['totalPageView']/data['totalUnique']).toFixed(1) : 0,
			avgRepeatRatio: data['avgRepeatRatio'],
			totalUnique: data['totalUnique']
	    }); 
    });
	
	function deleteScoredNotValid(callback) {
		Scored.find({app_id: data['app']._id})
		.populate('user_id', 'name')
		.select('user_id')
		.exec(function(err, allScoreds) {
			var listScoredDel = [];
			for (var i in allScoreds) {
				if (! allScoreds[i].user_id) {
					listScoredDel.push(allScoreds[i]._id);
				}
			}
			if (listScoredDel.length) {
				Scored.remove({ '_id': {
					$in: listScoredDel
				}}, function() {
					callback(null);
				});
			} else {
				callback(null);
			}
		});
	}
	
    function getAppViews(callback) {
    	var toDate = req.app.locals.moment().endOf('day')._d.toISOString(),
    		fromDate = req.app.locals.moment().subtract(8,'d').endOf('day')._d.toISOString();
    	
    	AppViews.find({
    		"app_id": data['app']._id,
    		"date": {
    			"$gte": fromDate,
    			"$lte": toDate
    		}
    	})
		.sort({ date: 1 })
		.exec(function(err, appViews) {
			data['appViews'] = appViews;
			data['appViewToDate'] = req.app.locals.moment().format('YYYY-MM-DD');
    		callback(null);
		});
    }

    function getPowerUser(callback) {
    	Scored.find({
    		"app_id": data['app']._id
    	})
    	.populate('user_id', 'name avatar fb_friends')
    	.sort({ point: -1 })
    	.limit(2000)
    	.exec(function(err, scoreds) {
    		data['poweruser'] = scoreds ? scoreds : [];
    		callback(null);
    	});
    }
    
    function getNumMale(callback) {
    	Scored.count({
    		"app_id": data['app']._id,
    		"fb_gender": "male"
    	})
    	.exec(function(err, numMale) {
    		data['numMale'] = numMale;
    		callback(null);
    	});
    }
    
    function getNumFemale(callback) {
    	Scored.count({
    		"app_id": data['app']._id,
    		"fb_gender": "female"
    	})
    	.exec(function(err, numFemale) {
    		data['numFemale'] = numFemale;
    		callback(null);
    	});
    }
    
    function getApp(callback) {
		App.findOne({url: req.params['appurl']}).exec(function (err, app) {
            data['app'] = app;
            callback(null);
        });
	}
    
    function getSignUpThisMonth(callback) {
    	Scored.getSignUpOfMonth(data['app']._id, req.app.locals.moment().format('YYYY-MM'), function(scoreds, numDay) {
    		signupThisMonth = scoreds;
    		numDayFromBeginThisMonth = numDay;
    		callback(null);
    	})
    }
    
    function getTotalSignup(callback) {
    	Scored.count({
    		"app_id": data['app']._id,
    	})
    	.exec(function(err, totalNewSignup) {
    		data['totalNewSignup'] = totalNewSignup;
    		callback(null);
    	});
    }
    
    function getTotalPageView(callback) {
    	AppViews.aggregate([
	    	{ $match: {
	    		app_id: data['app']._id + ''	// Convert objectId to string
			}},
			{ $group: {
				_id: null,
				total_views: { $sum: "$count_view"  }
			}}
		], function (err, result) {
			data['totalPageView'] = 0;
			if (! err && result.length) {
				data['totalPageView'] = result[0]['total_views'];
		    }
			callback(null);
		});
    }
    
    function getAppRepeat(callback) {
    	var toDate = req.app.locals.moment().endOf('day')._d.toISOString(),
			fromDate = req.app.locals.moment().subtract(8,'d').endOf('day')._d.toISOString();
		
		AppRepeat.find({
			"app_id": data['app']._id,
			"date": {
				"$gte": fromDate,
				"$lte": toDate
			}
		})
		.sort({ date: 1 })
		.exec(function(err, appRepeats) {
			data['appRepeats'] = appRepeats;
			data['appRepeatToDate'] = req.app.locals.moment().format('YYYY-MM-DD');
			callback(null);
		});
    }
    
    function getUniqueToday(callback) {
    	var beginToday = req.app.locals.moment().startOf('day').format('YYYY-MM-DDT00:00:00.000') + 'Z';
    	
    	AppRepeat.findOne({
    		"app_id": data['app']._id,
    		"date": {"$gte": beginToday}
    	}, function(err, appRepeat) {
    		data['uniqueToday'] = 0;
    		if (appRepeat) {
    			data['uniqueToday'] = appRepeat.user_signup_count;
    		}
    		callback(null);
    	});
    }
    
    function getAvgRepeatRatio(callback) {
    	AppRepeat.aggregate([
	    	{ $match: {
	    		app_id: data['app']._id + ''	// Convert objectId to string
			}},
			{ $group: {
				_id: null,
				total_unique: { $sum: "$user_signup_count"  },
				total_repeat: { $sum: "$user_login_count"  }
			}}
		], function (err, result) {
    		data['avgRepeatRatio'] = 0;
			if (! err && result.length) {
				data['avgRepeatRatio'] = result[0]['total_repeat'] && result[0]['total_unique'] ? (result[0]['total_repeat'] / result[0]['total_unique']).toFixed(2) : 0; 
		    }
			callback(null);
		});
    }
    
    function getTotalUnique(callback) {
    	AppRepeat.aggregate([
 	    	{ $match: {
 	    		"app_id": data['app']._id + ''	// Convert objectId to string
 			}},
 			{ $group: {
 				_id: null,
 				total_unique: { $sum: "$user_signup_count"  }
 			}}
 		], function (err, result) {
     		data['totalUnique'] = 0;
 			if (! err && result.length) {
 				data['totalUnique'] = result[0]['total_unique'];
 		    }
 			callback(null);
 		});
    }
    
});

router.get('/:appurl/admin/setting', isAdminLoggedIn, function(req, res) {
	App.findOne({ 'url' : req.params['appurl'] }, function(err, app) {
		User.findOne({_id: app.admin_id}, function(err, user) {
			res.render('admin/setting.jade', { 
		    	title: 'Setting',
		    	user: user,
		    	message: req.flash('settingMessage'),
		    	successful: req.flash('successfulMessage'),
		    	app: app
		    }); 
		});
	});
});

router.post('/:appurl/admin/setting', isAdminLoggedIn, function(req, res) {
	
	var app_email = req.body.app_email;
	var app_password = req.body.app_password;
	var app_newpassword = req.body.app_newpassword;
	var app_verpassword = req.body.app_verpassword;	
	
	req.checkBody('app_password', 'Current password is empty').notEmpty();
	req.checkBody('app_newpassword', 'New password is empty').notEmpty();
	req.checkBody('app_newpassword', 'New password must at least 6 characters').isLength({min:6});
	req.checkBody('app_newpassword', 'New password must contain at least 1 uppercase, 1 lowercase, 1 number and no special characters').adminPassword();
	req.checkBody('app_verpassword', 'Verify password is empty').notEmpty();
	var errors = req.validationErrors();			
	if (errors) {
		var errors_list = [];
		for(var i in errors) {
			errors_list.push(errors[i]['msg']);
		}
		req.flash('settingMessage', errors_list);		
		return res.redirect('back');
	} else {	
		if(req.body.app_newpassword != req.body.app_verpassword) {
			req.flash('settingMessage', ['The new password is mismatch']);		
			return res.redirect('back');
		}
	}
	
	App.findOne({ 'url' : req.params['appurl'] }, function(err, app) {
		User.findOne({_id: app.admin_id}, function(error, user) {
			if(user) {											
				if(user.validPassword(app_password)) {				
					user.password = user.generateHash(app_newpassword);
					user.save(function(err) {
						req.flash('successfulMessage', ['Update successfully']);
						return res.redirect('back');
					});
			} else {
				req.flash('settingMessage', ['Current password not match.']);
				return res.redirect('back');
			}
		}
		else {
			req.flash('settingMessage', ['Cannot find user!']);		
		return res.redirect('back');
			}
		});	
	});		
});

router.get('/:appurl/admin/new-question', isAdminLoggedIn, function(req, res) {
	App.findOne({ 'url' : req.params['appurl'] }, function(err, app) {
		res.render('admin/newquestion.jade', { 
	    	title: 'New Question',
	    	appUrl: '/'+ app.url,
	    	appInfo: app,
	    	app: app
	    });
	});
    
});


router.get('/:appurl/admin/update-question/:questionId', isAdminLoggedIn, function(req, res) {
	App.findOne({ 'url' : req.params['appurl'] }, function(err, app) {
		Question.findOne({app_id: app._id, _id: req.params['questionId'] }, function(err, question){
	    	if (question) {
	    		res.render('admin/updatequestion.jade', { 
	    	    	title: 'Update Question',
	    	    	question: question,
	    	    	app: app
	    	    });
	    	} else {
	    		res.send("Can't found question");
	    	}
	    });
	});
    
});

router.get('/:appurl/preview-question', function(req, res) {
	App.findOne({ 'url' : req.params['appurl'] }, function(err, app) {
		var preview = req.app.locals.previewQuestion[app.url];
		if (! preview) {
			return res.send('Empty preview data');
		}
		
	    res.render('web/preview-page.jade', { 
	    	title: 'Preview Question',
	    	appUrl: '/'+ app.url,
	    	appInfo: app,
	    	preview: preview
	    });
	});
});

router.post('/:appurl/preview-question', function(req, res) {
	App.findOne({ 'url' : req.params['appurl'] }, 'url', function(err, app) {
		req.app.locals.previewQuestion[app.url] = {
		    	title : req.body.title,
		    	question: req.body.question,
		    	answer: req.body.answer,
		    	explain: req.body.explain,
		    	img: req.body.img,
		    	font_color: req.body.font_color
		    }
		    res.json({success: 1});
	});
});

router.post('/:appurl/admin/upsert-question', isAdminLoggedIn, function(req, res) {
	req.checkBody('qs_title', 'Title is empty').notEmpty();
	req.checkBody('qs_question', 'Question is empty').notEmpty();
	req.checkBody('qs_answer', 'Correct answer is empty').notEmpty();
	req.checkBody('qs_explain', 'Explain is empty').notEmpty();
	req.checkBody('font_color', 'Font colour is empty').notEmpty();
	
	// Check valid data
	var errors = req.validationErrors();
	if (errors) {
		res.send('There have been validation errors: ' + util.inspect(errors), 400);
		return;
	}
	
	App.findOne({ 'url' : req.params['appurl'] }, 'url', function(err, app) {
		if (typeof(req.body.qs_id) == 'undefined') {
			var newOrder = 1,
				newIndex = 1;
			// Find next index
			Question.findOne().sort( {index: -1} ).exec(function(err, lastQuestion) {
				if (lastQuestion.index != undefined) {
					newIndex = lastQuestion.index + 1;
				}
				// Find next order for new question
				Question.findOne({app_id : app._id}).sort( { order: -1 } ).exec(function (err, question) {
					if (!err && question) { newOrder = question.order + 1; }
					
					var base64Data = req.body.img_base64.replace(/^data:image\/png;base64,/, "");
					var imgName = 'qs-image-'+ Date.now() +'.png';
					var imgPath = '../app/public/uploads/'+ imgName;
					fs.writeFile(imgPath, base64Data, 'base64', function(err) {
						if (! err) {
							// Create new question
							var newQuestion = new Question();
							newQuestion.app_id = app._id;
							newQuestion.title = req.body.qs_title;
							newQuestion.question = req.body.qs_question;
							newQuestion.correct_answer = req.body.qs_answer;
							newQuestion.explain = req.body.qs_explain;
							newQuestion.font_color = req.body.font_color;
							newQuestion.image = '/uploads/'+ imgName;
							newQuestion.order = newOrder;
							newQuestion.qs_status = req.body.qs_status;
							newQuestion.index = newIndex;
							newQuestion.save(function() {
								res.redirect('/'+ app.url +'/admin/application');
							});
						} else {
							return res.send('Upload image error');
						}
					});
				});
			});
			
		} else {
			// Update question
			Question.findOne({ '_id' : req.body.qs_id }, function(err, question) {
				if (question) {
					question.title = req.body.qs_title;
					question.question = req.body.qs_question;
					question.correct_answer = req.body.qs_answer;
					question.explain = req.body.qs_explain;
					question.font_color = req.body.font_color;
					question.qs_status = req.body.qs_status;
					
					if (typeof(req.body.img_base64) != 'undefined' && req.body.img_base64) {
						var base64Data = req.body.img_base64.replace(/^data:image\/png;base64,/, "");
						var imgName = 'qs-image-'+ Date.now() +'.png';
						var imgPath = '../app/public/uploads/'+ imgName;
						fs.writeFile(imgPath, base64Data, 'base64', function(err) {
							if (! err) {
								question.image = '/uploads/'+ imgName;
								question.save(function(err) {
									// Delete old image
									helpers.deleteFile(req.body.qs_current_image);
								});
								
								//res.redirect('back');
								res.redirect('/'+ app.url +'/admin/application')
							} else {
								return res.send('Upload image error');
							}
						});
					} else {
						question.save();
						//res.redirect('back');
						res.redirect('/'+ app.url +'/admin/application')
					}
				}
			});
		}
	});
});

router.post('/:appurl/admin/delete-question', isAdminLoggedIn, function(req, res) {
	var questionId = typeof(req.body.qs_id_delete) != 'undefined' ? req.body.qs_id_delete : '';
	
	if (questionId) {
		Question.findOne({ '_id' : questionId }, function(err, question) {
			if (question) {
				helpers.deleteFile(question.image);
				question.remove(function() {
					res.redirect('back');
				});
			} else {
				return res.redirect('back');
			}
		});
	} else {
		return res.redirect('back');
	}
})

router.post('/:appurl/admin/sortable-data', isAdminLoggedIn, function(req, res) {
	var listOrder = req.body.list_order,
		table = req.body.table,
		count = 0,
		process = true,
		collection = table == 'question' ? Question : Bonus,
		listIndex = table == 'question' ? req.body.qs_index_list : req.body.bn_index_list;
		
	
	// For update multiple we use setInterval to know update success before update other
	var handle = setInterval(function() {
		if (process) {
			process = false;
			var order = count + 1,
				documentId = listOrder[count]
			
			collection.findOne({ '_id' : documentId }, function(err, item) {
				if (item) {
					item.order = order;
					item.index = parseInt(listIndex[count]);
					item.save();
				}
				
				count++;
				process = true;
			});
		}
		
		if (listOrder.length - 1 == count) {
			clearInterval(handle);
			return res.json({status: 'done'});
		}
	}, 20);
})

router.get('/:appurl/admin/application', isAdminLoggedIn, function(req, res) {
	var data = {
			countQuestion: 0,
			maxScored: 0,
			qsIndexList: [],
			bnIndexList: []
	};
	
	async.waterfall([
         getApp,
         getListQuestion,
         getScored,
         getBonus,
    ], function (err) {
		req.app.locals.activeMenu.parent = 'application';		 
		res.render('admin/application.jade', { 
             title: 'Application',
             questions: data.questions,
             countQuestion: data.countQuestion,
             maxScored: data.maxScored,
             avergeScored: data.avergeScored,
             bonus: data.bonus,
             app_name: data['app']['app_name'],
             app_id: data['app']['_id'],
             app: data['app'],
             qsIndexList: data.qsIndexList,
             bnIndexList: data.bnIndexList
         });  
    });
	 
	function getListQuestion(callback) {
		Question.find({app_id: data['app']['_id']})
			.sort({ order: 1 })
			.exec(function(err, questions) {
				for(var i=0 in questions)
				{
					var item = questions[i];
					if (item.qs_status == "1") {
						data.countQuestion++;                                                           				
					}
					
					data['qsIndexList'].push(item.index);
		 		}
				
				data['questions'] = questions;
				callback(null);
			});
	}
	function getScored(callback) {
		Scored.findOne({app_id: data['app']['_id']})
			.sort( { point: -1 } )
			.exec(function (err, scored) {
				if(scored){
					data.maxScored = scored.point;
			    }
				
				callback(null);
			});
	}
	function getApp(callback) {
		App.findOne({url: req.params['appurl']}).exec(function (err, app) {
	        	var total_round = 0;
                if(app.total_round == 0) {
                    total_round = 1;
                } else{
                    total_round = app.total_round;
                }
                data['avergeScored'] = app.total_scored && total_round ?  (app.total_scored/total_round).toFixed(2) : '0';
                data['app'] = app;
                callback(null);
	        });
	}
	
	function getBonus(callback) {
		Bonus
			.find({app_id: data['app']['_id']})
			.sort({ order: 1 })
			.exec(function(err, bonus) {
				if (bonus) {
					data['bonus'] = bonus;
					for(var i=0 in bonus)
					{
						var item = bonus[i];
						if (item.bonus_status == "1") {
							data.countQuestion++;                                                           				
						}
						
						data['bnIndexList'].push(item.index);
			 		}
				} else {
					data['bonus'] = [];
				}
				
				callback(null);
			});
	}
});

router.get('/:appurl/admin/new-bonus', isAdminLoggedIn, function(req, res) {
	App.findOne({ 'url' : req.params['appurl'] }, function(err, app) {
		res.render('admin/newbonus.jade', { 
	    	title: 'New Bonus',
	    	app: app
	    }); 
	});
});

router.get('/:appurl/admin/update-bonus/:bonusId', isAdminLoggedIn, function(req, res) {
	App.findOne({ 'url' : req.params['appurl'] }, function(err, app) {
		Bonus.findOne({app_id: app._id, _id: req.params['bonusId'] }, function(err, bonus){
	    	if (bonus) {
	    		res.render('admin/updatebonus.jade', { 
	    	    	title: 'Update Bonus',
	    	    	bonus: bonus,
	    	    	app: app
	    	    });
	    	} else {
	    		res.send("Can't found bonus");
	    	}
	    });
	});
});

router.post('/:appurl/admin/upsert-bonus', isAdminLoggedIn, function(req, res) {
	req.checkBody('bonus_title', 'Title is empty').notEmpty();
	req.checkBody('bonus_caption', 'Caption is empty').notEmpty();
	req.checkBody('bonus_fb_link', 'Facebook Link is empty').notEmpty();
	req.checkBody('bonus_action', 'Required Action is empty').notEmpty();
	req.checkBody('bonus_point', 'Bonus Points is empty').notEmpty();
	req.checkBody('background', 'Background is empty').notEmpty();
	
	// Check valid data
	var errors = req.validationErrors();
	if (errors) {
		res.send('There have been validation errors: ' + util.inspect(errors), 400);
		return;
	}
	
	App.findOne({ 'url' : req.params['appurl'] }, 'url', function(err, app) {
		if (typeof(req.body.bonus_id) == 'undefined') {
			var newOrder = 1,
				newIndex = 1;
			
			// Find last index
			Bonus.findOne().sort( { index: -1 } ).exec(function (err, lastBonus) {
				if (lastBonus.index != undefined) {
					newIndex = lastBonus.index + 1;
				}
				
				// Find next order for new bonus
				Bonus.findOne({app_id : app._id}).sort( { order: -1 } ).exec(function (err, bonus) {
					if (!err && bonus) { newOrder = bonus.order + 1; }
					
					var base64Data = req.body.img_base64.replace(/^data:image\/png;base64,/, "");
					var imgName = 'qs-image-'+ Date.now() +'.png';
					var imgPath = '../app/public/uploads/'+ imgName;
					fs.writeFile(imgPath, base64Data, 'base64', function(err) {
						if(! err) {
							// Create new bonus
							var newBonus = new Bonus();
							newBonus.app_id = app._id;
							newBonus.title = req.body.bonus_title;
							newBonus.caption = req.body.bonus_caption;
							newBonus.fb_link = req.body.bonus_fb_link;
							newBonus.required_action = req.body.bonus_action;
							newBonus.background = req.body.background;
							newBonus.point = req.body.bonus_point;
							newBonus.image = '/uploads/'+ imgName;
							newBonus.order = newOrder;
							newBonus.index = newIndex;
							newBonus.bonus_status = req.body.bonus_status;
							newBonus.save(function() {
								res.redirect('/'+ app.url +'/admin/application');
							});
						} else {
							return res.send('Upload image error');
						}
					});
					
				});
			});
		} else {
			// Update bonus
			Bonus.findOne({ '_id' : req.body.bonus_id }, function(err, bonus) {
				if (bonus) {
					bonus.title = req.body.bonus_title;
					bonus.caption = req.body.bonus_caption;
					bonus.fb_link = req.body.bonus_fb_link;
					bonus.required_action = req.body.bonus_action;
					bonus.point = req.body.bonus_point;
					bonus.background = req.body.background;
					bonus.bonus_status = req.body.bonus_status;
					
					if (typeof(req.body.img_base64) != 'undefined' && req.body.img_base64) {
						var base64Data = req.body.img_base64.replace(/^data:image\/png;base64,/, "");
						var imgName = 'qs-image-'+ Date.now() +'.png';
						var imgPath = '../app/public/uploads/'+ imgName;
						fs.writeFile(imgPath, base64Data, 'base64', function(err) {
							if (! err) {
								bonus.image = '/uploads/'+ imgName;
								bonus.save(function(err) {
									// Delete old image
									helpers.deleteFile(req.body.qs_current_image);
								});
								
								//res.redirect('back');
								res.redirect('/'+ app.url +'/admin/application')
							} else {
								return res.send('Upload image error');
							}
						});
					} else {
						bonus.save();
						//res.redirect('back');
						res.redirect('/'+ app.url +'/admin/application');
					}
				}
			});
		}
	});
	
});

router.post('/:appurl/admin/delete-bonus', isAdminLoggedIn, function(req, res) {
	var bonusId = typeof(req.body.bn_id_delete) != 'undefined' ? req.body.bn_id_delete : '';
	
	if (bonusId) {
		Bonus.findOne({ '_id' : bonusId }, function(err, bonus) {
			if (bonus) {
				var image = bonus.image;
				// Delete image
				helpers.deleteFile(bonus.image);
				bonus.remove(function() {
					return res.redirect('back');
				});
			} else {
				return res.redirect('back');
			}
		});
	} else {
		return res.redirect('back');
	}
})

router.get('/:appurl/preview-bonus', function(req, res) {
	var preview = req.app.locals.previewBonus[req.params['appurl']];
	if (! preview) {
		return res.send('Empty preview data');
	}
    res.render('web/preview-bonus.jade', { 
    	title: 'Preview Bonus',
    	preview: preview
    }); 
});

router.post('/:appurl/preview-bonus', function(req, res) {
    req.app.locals.previewBonus[req.params['appurl']] = {
    	title : req.body.title,
    	caption: req.body.caption,
    	fb_link: req.body.fb_link,
    	required_action: req.body.required_action,
    	point: req.body.point,
    	img: req.body.img,
        background: req.body.background
    }
    res.json({success: 1}); 
});

router.get('/:appurl/admin/general', isAdminLoggedIn, function(req, res) {
	req.app.locals.activeMenu = {parent: 'customise', child: 'general'};
	App.findOne({ 'url' : req.params['appurl'] }, function(err, app) {
		res.render('admin/general.jade', { 
	    	title: 'General',
	    	app: app,
	    	message: req.flash('generalMessage'),
	    	admin_access: req.user.role != 1 && (app.app_status == 0 && app.user_role_save_status == 1) ? false : true,
	    });
	});
})

router.post('/:appurl/admin/general', isAdminLoggedIn, requiredAdminAccess,
		uploadImageApp.fields([{
	    	name: 'profile_file', maxCount: 1
		}, {
			name: 'intro_file', maxCount: 1
		}]), function(req, res) {
	
			req.checkBody('app_name', 'Application name is empty').notEmpty();
			req.checkBody('url', 'Application url is empty').notEmpty();
			req.checkBody('app_status', 'Application status is empty').notEmpty();
			req.checkBody('term_condition', 'Terms and Conditions is empty').notEmpty();
			req.checkBody('privacy', 'Privacy Policy is empty').notEmpty();
			req.checkBody('term_service', 'Terms of service is empty').notEmpty();
			var only_login_fb = req.body.only_login_fb == 'on' ? true : false;
			
			// Check valid data
			var errors = req.validationErrors();
			if (errors) {
				var errors_list = [];
				for(var i in errors) {
					errors_list.push(errors[i]['msg']);
				}
				req.flash('generalMessage', errors_list);
				helpers.deleteGeneralUploadedFiles(req.files);	// Delete files uploaded
				return res.redirect('back');
			}
			
			// Valid url
			var regexp = /^[a-z0-9-_]+$/;
			if (req.body.url.search(regexp) == -1) {
				req.flash('generalMessage', ['Application Url expect alphabet, number, dash and underscore character.']);
				helpers.deleteGeneralUploadedFiles(req.files);	// Delete files uploaded
				return res.redirect('back');
			}
			
			var data = {
				app_status: req.body.app_status,
				user_role_save_status: req.user.role,
				term_condition: req.body.term_condition,
				privacy: req.body.privacy,
				term_service: req.body.term_service,
				only_login_fb: only_login_fb
			};
			
			if (typeof req.files.profile_file != 'undefined') {
				data['profile_img'] = '/uploads/'+ req.files.profile_file[0].filename;
			}
			
			if (typeof req.files.intro_file != 'undefined') {
				data['intro_file'] = '/uploads/'+ req.files.intro_file[0].filename;
			} 
			
			App.findOne({ 'url' : req.params['appurl'] }, function(err, currentApp) {
				// Case not change app name and url
				if (currentApp.app_name == req.body.app_name && currentApp.url == req.body.url) {
					App.updateData(currentApp._id, data, function(app) {
						return res.redirect('back');
					});
				// Case only change app name
				} else if(currentApp.app_name != req.body.app_name && currentApp.url == req.body.url) {
					App.findOne({ 'app_name' : req.body.app_name }, function(err, app) {
						if (app) {
							req.flash('generalMessage', ['Application name exist.']);
							helpers.deleteGeneralUploadedFiles(req.files);	// Delete files uploaded
							res.redirect('back');
						} else {
							data['app_name'] = req.body.app_name;
							App.updateData(currentApp._id, data, function(app) {
								return res.redirect('back');
							});
						}
					});
					// Case only change app url
				} else if (currentApp.app_name == req.body.app_name && currentApp.url != req.body.url) {
					App.findOne({ 'url' : req.body.url }, function(err, app) {
						if (app) {
							req.flash('generalMessage', ['Application url exist.']);
							helpers.deleteGeneralUploadedFiles(req.files);	// Delete files uploaded
							return res.redirect('back');
						} else {
							data['url'] = req.body.url;
							
							App.updateData(currentApp._id, data, function(app) {
								return res.redirect('/'+ app.url +'/admin/general');
							});
						}
					});
				// Case change both app name and url
				} else {
					App.findByNameOrUrl(req.body.app_name, req.body.url, function(err, app) {
						if (app.length) {
							req.flash('generalMessage', ['Application name or url exist.']);
							helpers.deleteGeneralUploadedFiles(req.files);	// Delete files uploaded
							res.redirect('back');
						} else {
							data['app_name'] = req.body.app_name;
							data['url'] = req.body.url;
							
							App.updateData(currentApp._id, data, function(appUpdate) {
								return res.redirect('/'+ appUpdate.url +'/admin/general');
							});
						}
					});
				}
			})
})

router.get('/:appurl/admin/theme', isAdminLoggedIn, function(req, res) {
	req.app.locals.activeMenu = {parent: 'customise', child: 'theme'};
	App.findOne({ 'url' : req.params['appurl'] }, function(err, app) {
		res.render('admin/theme.jade', { 
	    	title: 'Theme',
	    	message: req.flash('themeMessage'),
	    	app: app
	    });
	});
})

router.post('/:appurl/admin/theme', isAdminLoggedIn, uploadImageApp.single('theme_destop_bg'), function(req, res) {
	req.checkBody('font_color', 'Font colour is empty').notEmpty();
	
	// Check valid data
	var errors = req.validationErrors();
	if (errors) {
		var errors_list = [];
		for(var i in errors) {
			errors_list.push(errors[i]['msg']);
		}
		req.flash('themeMessage', errors_list);
		if (typeof(req.file) != 'undefined') {
			helpers.deleteFile(req.file.path, true);	// Delete file uploaded
		}
		
		return res.redirect('back');
	}
	
	var data = { font_color: req.body.font_color };
	
	// Add theme destop bg data if upload
	if (typeof(req.file) != 'undefined') {
		data['theme_destop_bg'] = '/uploads/'+ req.file.filename;
	}
	
	// Update data to app
	App.findOne({ 'url' : req.params['appurl'] }, 'url', function(err, app) {
		App.updateData(app._id, data, function(appUpdate) {
			return res.redirect('back');
		});
	});
	
})

router.get('/:appurl/admin/gameplay', isAdminLoggedIn, function(req, res) {
	req.app.locals.activeMenu = {parent: 'customise', child: 'gameplay'};
	
	App.findOne({ 'url' : req.params['appurl'] }, function(err, app) {
		res.render('admin/gameplay.jade', { 
	    	title: 'Gameplay',
	    	app: app,
	    	message: req.flash('gameplayMessage'),
	    }); 
	});
})

router.post('/:appurl/admin/gameplay', isAdminLoggedIn, function(req, res) {
	req.checkBody('rule', 'Rules of the game is empty').notEmpty();
	req.checkBody('completion_message', 'Game completion message is empty').notEmpty();
	req.checkBody('point_adwarded', 'Points awarded is empty').notEmpty();
	req.checkBody('point_deducted', 'Points deducted is empty').notEmpty();
	req.checkBody('completion_background', 'Background is empty').notEmpty();
	req.checkBody('invite_point', 'Invitation reward is empty').notEmpty();

	// Check valid data
	var errors = req.validationErrors();
	if (errors) {
		var errors_list = [];
		for(var i in errors) {
			errors_list.push(errors[i]['msg']);
		}
		req.flash('gameplayMessage', errors_list);
		return res.redirect('back');
	}
	
	var data = {
			rule: req.body.rule,
			completion_message: req.body.completion_message,
			point_adwarded: req.body.point_adwarded,
			point_deducted: req.body.point_deducted,
			completion_background: req.body.completion_background,
			invite_point: req.body.invite_point
	}
	
	// Update data to app
	App.findOne({ 'url' : req.params['appurl'] }, 'url', function(err, app) {
		App.updateData(app._id, data, function(appUpdate) {
			return res.redirect('back');
		});
	});
})

router.get('/:appurl/admin/web', isAdminLoggedIn, function(req, res) {
	App.findOne({ 'url' : req.params['appurl'] }, function(err, app) {
		req.app.locals.activeMenu = {parent: 'customise', child: 'web'};
		res.render('admin/web.jade', { 
	    	title: 'Web',
	    	linkapi: 'http://'+req.get('host')+ '/'+ req.params['appurl'],
	    	message: req.flash('webMessage'),
	    	app: app
	    });
	});
})

//route middleware to make sure a admin is logged in
function isAdminLoggedIn(req, res, next) {
	App.findOne({ 'url' : req.params['appurl'] }, function(err, app) {
		if (req.isAuthenticated()) {
			if (req.user.role == 1) {
				return next();
			} else if(req.user.role == 2 && app.admin_id == req.user.id) {
				return next();
			} else {
				return res.redirect('/'+ req.params['appurl'] +'/');
			}
		} else {
			return res.redirect('/'+ req.params['appurl'] +'/');
		}
	});
}

//route middleware to make sure admin can access app 
function requiredAdminAccess(req, res, next) {
	App.findOne({ 'url' : req.params['appurl'] }, function(err, app) {
		var admin_access = req.user.role != 1 && (app.app_status == 0 && app.user_role_save_status == 1) ? false : true;
		if (admin_access) {
			return next();
		}
		
		return res.redirect('back');
	});
}

module.exports = router;